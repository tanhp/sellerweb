import http from "k6/http";
import { check, sleep } from "k6";
import { getSellerApiUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

const config = JSON.parse(open("../../Config/configInfo.json"));

var editBlogTrend = new Trend("edit_product_duration", true);
var failedEditBlogRate = new Rate("failed_load_product_list_percentage",true);

export function postBlog(access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[4].api_blog}`);
  let payload = JSON.stringify({
    "Title": "[SPQC] Bài viết test",
    "ShortDescription": "Mô tả ngắn bài viết test",
    "PictureURL":"http://media3.scdn.vn/img2/2018/4_20/oVDgyj.jpg",
    "FullDescription":"Mô tả đầy đủ bài viết test"
  });

  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }

  let res = http.post(requestUrl, payload, { headers: headers });
  console.log(res.body);
  let responseBody = JSON.parse(res.body);
  return responseBody;
}

export function putBlog(blogId, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[4].api_blog}`);
  let resBlogDetail = getBlogDetail(blogId, access_token);
  let payload = JSON.stringify({
    "Id": blogId,
    "Title": "[SPQC] Bài viết test",
    "ShortDescription": "Mô tả ngắn bài viết test",
    "PictureURL":"http://media3.scdn.vn/img2/2018/4_20/oVDgyj.jpg",
    "FullDescription":"Mô tả đầy đủ bài viết test",
    "VersionNo": resBlogDetail.VersionNo
  });

  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }

  let res = http.put(requestUrl, payload, { headers: headers });
  editBlogTrend.add(res.timings.duration);
  if (check(res, {
      "Edit blog >> Status 200": (r) => r.status === 200
  })) {
      failedEditBlogRate.add(false);
  } else {
      failedEditBlogRate.add(true);
  }
  let responseBody = JSON.parse(res.body);
  return responseBody;
}

export function delBlog(blogId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_blog}`);
    let payload = JSON.stringify([blogId]);
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.del(requestUrl, payload, { headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getBlogList(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_blog}`);
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.get(requestUrl + "SearchBlog",{ headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getBlogDetail(blogId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_blog}`);
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.get(requestUrl + blogId,{ headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}
