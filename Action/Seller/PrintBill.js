import http from "k6/http";
import { getSellerWebUrl } from "../general.js";

const config = JSON.parse(open("../../Config/configInfo.json"));

export function printSalesOrder(orderIds, access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_print_sales_order}`);
    let payload = JSON.stringify({
        "ids": orderIds
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.post(requestUrl, payload, { headers: headers });
}

export function updatedSoPrintedStatus(orderIds, access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_updated_so_printed_status}`);

    let payload = JSON.stringify({
        "orderIds": orderIds
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.post(apiUpdatedSoPrintedStatusUrl, payload, { headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}