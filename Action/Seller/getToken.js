import http from "k6/http";
import { check, sleep } from "k6";
import { getSellerWebUrl, getSellerApiUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

const config = JSON.parse(open("../../Config/configInfo.json"));

var loginTrend = new Trend("login_trend", true);
var failedLoginRate = new Rate("failed_login_percentage",true);
var failedReLoginRate = new Rate("failed_relogin_percentage",true);

export function getToken(username, password){
    let requestUrl = getSellerApiUrl(`${config.seller[0].api_get_token}`);

    let payload = JSON.stringify({
      "UserName": username,
      "Password": password,
      "reCaptchaResponse": 123
    });

    console.log(payload);

    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "ReCaptchaResponse": "smc@123456"
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    let responseBody = JSON.parse(res.body);
    console.log(responseBody.result.token);
    // check(responseBody, {
    //   "Get Product Detail >> Status 200": (r) => r.statusCode === 200,
    //   "Get Product Detail >> Get token successfully": (r) => r.token === null
    // });

    return responseBody.result.token;
}

export function getStore(username, password){
  let requestUrl = getSellerApiUrl(`${config.seller[0].api_get_token}`);

  let payload = JSON.stringify({
    "UserName": username,
    "Password": password,
    "reCaptchaResponse": 123
  });

  console.log(payload);

  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "ReCaptchaResponse": "smc@123456"
  }

  let res = http.post(requestUrl, payload, { headers: headers });
  let responseBody = JSON.parse(res.body);
  // check(responseBody, {
  //   "Get Product Detail >> Status 200": (r) => r.statusCode === 200,
  //   "Get Product Detail >> Get token successfully": (r) => r.token === null
  // });

  return responseBody.result.userContextDto;
}

export function getCookies(username, password){
    let requestUrl = getSellerWebUrl(`${config.seller[0].api_login}`);

    let payload = JSON.stringify({
      "UserName": username,
      "Password": password,
      "reCaptchaResponse": 123
    });

    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "ReCaptchaResponse": "smc@123456"
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(JSON.stringify(res.cookies));
    return res.cookies;
}

export function relogin(storeName){
  let requestUrl = getSellerWebUrl(`${config.seller[0].api_relogin}`);
  let headers = {
  }
  let res = http.post(requestUrl, { headers: headers });
  if (check(res, {
    "Login >> Compare storeName": (r) => r.body.includes(storeName)
  })) {
    console.log("Có tồn tại " + storeName);
    failedReLoginRate.add(false);
  } else {
    failedReLoginRate.add(true);
  }
  return res;
}

export function login(username, password, shopId){
    console.log(username);
    //let requestUrl = getSellerApiUrl(`${config.seller[0].api_get_token}`);
    let requestUrl = getSellerWebUrl(`${config.seller[0].api_login}`);

    let payload = JSON.stringify({
      "UserName": username,
      "Password": password
      //"reCaptchaResponse": 123
    });

    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "ReCaptchaResponse": "smc@123456"
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(JSON.stringify(res.cookies));
    if (check(res, {
        "Login >> Status 200": (r) => r.status === 200,
        "Login >> Compare storeId": (r) => r.cookies.storeid[0].value == shopId
    })) {
      failedLoginRate.add(false);
    } else {
      console.log("SAI! " + res.cookies.storeid[0].value + " & " + shopId);
      failedLoginRate.add(true);
    }
    return res;
}

export function logOut(){
    let resquestUrl = getSellerWebUrl(`${config.seller[0].api_logout}`);
    let headers = {
      "Content-Type": "text/html",
    }
    let res = http.get(requestUrl, { headers: headers });
}