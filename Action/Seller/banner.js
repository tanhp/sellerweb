import http from "k6/http";
import { check, sleep } from "k6";
import { getSellerApiUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

const config = JSON.parse(open("../../Config/configInfo.json"));

var editBannerTrend = new Trend("edit_product_duration", true);
var failedEditBannerRate = new Rate("failed_load_product_list_percentage",true);

export function postBanner(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_banner_shop}`);
    let payload = JSON.stringify({
        "SystemURL":"https://www.sendo.vn/shop/tan-pro",
        "DisplayOrder":2,
        "PictureURL":"http://media3.scdn.vn/img3/2019/9_19/QoEn0a.jpg",
        "IsDisplayed":false
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function putBanner(bannerId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_banner_shop}`);
    let resBannerDetail = getBannerDetail(bannerId, access_token);
    let payload = JSON.stringify({
        "Id": bannerId,
        "SystemURL":"https://www.sendo.vn/shop/tan-pro",
        "DisplayOrder":2,
        "PictureURL":"http://media3.scdn.vn/img3/2019/9_19/QoEn0a.jpg",
        "IsDisplayed":false,
        "VersionNo": resBannerDetail.versionNo
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }
      
    let res = http.put(requestUrl, payload, { headers: headers });
    editBannerTrend.add(res.timings.duration);
    if (check(res, {
        "Edit banner >> Status 200": (r) => r.status === 200
    })) {
        failedEditBannerRate.add(false);
    } else {
        failedEditBannerRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function delBanner(bannerId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_banner_shop}`);
    let resBannerDetail = getBannerDetail(bannerId, access_token);
    let payload = JSON.stringify({
        "Id": bannerId,
        "SystemURL":"https://www.sendo.vn/shop/tan-pro",
        "DisplayOrder":2,
        "PictureURL":"http://media3.scdn.vn/img3/2019/9_19/QoEn0a.jpg",
        "IsDisplayed":false,
        "VersionNo": resBannerDetail.versionNo
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.del(requestUrl, payload, { headers: headers });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getBannerList(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[4].api_banner_shop}`);
    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }
    let res = http.get(requestUrl,{ headers: headers });
    console.log(res.status);
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getBannerDetail(bannerId, access_token){
    let resBanners = getBannerList(access_token);
    let banners = resBanners.result.data;
    for(var i=0; i<banners.length; i++){
        if(banners[i].id == bannerId)
            return banners[i];
    }
    return null;
}
