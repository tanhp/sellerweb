import http from "k6/http";
import { getSellerApiUrl } from "../general.js";

const config = JSON.parse(open("../../Config/configInfo.json"));

export function updateInstallment(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_installment_term}`);
    let payload = JSON.stringify({
        "InstallmentTerms":[
            {
                "Label": "Trả góp 3 tháng",
                "Percent": 0,
                "Period": 3,
                "IsActive": true,
                "OrdinalNo": 0
            },
            {
                "Label": "Trả góp 6 tháng",
                "Percent": 0,
                "Period": 6,
                "IsActive": false,
                "OrdinalNo": 0
            },
            {
                "Label": "Trả góp 9 tháng",
                "Percent": 1.5,
                "Period": 9,
                "IsActive": false,
                "OrdinalNo": 0
            },
            {
                "Label": "Trả góp 12 tháng",
                "Percent": 3,
                "Period": 12,
                "IsActive": false,
                "OrdinalNo": 0
            },
            {
                "Label": "Trả góp 24 tháng",
                "Percent": 6,
                "Period": 24,
                "IsActive": false,
                "OrdinalNo": 0
            }
        ],
        "IsActive": true
    });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updatePayment(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_payment}`);
    let payload = JSON.stringify({
        "UseSenpay": true,
        "UseCOD": true,
        "StoreId": 141736
      });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function subscribeShippingConfig(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_subscribe_shipping_config}`);
    let payload = JSON.stringify({
        "Levels": [
        {
            "OrderAmount": 300000,
            "SellerSupportFee": 15000,
            "Position": 1,
            "IsActive": false
        },
        {
            "OrderAmount": 1000000,
            "SellerSupportFee": 30000,
            "Position": 2,
            "IsActive": false
        }]
    });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updateLoyaltyPoint(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_loyalty_point}`);
    let payload = JSON.stringify({
        "percent": 2,
        "is_active": true,
        "store_Id": 141736
    });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updateMobilePaymentOnline(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_support_payment_online}`);
    let payload = JSON.stringify({
        "SenpayLevel": [
        {
            "OrderAmount": 100000,
            "DisCountPercent": 3,
            "Position": 1,
            "IsActive": true
        },
        {
            "OrderAmount": 500000,
            "DisCountPercent": 10,
            "Position": 2,
            "IsActive": true
        }]
      });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updateMobilePayment(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_mobile_payment}`);
    let payload = JSON.stringify({
        "AppLevel": [
          {
            "OrderAmount": 0,
            "DisCountPercent": 10,
            "Position": 0,
            "IsActive": false,
          }
        ]
      });
    let res = http.post(requestUrl, payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updateStoreAllowCheck(access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[3].api_store_allow_check}`);
    let res = http.post(requestUrl + "true", payload, { headers: getHeaders(access_token) });
    let responseBody = JSON.parse(res.body);
    return responseBody;
}