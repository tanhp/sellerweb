import http from "k6/http";
import { check, sleep } from "k6";
import { getSellerWebUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";
import { getToken } from "./getToken.js";


const config = JSON.parse(open("../../Config/configInfo.json"));

var loadOrderListTrend = new Trend("load_order_list_duration",true);
var failedOrderListRate = new Rate("failed_load_order_list_percentage",true);

export function getOrderList(cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_search_sales_order}`);

    let payload = JSON.stringify({
        "SalesOrderStatus": "Total",
        "Page": {
        "CurrentPage": 1,
        "PageSize": 10
        },
        "CarrierCode": "ALL",
        "OrderDateFrom": "01/01/2019",
        "OrderDateTo": "04/10/2019",
        "OrderNumberFrom": "",
        "CustomerName": "",
        "PhoneNumber": ""
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.status);
    console.log(res.body);
    loadOrderListTrend.add(res.timings.duration);
    if (check(res, {
        "Get Sales Order List >> Status 200": (r) => r.status === 200
    })) {
        failedOrderListRate.add(false);
    } else {
        failedOrderListRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

