import http from "k6/http";
import { check, sleep } from "k6";
import { getToken } from "./GetToken.js";
import { getSellerWebUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

var getPrivateOfferTrend = new Trend("get_privateOffer_trend", true);
var failedPrivateOfferRate = new Rate("failed_get_privateOffer_percentage",true);

const config = JSON.parse(open("../../Config/configInfo.json"));

export function loadPrivateOfferList(access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[5].api_get_privateoffer_list}`);

    let payload = JSON.stringify({
        "BuyerSendoId": 0
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"token_seller_api": access_token} });
    console.log(res.status);
    console.log(res.body);
    getPrivateOfferTrend.add(res.timings.duration);
    if (check(res, {
        "Get Sales Order List >> Status 200": (r) => r.status === 200
    })) {
        failedPrivateOfferRate.add(false);
    } else {
        failedPrivateOfferRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}