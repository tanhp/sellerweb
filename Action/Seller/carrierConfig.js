import http from "k6/http";
import { check, sleep } from "k6";
import { getToken } from "./GetToken.js";
import { getSellerWebUrl } from "../general.js";

const config = JSON.parse(open("../../Config/configInfo.json"));

import { Trend, Rate } from "k6/metrics";

var getListCarrierConfigTrend = new Trend("get_list_carrier_config_trend", true);
var failedGetListCarrierConfigRate = new Rate("failed_get_list_carrier_config_percentage",true);

export function saveShopInfo(access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[3].api_update_list_carrier_config}`);
    let resShopInfo = getShopInfo(access_token);
    let payload = JSON.stringify([
        {
          "PackageType": 0,
          "PackageCode": "ecom_shipping_shop_tvc",
          "PackageName": "Tự vận chuyển",
          "Carriers": [],
          "PackagePictureUrl": null,
          "IsUse": true,
          "IsUseAllProduct": null,
          "WarehouseLat": 0,
          "WarehouseLong": 0,
          "IsActiveMap": false,
          "WarehouseRegionId": 0,
          "FullShopAddress": null,
          "UpdatedDate": "/Date(1570059869000)/",
          "IsDisplayCarrier": false,
          "IsRequired": false,
          "RequiredType": 0
        },
        {
          "PackageType": 1,
          "PackageCode": "ecom_shipping_dispatch_cptc",
          "PackageName": "Chuyển phát tiêu chuẩn",
          "Carriers": [
            {
              "CarrierCode": "ecom_shipping_ghn",
              "CarrierName": "GHN",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/ghn.png",
              "PrivateCode": "ecom_shipping_dispatch_cptc_ecom_shipping_ghn"
            },
            {
              "CarrierCode": "ecom_shipping_njv_standard",
              "CarrierName": "NJV-STANDARD",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/ninjavan.png",
              "PrivateCode": "ecom_shipping_dispatch_cptc_ecom_shipping_njv_standard"
            },
            {
              "CarrierCode": "ecom_shipping_vnc_cpn",
              "CarrierName": "VNC-CPN",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vncpost.png",
              "PrivateCode": "ecom_shipping_dispatch_cptc_ecom_shipping_vnc_cpn"
            },
            {
              "CarrierCode": "ecom_shipping_vnpt",
              "CarrierName": "VNPost",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vnpt.png",
              "PrivateCode": "ecom_shipping_dispatch_cptc_ecom_shipping_vnpt"
            }
          ],
          "PackagePictureUrl": null,
          "IsUse": true,
          "IsUseAllProduct": null,
          "WarehouseLat": 0,
          "WarehouseLong": 0,
          "IsActiveMap": false,
          "WarehouseRegionId": 0,
          "FullShopAddress": null,
          "UpdatedDate": "/Date(1570166793000)/",
          "IsDisplayCarrier": true,
          "IsRequired": false,
          "RequiredType": 1
        },
        {
          "PackageType": 2,
          "PackageCode": "ecom_shipping_dispatch_cptk",
          "PackageName": "Chuyển phát bưu kiện",
          "Carriers": [
            {
              "CarrierCode": "ecom_shipping_viettel_cptk",
              "CarrierName": "VIETTEL-CPTK",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/viettel.png",
              "PrivateCode": "ecom_shipping_dispatch_cptk_ecom_shipping_viettel_cptk"
            },
            {
              "CarrierCode": "ecom_shipping_vnc_cptk",
              "CarrierName": "VNC-CPTK",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vncpost.png",
              "PrivateCode": "ecom_shipping_dispatch_cptk_ecom_shipping_vnc_cptk"
            },
            {
              "CarrierCode": "ecom_shipping_vnpt_cptk",
              "CarrierName": "VNPost-CPTK",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vnpt.png",
              "PrivateCode": "ecom_shipping_dispatch_cptk_ecom_shipping_vnpt_cptk"
            }
          ],
          "PackagePictureUrl": null,
          "IsUse": true,
          "IsUseAllProduct": null,
          "WarehouseLat": 0,
          "WarehouseLong": 0,
          "IsActiveMap": false,
          "WarehouseRegionId": 0,
          "FullShopAddress": null,
          "UpdatedDate": "/Date(1570166793000)/",
          "IsDisplayCarrier": true,
          "IsRequired": false,
          "RequiredType": 0
        },
        {
          "PackageType": 3,
          "PackageCode": "ecom_shipping_dispatch_cpn",
          "PackageName": "Chuyển phát trong ngày",
          "Carriers": [
            {
              "CarrierCode": "ecom_shipping_ship60_sd",
              "CarrierName": "SHIP60 SAMEDAY",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/ship60.png",
              "PrivateCode": "ecom_shipping_dispatch_cpn_ecom_shipping_ship60_sd"
            }
          ],
          "PackagePictureUrl": null,
          "IsUse": true,
          "IsUseAllProduct": null,
          "WarehouseLat": 0,
          "WarehouseLong": 0,
          "IsActiveMap": false,
          "WarehouseRegionId": 1,
          "FullShopAddress": null,
          "UpdatedDate": "/Date(1569813607000)/",
          "IsDisplayCarrier": true,
          "IsRequired": false,
          "RequiredType": 1
        },
        {
          "PackageType": 4,
          "PackageCode": "ecom_shipping_dispatch_instant",
          "PackageName": "Chuyển phát hỏa tốc",
          "Carriers": [
            {
              "CarrierCode": "ecom_shipping_grab",
              "CarrierName": "Grab",
              "IsUse": true,
              "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/grab.png",
              "PrivateCode": "ecom_shipping_dispatch_instant_ecom_shipping_grab"
            }
          ],
          "PackagePictureUrl": null,
          "IsUse": true,
          "IsUseAllProduct": null,
          "WarehouseLat": 10.7536879,
          "WarehouseLong": 106.7406081,
          "IsActiveMap": true,
          "WarehouseRegionId": 1,
          "FullShopAddress": "Tòa nhà FPT , đường số 8, kcx tân thuận2, Xã Xuân Thới Thượng, Huyện Hóc Môn, Hồ Chí Minh.",
          "UpdatedDate": "/Date(1570166793000)/",
          "IsDisplayCarrier": true,
          "IsRequired": false,
          "RequiredType": 1
        }
      ]);

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getListCarrierConfig(cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[3].api_get_list_carrier_config}`);
    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, "", { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.body);
    getListCarrierConfigTrend.add(res.timings.duration);
    if (check(res, {
        "Get List Carrier Config >> Status 200": (r) => r.status === 200
    })) {
        failedGetListCarrierConfigRate.add(false);
    } else {
        failedGetListCarrierConfigRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}