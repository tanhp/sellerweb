import http from "k6/http";
import { check, sleep } from "k6";
import { getCategoryByName } from "./Category.js";
import { getSellerApiUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

const config = JSON.parse(open("../../Config/configInfo.json"));

var loadProductListTrend = new Trend("load_product_list_duration",true);
var addNewProductTrend = new Trend("add_new_product_duration", true);
var editProductTrend = new Trend("edit_product_duration", true);
var deleteProductTrend = new Trend("delete_product_duration", true);
var changeStockTrend = new Trend("change_stock_duration", true);
var updateProductTrend = new Trend("update_product_duration", true);
var copyProductTrend = new Trend("copy_product_duration", true);
var failedLoadProductListRate = new Rate("failed_load_product_list_percentage",true);
var failedAddNewProductRate = new Rate("failed_add_new_product_percentage", true);
var failedEditProductRate = new Rate("failed_edit_product_percentage", true);
var failedDeleteProductRate = new Rate("failed_delete_product_percentage", true);
var failedChangeStockRate = new Rate("failed_change_stock_percentage", true);
var failedUpdateProductRate = new Rate("failed_update_product_percentage", true);
var failedCopyProductRate = new Rate("failed_copy_product_percentage", true);

export function postProduct(categoryName, access_token){
    let resCategory = getCategoryByName(categoryName, access_token);
    let resProductAttributes = getProductAttributes(resCategory.result[0].cate4, access_token);
    let payload = JSON.stringify({
      "Name": "[SPQC] Thức ăn cho cá K6",
      "StoreSku": getRandomSKU(),
      "Price": 100000,
      "UnitId": "Single",
      "Weight": 100,
      "StockAvailability": false,
      "StockQuantity": 100,
      "Description": "Mô tả sản phẩm chứa ít nhất 30 ký tự!",
      "Cat2Id": resCategory.result[0].cate2,
      "Cat3Id": resCategory.result[0].cate3,
      "Cat4Id": resCategory.result[0].cate4,
      "Attributes": resProductAttributes,
      "ProductImage": "http://media3.scdn.vn/img3/2019/4_9/SLcB2m.jpg",
      "special_price": null,
      "promotion_from_date": null,
      "promotion_to_date": null,
      "Variants": [],
      "IsConfigVariant": false,
      "Voucher": {
          "ProductType": 1,
          "StartDate": null,
          "EndDate": null,
          "IsCheckDate": false,
          "__moduleId__": "models/product/m.voucher"
        }
    });

    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }

    console.log(payload);

    let requestUrl = getSellerApiUrl(`${config.seller[1].api_product}`);
    let res = http.post(requestUrl, payload, { headers: headers });
    addNewProductTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "is status 200": (r) => r.status === 200
    })) {
        failedAddNewProductRate.add(false);
    } else {
        failedAddNewProductRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function putProduct(productId, categoryName, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_product}`);
  let resCategory = getCategoryByName(categoryName, access_token);
  let resProductAttributes = getProductAttributes(resCategory.result[0].cate4, access_token);
  let resProductDetail = getProductDetail(productId,access_token);

  let payload = JSON.stringify({
    "Id": productId,
    "Name": "[SPQC] Thức ăn cho cá K6",
    "StoreSku": getRandomSKU(),
    "Price": 100000,
    "UnitId": "Single",
    "Weight": 100,
    "StockAvailability": false,
    "StockQuantity": 100,
    "Description": "Mô tả sản phẩm chứa ít nhất 30 ký tự!",
    "Cat2Id": resCategory.result[0].cate2,
    "Cat3Id": resCategory.result[0].cate3,
    "Cat4Id": resCategory.result[0].cate4,
    "Attributes": resProductAttributes,
    "ProductImage": resProductDetail.result.productImage,
    "special_price": null,
    "promotion_from_date": null,
    "promotion_to_date": null,
    "Variants": [],
    "IsConfigVariant": false,
    "Voucher": {
      "ProductType": 1,
      "StartDate": null,
      "EndDate": null,
      "IsCheckDate": false,
      "__moduleId__": "models/product/m.voucher"
    },
    "VersionNo": resProductDetail.result.versionNo
  });

  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }

  let res = http.put(requestUrl, payload, { headers: headers });
  editProductTrend.add(res.timings.duration);
  console.log(res.status);
  if (check(res, {
      "is status 200": (r) => r.status === 200
  })) {
      failedEditProductRate.add(false);
  } else {
      failedEditProductRate.add(true);
  }
  let responseBody = JSON.parse(res.body);
  return responseBody;
}

export function delProduct(productId, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_product}`);
  let payload = JSON.stringify([productId]);
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.del(requestUrl, payload, { headers: headers });
  deleteProductTrend.add(res.timings.duration);
  console.log(res.status);
  if (check(res, {
      "is status 200": (r) => r.status === 200
  })) {
      failedDeleteProductRate.add(false);
  } else {
      failedDeleteProductRate.add(true);
  }
  let responseBody = JSON.parse(res.body);
  return responseBody;
}

export function postCopiedProduct(result, access_token){
  let payload = JSON.stringify({
    "Name": result.name,
    "StoreSku": getRandomSKU(),
    "Price": result.price,
    "UnitId": result.unitId,
    "Weight": result.weight,
    "StockAvailability": result.stockAvailability,
    "StockQuantity": 10,
    "Description": result.description,
    "Cat2Id": result.cat2Id,
    "Cat3Id": result.cat3Id,
    "Cat4Id": result.cat4Id,
    "Attributes": result.attributes,
    "ProductImage": result.productImage,
    "special_price": null,
    "promotion_from_date": null,
    "promotion_to_date": null,
    "Variants": result.variants,
    "IsConfigVariant": result.isConfigVariant,
    "Voucher": result.voucher,
    "VersionNo": result.versionNo
  });

  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }

  let requestUrl = getSellerApiUrl(`${config.seller[1].api_product}`);
  let res = http.post(requestUrl, payload, { headers: headers });
  console.log(res.status);
  console.log(res.body);
  let responseBody = JSON.parse(res.body);
  check(responseBody, {
    "Add new product >> Status 200": (r) => r.statusCode === 200
  });
  return responseBody;
}

export function getProductAttributes(cate4, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_get_attr_brand}`); 
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let resAttrBrand = http.get(requestUrl + cate4, { headers: headers })
  let responseBody = JSON.parse(resAttrBrand.body);
  let getAttributes = responseBody.result.attributeCollections;
  let attributes = [];
  for(var i=0; i<getAttributes.length; i++){
    let getAttribute = getAttributes[i];
    var getId = getAttribute.id;
    var getName = getAttribute.name;
    var getControlType = getAttribute.controlType;
    var getIsCheckout = getAttribute.isCheckout;

    var attribute = {};
    attribute["ID"] = getId;
    attribute["Name"] = getName;
    attribute["ControlType"] = getControlType;
    attribute["IsCheckout"] = getIsCheckout;

    let getAttributeValues = getAttribute.attributeValues;
    let attributeValues = [];

    for(var j=0; j < 1; j++){
      let getAttributeValue;
      var isSelected = null;
      if(getControlType=="TextBox"){
        getAttributeValue = getAttributeValues[0];
      }else{
        getAttributeValue = getAttributeValues[j];
        isSelected = true;
      }
      var getIdValue = getAttributeValue.id;

      var attributeValue = {};
      attributeValue["ID"] = getIdValue;
      attributeValue["Value"] = "Test";
      attributeValue["IsSelected"] = isSelected;

      attributeValues.push(attributeValue);
    }
    attribute["AttributeValues"] = attributeValues;
    attributes.push(attribute);
  }
  // if(checkout == true){
  //   console.log("Add attribute3 to attributes");
  //   var attribute3 = {};
  //   attribute3["ID"] = 7000;
  //   attribute3["Name"] = "ABCD";
  //   attribute3["ControlType"] = "CheckBox";
  //   attribute3["IsCheckout"] = true;
  //   attribute3["IsCustom"] = true;

  //   let attributeValues = [];
  //   var attributeValue = {};
  //   attributeValue["ID"] = 7001;
  //   attributeValue["Value"] = "1234";
  //   attributeValue["IsSelected"] = true;
  //   attributeValues.push(attributeValue);

  //   attribute3["AttributeValues"] = attributeValues;
  //   attributes.push(attribute3);
  // }
  console.log(JSON.stringify(attributes));
  return attributes;
}

export function getProductDetail(productId, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_product}`) + productId;
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.get(requestUrl, { headers: headers });
  let responseBody = JSON.parse(res.body);

  check(responseBody, {
    "Get Product Detail >> Status 200": (r) => r.statusCode === 200,
    "Get Product Detail >> Load product detail successfully": (r) => r.result.id === productId
  });

  return responseBody;
}

export function getProductList(productName, storeSku, productStatus, productStock, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[1].api_product_list}`);
    let payload = JSON.stringify({
      "CategoryId":"",
      "ProductName":productName,
      "ProductStatus":productStatus,
      "ProductStock":productStock,
      "StoreSKU":storeSku
    });
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(res.status);
    loadProductListTrend.add(res.timings.duration);
    if (check(res, {
        "Get Product List >> Status 200": (r) => r.status === 200
    })) {
        failedLoadProductListRate.add(false);
    } else {
        failedLoadProductListRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function stockAvailability(productId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[1].api_change_stock}`);
    let resProductDetail = getProductDetail(productId,access_token);
    let payload = JSON.stringify({
        "Id":productId,
        "ProductStock":false,
        "VersionNo": resProductDetail.result.versionNo
    });
    console.log(payload);
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.post(requestUrl, payload, { headers: headers });
    changeStockTrend.add(res.timings.duration);
    console.log(res.status);
    if (check(res, {
        "is status 200": (r) => r.status === 200
    })) {
        failedChangeStockRate.add(false);
    } else {
        failedChangeStockRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function updateProduct(productId, access_token){
    let requestUrl = getSellerApiUrl(`${config.seller[1].api_update_products}`);
    let resProductDetail = getProductDetail(productId,access_token);
    let payload = JSON.stringify([{
        "Id":productId,
        "ProductName":"[SPQC] Thức ăn cho cá K6",
        "ProductSku":getRandomSKU(),
        "ProductPrice":"100000",
        "StockQuantity":"12",
        "VersionNo": resProductDetail.result.versionNo
    }]);
    let headers = {
      "Content-Type": "application/json",
      "Access": "application/json",
      "Authorization": access_token
    }
    let res = http.put(requestUrl, payload, { headers: headers });
    updateProductTrend.add(res.timings.duration);
    console.log(res.status);
    if (check(res, {
        "is status 200": (r) => r.status === 200
    })) {
        failedUpdateProductRate.add(false);
    } else {
        failedUpdateProductRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function copyProduct(productId, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_copy}`) + productId;
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.get(requestUrl, { headers: headers });
  copyProductTrend.add(res.timings.duration);
  console.log(res.status);
  if (check(res, {
      "is status 200": (r) => r.status === 200
  })) {
      failedCopyProductRate.add(false);
  } else {
      failedCopyProductRate.add(true);
  }
  let responseBody = JSON.parse(res.body);
  return responseBody;
}

export function getProductListForClone(access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_product_list_for_clone}`);
  let payload = JSON.stringify({
    "ProductName":"thức ăn và dinh dưỡng cho cá",
    "CateId": 0,
    "CategoryID":0,
    "SortDesc":null,
    "OrderBy":null
  });
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.post(requestUrl, payload, { headers: headers });
  console.log(res.status);
  let responseBody = JSON.parse(res.body);
  check(responseBody, {
    "Get Product List For Clone >> Status 200": (r) => r.statusCode === 200
  });
  return responseBody;
}

export function getRandomSKU(){
    var alphabet           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    var result = characters.charAt(Math.floor(Math.random() * charactersLength));
    return "K6" + result + Math.floor(Math.random()*1000);    
}

export function getToday(){
  let current_datetime = new Date();
  let formatted_date = current_datetime.getDate() + "/" + (current_datetime.getMonth() + 1) + "/" + current_datetime.getFullYear();
  console.log(formatted_date);
  return formatted_date;
}

