import http from "k6/http";
import { getSellerWebUrl } from "../general.js";
import { check, sleep } from "k6";
import { Trend, Rate } from "k6/metrics";

const config = JSON.parse(open("../../Config/configInfo.json"));
var printSalesOrderTrend = new Trend("print_sales_order_duration",true);
var updatedSoPrintedStatusTrend = new Trend("updated_so_printed_status_duration",true);
var failedPrintSalesOrderRate = new Rate("failed_print_sales_order_percentage",true);
var failedUpdatedSoPrintedStatusRate = new Rate("failed_updated_so_printed_status_percentage",true);

export function printSalesOrder(orderIds, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_print_sales_order}`);
    let payload = JSON.stringify({
        "ids": orderIds
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    printSalesOrderTrend.add(res.timings.duration);
    console.log(res.status);
    if (check(res, {
        "Print Sales Order >> Status 200": (r) => r.status === 200
    })) {
        failedPrintSalesOrderRate.add(false);
    } else {
        failedPrintSalesOrderRate.add(true);
    }
    return res;
}

export function updatedSoPrintedStatus(orderIds, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_updated_so_printed_status}`);

    let payload = JSON.stringify({
        "orderIds": orderIds
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    updatedSoPrintedStatusTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Updated So Printed Status >> Status 200": (r) => r.status === 200
    })) {
        failedUpdatedSoPrintedStatusRate.add(false);
    } else {
        failedUpdatedSoPrintedStatusRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}