import http from "k6/http";
import { check, sleep } from "k6";
import { getToken } from "./GetToken.js";
import { getSellerWebUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

var getDropOffConfigTrend = new Trend("get_dropoff_config_trend", true);
var failedGetDropOffConfigRate = new Rate("failed_get_dropoff_config_percentage",true);

const config = JSON.parse(open("../../Config/configInfo.json"));

export function saveShopInfo(access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[3].api_update_dropoff_config}`);
    let resShopInfo = getShopInfo(access_token);
    let payload = JSON.stringify([
        {
            "Carriers": [
              {
                "CarrierCode": "ecom_shipping_ghn",
                "CarrierName": "GHN",
                "IsUse": false,
                "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/ghn.png",
                "PrivateCode": null
              },
              {
                "CarrierCode": "ecom_shipping_vnc_cpn",
                "CarrierName": "VNC-CPN",
                "IsUse": false,
                "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vncpost.png",
                "PrivateCode": null
              },
              {
                "CarrierCode": "ecom_shipping_vnpt_cptk",
                "CarrierName": "VNPost-CPTK",
                "IsUse": false,
                "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vnpt.png",
                "PrivateCode": null
              },
              {
                "CarrierCode": "ecom_shipping_vnc_cptk",
                "CarrierName": "VNC-CPTK",
                "IsUse": false,
                "CarrierLogoUrl": "/Content/images/design/logo/carriers/large/vncpost.png",
                "PrivateCode": null
              }
            ],
            "IsUse": false,
            "StoreId": 141736,
            "UpdatedDate": "/Date(1568708635000)/"
          }
      ]);

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
        "Authorization": access_token
    }

    let res = http.post(requestUrl, payload, { headers: headers });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getDropOffConfig(cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[3].api_get_dropoff_config}`);

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, "", { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.body);
    getDropOffConfigTrend.add(res.timings.duration);
    if (check(res, {
        "Get DropOff Config >> Status 200": (r) => r.status === 200
    })) {
        failedGetDropOffConfigRate.add(false);
    } else {
      failedGetDropOffConfigRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}