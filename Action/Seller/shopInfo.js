import http from "k6/http";
import { check, sleep } from "k6";
import { getToken } from "./GetToken.js";
import { getSellerWebUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";

var saveShopInfoTrend = new Trend("save_shopinfo_duration", true);
var failedSaveShopInfoRate = new Rate("failed_save_shopinfo_percentage",true);

const config = JSON.parse(open("../../Config/configInfo.json"));
 
export function getDistrictByRegionId(regionId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[4].api_get_district_by_region_id}`);
    let formData = {
        regionId: regionId
    }
  
    let headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }

    console.log(cookies.sendoId[0].value);
    let res = http.post(requestUrl, formData, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getDistrictWardByDistrictId(districtId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[4].api_get_district_ward_by_district_id}`);
    let formData = {
        districtId: districtId
    }
  
    let headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }

    console.log(cookies.sendoId[0].value);
    let res = http.post(requestUrl, formData, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.body);
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function saveShopInfo(cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[4].api_save_shop_info}`);
    let resShopInfo = getShopInfo(cookies);
    let payload = JSON.stringify({
        "Id": resShopInfo.Data.Id,
        "Name": resShopInfo.Data.Name,
        "ShopLogo": resShopInfo.Data.ShopLogo,
        "ShopCover": resShopInfo.Data.ShopCover,
        "ShopBackground": resShopInfo.Data.ShopBackground,
        "Slogan": resShopInfo.Data.Slogan,
        "ShopDescription": resShopInfo.Data.ShopDescription,
        "Code": resShopInfo.Data.Code,
        "ShopRepresentative": resShopInfo.Data.ShopRepresentative,
        "Email": resShopInfo.Data.Email,
        "Mobile": resShopInfo.Data.Mobile,
        "Phone": resShopInfo.Data.Phone,
        "ShopAddress": resShopInfo.Data.ShopAddress,
        "ShopRegionId": resShopInfo.Data.ShopRegionId,
        "ShopDistrictId": resShopInfo.Data.ShopDistrictId,
        "ShopWardId": resShopInfo.Data.ShopWardId,
        "ShopWard": resShopInfo.Data.ShopWard,
        "WarehouseAddress": resShopInfo.Data.WarehouseAddress,
        "WarehouseRegionId": resShopInfo.Data.WarehouseRegionId,
        "WarehouseDistrictId": resShopInfo.Data.WarehouseDistrictId,
        "WarehouseWardId": resShopInfo.Data.WarehouseWardId,
        "WarehouseWard": resShopInfo.Data.WarehouseWard,
        "SystemURL": resShopInfo.Data.SystemURL,
        "ShopStatus": resShopInfo.Data.ShopStatus,
        "VersionNo": resShopInfo.Data.VersionNo,
        "AllowCheckOrder": resShopInfo.Data.AllowCheckOrder,
        "ShopFullAddress": resShopInfo.Data.ShopFullAddress,
        "IsSelfShipping": resShopInfo.Data.IsSelfShipping,
        "IsClosedStore": resShopInfo.Data.IsClosedStore,
        "TaxNumber": resShopInfo.Data.TaxNumber,
        "WarehouseLat": resShopInfo.Data.WarehouseLat,
        "WarehouseLong": resShopInfo.Data.WarehouseLong,
        "TransportTypeName": resShopInfo.Data.TransportTypeName,
        "UserName": resShopInfo.Data.UserName,
        "IsSortingCenter": resShopInfo.Data.IsSortingCenter,
        "TransportTypeCode": resShopInfo.Data.TransportTypeCode
      });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json",
    }

    console.log(cookies.sendoId[0].value);
    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    console.log(res.body);
    saveShopInfoTrend.add(res.timings.duration);
    if (check(res, {
        "Get Product List >> Status 200": (r) => r.status === 200
    })) {
        failedSaveShopInfoRate.add(false);
    } else {
        failedSaveShopInfoRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getShopInfo(access_token){
    let requestUrl = getSellerWebUrl(`${config.seller[4].api_get_shop_info}`);
    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, "",{ headers: headers, cookies: {"token_seller_api": access_token} });
    console.log(res.body);
    saveShopInfoTrend.add(res.timings.duration);
    if (check(res, {
        "Get Product List >> Status 200": (r) => r.status === 200
    })) {
        failedSaveShopInfoRate.add(false);
    } else {
        failedSaveShopInfoRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    // getDistrictByRegionId(responseBody.Data.ShopRegionId, cookies);
    // getDistrictWardByDistrictId(responseBody.Data.ShopDistrictId, cookies);
    // getDistrictByRegionId(responseBody.Data.WarehouseRegionId, cookies);
    // getDistrictWardByDistrictId(responseBody.Data.WarehouseDistrictId, cookies);
    return responseBody;
}