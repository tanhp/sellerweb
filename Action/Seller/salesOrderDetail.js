import http from "k6/http";
import { getSellerApiUrl, getSellerWebUrl } from "../general.js";
import { Trend, Rate } from "k6/metrics";
import { check, sleep } from "k6";

const config = JSON.parse(open("../../Config/configInfo.json"));
var getOrderDetailTrend = new Trend("get_order_detail_duration",true);
var getShipmentTrackingLogTrend = new Trend("get_shipment_tracking_log_duration",true);
var getSaleOrderNoteTrend = new Trend("get_sale_order_note_duration",true);
var getCallLogTrend = new Trend("get_call_log_duration",true);
var getBuyerSummaryInfoTrend = new Trend("get_buyer_summary_info_duration",true);
var getClaimDetailTrend = new Trend("get_claim_detail_duration",true);
var failedOrderDetailRate = new Rate("failed_get_order_detail_percentage",true);
var failedShipmentTrackingLogRate = new Rate("failed_get_shipment_tracking_log_percentage",true);
var failedSaleOrderNoteRate = new Rate("failed_sale_order_note_percentage",true);
var failedCallLogRate = new Rate("failed_get_call_log_percentage",true);
var failedBuyerSummaryInfoRate = new Rate("failed_get_buyer_summary_info_percentage",true);
var failedClaimDetailRate = new Rate("failed_get_claim_detail_percentage",true);

export function getOrderDetail(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_order_detail}`);

    let payload = JSON.stringify({
        "OrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getOrderDetailTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Order Detail >> Status 200": (r) => r.status === 200
    })) {
        failedOrderDetailRate.add(false);
    } else {
        failedOrderDetailRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getShipmentTrackingLog(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_shipment_tracking_log}`);
    let payload = JSON.stringify({
        "OrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getShipmentTrackingLogTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Shipment Tracking Log >> Status 200": (r) => r.status === 200
    })) {
        failedShipmentTrackingLogRate.add(false);
    } else {
        failedShipmentTrackingLogRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getSaleOrderNote(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_sale_order_note}`);
    let payload = JSON.stringify({
        "saleOrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getSaleOrderNoteTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Sale Order Note >> Status 200": (r) => r.status === 200
    })) {
        failedSaleOrderNoteRate.add(false);
    } else {
        failedSaleOrderNoteRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getCallLog(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_call_log}`);
    let payload = JSON.stringify({
        "OrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getCallLogTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Call Log >> Status 200": (r) => r.status === 200
    })) {
        failedCallLogRate.add(false);
    } else {
        failedCallLogRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getBuyerSummaryInfo(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_buyer_summary_info}`);
    let payload = JSON.stringify({
        "OrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getBuyerSummaryInfoTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Buyer Summary Info >> Status 200": (r) => r.status === 200
    })) {
        failedBuyerSummaryInfoRate.add(false);
    } else {
        failedBuyerSummaryInfoRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getClaimDetail(orderId, cookies){
    let requestUrl = getSellerWebUrl(`${config.seller[2].api_get_claim_detail}`);
    let payload = JSON.stringify({
        "OrderId": orderId
    });

    let headers = {
        "Content-Type": "application/json",
        "Access": "application/json"
    }

    let res = http.post(requestUrl, payload, { headers: headers, cookies: {"sendoId": cookies.sendoId[0].value} });
    getClaimDetailTrend.add(res.timings.duration);
    console.log(res.status);
    console.log(res.body);
    if (check(res, {
        "Get Claim Detail >> Status 200": (r) => r.status === 200
    })) {
        failedClaimDetailRate.add(false);
    } else {
        failedClaimDetailRate.add(true);
    }
    let responseBody = JSON.parse(res.body);
    return responseBody;
}

export function getSalesOrderDetail(orderId, access_token){
    getOrderDetail(orderId, access_token);
    getShipmentTrackingLog(orderId, access_token);
    getSaleOrderNote(orderId, access_token);
    getCallLog(orderId, access_token);
    getBuyerSummaryInfo(orderId, access_token);
    getClaimDetail(orderId, access_token);
}