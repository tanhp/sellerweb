import http from "k6/http";
import { getSellerApiUrl } from "../general.js";

const config = JSON.parse(open("../../Config/configInfo.json"));

export function getCategoryByName(categoryName, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_get_category_by_name}`);
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.get(requestUrl + categoryName, { headers: headers });
  console.log(res.body);
  let responseBody = JSON.parse(res.body);
  console.log(responseBody.result[0].cate2);
  console.log(responseBody.result[0].cate3);
  console.log(responseBody.result[0].cate4);
  return responseBody;
}

export function getCategoriesByParentId(parentId, access_token){
  let requestUrl = getSellerApiUrl(`${config.seller[1].api_get_categories_by_parentid}`);
  let headers = {
    "Content-Type": "application/json",
    "Access": "application/json",
    "Authorization": access_token
  }
  let res = http.get(requestUrl + parentId, { headers: headers });
  let responseBody = JSON.parse(res.body);
  return responseBody;
}
