import http from "k6/http";
import { Trend, Rate } from "k6/metrics";
import { check } from "k6";
import { getCheckoutUrl } from "../general.js"

var saveOrderTrend = new Trend("save_order_duration", true);
var failedSaveOrderRate = new Rate("failed_save_order_percentage", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function saveOrder(addressId, shopId, productId, shippingPackage, paymentMethod, accessToken, hash) {
    let requestUrl = getCheckoutUrl(`${config.api_save_order}`);
    var payload = JSON.stringify(
        {
            "item_hash": hash,
            "shop_id": shopId,
            "current_carrier": shippingPackage,
            "sendo_platform": "ios",
            "device_id": "38857E45-2D4C-4FFC-AB9A-5533C28EFFB7",
            "current_payment_method": {
                "method": paymentMethod
            },
            "current_products": [{
                "final_price": 200000,
                "product_id": productId,
                "hash": hash
            }],
            "current_address_id": addressId
        });

    let requestHeaders = {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": accessToken
    };

    console.log("Save order");
    let response = http.post(requestUrl, payload, { headers: requestHeaders });
    let responseBody = JSON.parse(response.body);
    console.log(responseBody.increment_id);
    saveOrderTrend.add(response.timings.duration);
    paymentMethod = paymentMethod.split("_")[0];
    
    if (check(response, {
        "Save order > status should be 200": (r) => r.status === 200,
        "Save order > order successfully created": responseBody.increment_id > 0,
        "Save order > payment method should be correct": responseBody.payment_type === paymentMethod
    })) {
        failedSaveOrderRate.add(false);
    } else {
        failedSaveOrderRate.add(true);
    }

    return responseBody.increment_id;
}