import http from "k6/http";
import { Trend, Rate } from "k6/metrics";
import { check } from "k6";
import { getCheckoutUrl } from "../general.js"

var checkoutInfoTrend = new Trend("checkout_info_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function checkoutInfo(shopId, accessToken, hash) {
    let requestUrl = getCheckoutUrl(`${config.api_checkout_info}`);
    var payload = JSON.stringify(
        {
            "shop_id": shopId,
            "product_hashes": [hash],
            "sendo_platform": "desktop2"
        });

    let requestHeaders = {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": accessToken
    };

    console.log("Get checkout info");
    let response = http.post(requestUrl, payload, { headers: requestHeaders });
    checkoutInfoTrend.add(response.timings.duration);
    let responseBodyString = String(response.body);

    check(response, {
        "Checkout info > status should be 200": (r) => r.status === 200,
        "Checkout info > user info is successfully loaded": responseBodyString.includes("customer_data") && responseBodyString.includes("customer_id")
    });
    return response;
}