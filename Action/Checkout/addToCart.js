import http from "k6/http";
import { Trend, Rate } from "k6/metrics";
import { check } from "k6";
import { getSendoUrl } from "../general.js"

var cartTrend = new Trend("add_to_cart_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function addToCart(shopId, productId, accessToken) {
    let requestUrl = getSendoUrl(`${config.api_add_to_cart}`);
    var productOption = '{"' + productId + '_284":["' + productId + '_602"],"' + productId + '_298":["' + productId + '_818"]}';
    var productOptionObject = JSON.parse(productOption);

    var payload = JSON.stringify(
        {
            "from_source": 3,
            "shop_id": shopId,
            "buynow": 1,
            "device_id": "38857E45-2D4C-4FFC-AB9A-5533C28EFFB7",
            "qty": 1,
            "product_id": productId,
            "options": productOptionObject,
            "source_block_id": "",
            "source_info": "",
            "source_page_id": "",
            "source_url": ""
        });

    let requestHeaders = {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": accessToken
    };
    console.log("Add to cart with product id: " + productId);
    let response = http.post(requestUrl, payload, { headers: requestHeaders });
    let responseBody = JSON.parse(response.body);
    cartTrend.add(response.timings.duration);

    check(response, {
        "Add to cart > status should be 200": (r) => r.status === 200,
        "Add to cart > add to cart successfully, error should be false": responseBody.result.data.error === false,
        "Add to cart > hash is generated": responseBody.result.data.hash != null
    });

    var hash = responseBody.result.data.hash;
    return hash;
}