import http from "k6/http";
const config = JSON.parse(open("../Config/configInfo.json"));

export function getSendoUrl(apiPath) {
    let host;
    if( __ENV.ENVIRONMENT === 'PRODUCTION' ) {
        host = config.production.base_url;
    } else if (__ENV.ENVIRONMENT === 'STAGING') {
        host = config.staging.base_url;
    } else if (__ENV.ENVIRONMENT === 'TEST') {
        host = config.test.base_url;
    }
    return `${host}${apiPath}`;
}

export function getCheckoutUrl(apiPath) {
    let host;
    if( __ENV.ENVIRONMENT === 'PRODUCTION' ) {
        host = config.production.checkout_base_url;
    } else if (__ENV.ENVIRONMENT === 'STAGING') {
        host = config.staging.checkout_base_url;
    } else if (__ENV.ENVIRONMENT === 'TEST') {
        host = config.test.checkout_base_url;
    }
    return `${host}${apiPath}`;
}

export function getSellerWebUrl(apiPath){
    let host;
    if( __ENV.ENVIRONMENT === 'PRODUCTION' ) {
        host = config.production.seller_web_base_url;
    } else if (__ENV.ENVIRONMENT === 'STAGING') {
        host = config.staging.seller_web_base_url;
    } else if (__ENV.ENVIRONMENT === 'TEST') {
        host = config.test.seller_web_base_url;
    }
    console.log(`${host}${apiPath}`);
    return `${host}${apiPath}`;
}

export function getSellerApiUrl(apiPath){
    let host;
    if( __ENV.ENVIRONMENT === 'PRODUCTION' ) {
        host = config.production.seller_api_base_url;
    } else if (__ENV.ENVIRONMENT === 'STAGING') {
        host = config.staging.seller_api_base_url;
    } else if (__ENV.ENVIRONMENT === 'TEST') {
        host = config.test.seller_api_base_url;
    }
    console.log(`${host}${apiPath}`);
    return `${host}${apiPath}`;
}

export function getMinValue(currentValue, nextValue) {
    if(currentValue > nextValue) {
        return nextValue;
    } else {
        return currentValue;
    }
}

export function getMaxValue(currentValue, nextValue) {
    if(currentValue < nextValue) {
        return nextValue;
    } else {
        return currentValue;
    }
}

export function getRandomNumberInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function sellerAuthentication(username, password) {
    let requestUrl = `${config.api_seller_authentication}`;
    let payload = "grant_type=password&username=" + username + "&password=" + password;
    let requestHeaders = {
        "Content-type": "application/x-www-form-urlencoded"
    };
    let response = http.post(requestUrl, payload, { headers: requestHeaders });
    let responseBody = JSON.parse(response.body);
    return responseBody.access_token;
}

export function getProductDetail(productId, bearer) {
    let requestUrl = `${config.api_seller_get_product_detail}` + "/" + productId;

    let requestHeaders = {
        "Accept": "application/json",
        "Content-type": "application/json",
        "Authorization": "Bearer " + bearer
    };
    let response = http.get(requestUrl, { headers: requestHeaders });
}

export function setProductAvailable(products) {
    let bearer = "Bearer " + sellerAuthentication("607e4bb038ad4167afd68cd39b986dc6", "9397f820941f37f2d28383cc419ea34632022b54");
    let requestUrl = `${config.api_seller_config_variant}`;
    let requestHeaders = {
        "Content-type": "application/json",
        "Authorization": bearer
    };

    var i;
    for (i = 0; i < products.length; i++) {
        console.log("Set product available: " + products[i].product_id);
        let payload = '[{"isConfigVariant":true,"StockAvailability":true,"ProductId":' + products[i].product_id + ',"variants":[{"price":200000,"qty":99999,"Attributes":[{"attributeId":298,"attributeCode":"kich_thuoc_1","optionId":818},{"attributeId":284,"attributeCode":"mau_sac","optionId":602}],"SkuUser":"XL_Nâu"}]}]';
        let response = http.post(requestUrl, payload, { headers: requestHeaders });
    }
}

export function createAccount() {
    let requestUrl = `${config.base_url}` + `${config.api_register}`;
    let payload = JSON.stringify({
        email: "abcd0129323@gmail.com",
        firstname: "AC",
        lastname: "account",
        source: 1,
        password: "123456",
        re_password: "123456"
    });

    let requestHeaders = {
        "Accept": "application/json",
        "Content-type": "application/json"
    };

    let response = http.post(requestUrl, payload, { headers: requestHeaders });
}

export function getTokenFromArray(index, tokens){
    var token = tokens.split(",");
    return token[index];
}