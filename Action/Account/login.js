import http from "k6/http";
import { Trend, Rate } from "k6/metrics";
import { check } from "k6";
import { getSendoUrl } from "../general.js"

var loginTrend = new Trend("login_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function login(username, password) {
    let requestUrl = getSendoUrl(`${config.api_login}`);
    let payload = "&username=" + username + "&password=" + password;
    let requestHeaders = {
        "Content-Type": "application/x-www-form-urlencoded"
    };
    console.log("Login with username: " + username);
    let response = http.post(requestUrl, payload, { headers: requestHeaders });
    loginTrend.add(response.timings.duration);
    let responseBody = JSON.parse(response.body);
    var accessToken = responseBody.user_info.token;

    check(response, {
        "Login > status should be 200": (r) => r.status === 200,
        "Login > login successfully, error should be 0": responseBody.error === 0,
        "Login > token is generated": responseBody.user_info.token != null
    });
    return accessToken;
}