import http from "k6/http";
import { Trend } from "k6/metrics";
import { check } from "k6";
import { getSendoUrl } from "../general.js"

var searchTrend = new Trend("search_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function search(keyword, sortType, page) {
    let requestUrl = getSendoUrl(`${config.api_search}`) + "&p=" + page + "&sortType=" + sortType + "&q=" + keyword;
    console.log("Search with keyword: " + keyword);
    let response = http.get(requestUrl);
    searchTrend.add(response.timings.duration);
    let responseBody = JSON.parse(response.body);
    
    check(response, {
        "Search > status should be 200": (r) => r.status === 200
    });
}