import http from "k6/http";
import { Trend } from "k6/metrics";
import { check } from "k6";
import { getSendoUrl } from "../general.js"

var productDetailTrend = new Trend("load_product_detail_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function loadProductDetail(productId, productPath, shopId) {
    let requestUrl = getSendoUrl(`${config.api_get_product_detail}`) + productPath + "?platform=web";
    console.log("Get product detail with product path: " + productPath);
    let response = http.get(requestUrl);
    productDetailTrend.add(response.timings.duration);
    let responseBody = JSON.parse(response.body);
    
    check(response, {
        "Load Product Detail > status should be 200": (r) => r.status === 200,
        "Load Product Detail > product id should be correct": responseBody.result.data.id === productId,
        "Load Product Detail > shop id should be correct": responseBody.result.data.shop_info.shop_id === shopId
    });
}