import http from "k6/http";
import { Trend } from "k6/metrics";
import { check } from "k6";
import { getSendoUrl } from "../general.js"

var loadCartInfoTrend = new Trend("load_cart_info_duration", true);
const config = JSON.parse(open("../../Config/configInfo.json"));

export default function loadCartInfo(token) {
    let requestUrl = getSendoUrl(`${config.api_load_cart_info}`);
    console.log("Load cart info");
    let response = http.get(requestUrl);
    loadCartTrend.add(response.timings.duration);
    let responseBody = JSON.parse(response.body);
    
    check(response, {
        "Load Cart Info > status should be 200": (r) => r.status === 200
    });
}