import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";
import { getOrderList, getOrderListCookies } from "../../Action/Seller/salesOrder.js";
import { getShipmentTrackingLog, getSaleOrderNote, getCallLog, getBuyerSummaryInfo, getClaimDetail, getOrderDetail, getOrderDetailCookies } from "../../Action/Seller/salesOrderDetail.js";

// export let options = {
//     vus: 2,
//     duration: "5s"
// };

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    __ENV.COOKIES = JSON.stringify(getCookies("sendovn03@gmail.com","0903167673"));
}

export default function() {
    group("Load Sales Order Detail", function () {
        let cookies;
        let orders;

        group("Step 1: Login", function () {
            cookies = JSON.parse(__ENV.COOKIES);
        });

        group("Step 2: Load Order List", function () {
            let resOrderList = getOrderList(cookies);
            orders = resOrderList.data.SalesOrders;
            for(var i=0; i<orders.length; i++){
                var orderId = orders[i].Id;
                console.log(orderId);
            }

        });
    
        group("Step 3: Get Order Detail", function () {
            getOrderDetail(orders[0].Id, cookies);
        });

        group("Step 4: Get Shipment Tracking Log", function () {
            getShipmentTrackingLog(orders[0].Id, cookies);
        });

        group("Step 5: Get Order Note", function () {
            getSaleOrderNote(orders[0].Id, cookies);
        });

        group("Step 6: Get Call Log", function () {
            getCallLog(orders[0].Id, cookies);
        });

        group("Step 7: Get Buyer Summary Info", function () {
            getBuyerSummaryInfo(orders[0].Id, cookies);
        });
        
        group("Step 8: Get Claim Detail", function () {
            getClaimDetail(orders[0].Id, cookies);
        });


        sleep(1);
    });
}

