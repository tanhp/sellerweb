import { check, fail, sleep, group } from "k6";
import { getToken, login, logOut, relogin, getStoreName, getCookies } from "../../Action/Seller/getToken.js";
import { getProductList, getProductDetail, postProduct } from "../../Action/Seller/product.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers2.json"));

export let options = {
    vus: 5,
    duration: "1s"
};

export function setup(){
    
}

export default function() {
    group("Login", function () {
        let randomUser = getRandomNumberInRange(0, users.length - 1);
        let response = login(`${users[randomUser].username}`,`${users[randomUser].password}`,`${users[randomUser].shopId}`);
        var storeName = response.cookies.storeid[0].value;
        console.log(storeName);
        relogin(storeName);
    });
}