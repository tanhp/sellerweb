import { check, fail, sleep, group } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { postBanner, getBannerList, getBannerDetail, putBanner, delBanner } from "../../Action/Seller/banner.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Edit banner", function () {
        var access_token;
        var bannerId;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });

        group("Step 2: Load banner list", function () {
            let resBannerList = getBannerList(access_token);
            bannerId = resBannerList.result.data[0].id;
        });

        group("Step 3: Edit banner", function () {
            putBanner(bannerId, access_token);
        });
    });
}