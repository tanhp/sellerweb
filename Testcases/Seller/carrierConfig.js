import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { saveShopInfo, getShopInfo } from "../../Action/Seller/shopInfo.js";
import { getListCarrierConfig } from "../../Action/Seller/carrierConfig.js";

export let options = {
    vus: 100,
    duration: "5s"
};

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    __ENV.COOKIES = JSON.stringify(getCookies("0900000444","123456"));
}

export default function() {

    group("Load Shop Info", function () {
        let cookies;

        group("Step 1: Login", function () {
            cookies = JSON.parse(__ENV.COOKIES);
        });
    
        group("Step 2: Get Carrier Config", function () {
            getListCarrierConfig(cookies)
        });
        
        sleep(1);
    });
}

