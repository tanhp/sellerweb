import { sleep } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { getCategoriesByParentId } from "../../Action/Seller/category.js";

export function setup(){
    __ENV.TOKEN = getToken("tanhopham1990@gmail.com", "19901309");
}

export default function() {
    console.log("Step 1: Login");
    var access_token = __ENV.TOKEN;
    console.log(access_token);

    console.log("Step 2: Get Category");
    let resCategory = getCategoriesByParentId(94,access_token);
    console.log(JSON.stringify(resCategory));
    sleep(1);
}