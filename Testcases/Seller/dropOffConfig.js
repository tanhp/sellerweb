import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { getDropOffConfig } from "../../Action/Seller/dropOffConfig.js";

export function setup(){
    __ENV.COOKIES = JSON.stringify(getCookies("0900000444","123456"));
}

export default function() {

    group("Load Shop Info", function () {
        let cookies;

        group("Step 1: Login", function () {
            cookies = JSON.parse(__ENV.COOKIES);
        });
    
        group("Step 2: Get DropOff Config", function () {
            getDropOffConfig(cookies);
        });
        
        sleep(1);
    });
}

