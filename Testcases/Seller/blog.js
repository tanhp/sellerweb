import { check, fail, sleep, group } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { postBlog, getBlogList, getBlogDetail, putBlog, delBlog } from "../../Action/Seller/blog.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Edit blog", function () {
        var access_token;
        var blogId;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });

        group("Step 2: Load blog list", function () {
            let resBlog = getBlogList(access_token);
            blogId = resBlog.result.data[0].id;
        });

        group("Step 3: Load Blog Detail", function () {
            getBlogDetail(blogId, access_token);
        });

        group("Step 4: Edit blog", function () {
            putBlog(blogId, access_token);
        });
    });
}