import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { getOrderList } from "../../Action/Seller/salesOrder.js";
import { printSalesOrder, updatedSoPrintedStatus } from "../../Action/Seller/bill.js";

// export let options = {
//     vus: 10,
//     duration: "5s"
// };

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    __ENV.COOKIES = JSON.stringify(getCookies("sendovn03@gmail.com","0903167673"));
}

export default function() {
    group("Load Sales Product list", function () {
        let cookies;
        var orderId = "40367452";

        group("Step 1: Login", function () {
            cookies = JSON.parse(__ENV.COOKIES);
        });
    
        group("Step 2: Load SalesOrder list", function () {
            getOrderList(cookies);      
        });

        group("Step 3: Print Sales Order", function () {
            printSalesOrder(orderId, cookies);
        });

        group("Step 4: Update Printed Sales Order", function () {
            updatedSoPrintedStatus(orderId, cookies);
        });
        
        sleep(1);
    });
}

