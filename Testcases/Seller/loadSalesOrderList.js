import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";
import { getOrderList } from "../../Action/Seller/salesOrder.js";

// export let options = {
//     vus: 10,
//     duration: "5s"
// };

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    __ENV.COOKIES = JSON.stringify(getCookies("sendovn03@gmail.com","0903167673"));
}

export default function() {
    group("Load Sales Product list", function () {
        let cookies;

        group("Step 1: Login", function () {
            cookies = JSON.parse(__ENV.COOKIES);
        });
    
        group("Step 2: Load SalesOrder list", function () {
            getOrderList(cookies);
        });
        
        sleep(1);
    });
}

