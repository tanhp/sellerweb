import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";
import { loadPrivateOfferList } from "../../Action/Seller/privateOffer.js";

// export let options = {
//     vus: 10,
//     duration: "5s"
// };

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Load PrivateOffer list", function () {
        let access_token;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });
    
        group("Step 2: Load PrivateOffer list", function () {
            loadPrivateOfferList(access_token);
        });
        
        sleep(1);
    });
}

