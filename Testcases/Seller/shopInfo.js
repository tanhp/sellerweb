import { check, fail, sleep, group } from "k6";
import { getToken, getCookies } from "../../Action/Seller/getToken.js";
import { saveShopInfo, getShopInfo } from "../../Action/Seller/shopInfo.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

export let options = {
    vus: 2,
    duration: "5s"
};

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {

    group("Load Shop Info", function () {
        var access_token;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });
    
        group("Step 2: Get Shop Info", function () {
            getShopInfo(access_token);
        });
        
        sleep(1);
    });
}

