import { check, fail, sleep, group } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { copyProduct, getProductListForClone, postCopiedProduct } from "../../Action/Seller/product.js";
import { getProductList, getProductDetail, postProduct } from "../../Action/Seller/product.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Copy Product", function () {
        var access_token;
        var productId;
        let result;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });

        group("Step 2: Search Product For Clone", function () {
            let resProductClone = getProductListForClone(access_token);
            productId = resProductClone.result.data[0].id;
        });
        
        group("Step 3: Copy Product", function () {
            let resProductList = copyProduct(productId,access_token);
            result = resProductList.result;
        });

        group("Step 4: Add New Product", function () {
            let resProductList = postCopiedProduct(result,access_token);
        });

        sleep(1);
    });
}