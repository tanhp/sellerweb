import { check, fail, sleep, group } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { getProductList, getProductDetail } from "../../Action/Seller/product.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Load Product Detail", function () {
        var access_token;
        let products;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });

        group("Step 2: Load ProductList", function () {
            let resProductList = getProductList("","","","",access_token);
            products = resProductList.result.data;
            for(var i=0; i<products.length; i++){
                var productId = products[i].id;
                console.log(productId);
            }
        });

        group("Step 3: Load Product Detail", function () {
            let resProductDetail = getProductDetail(products[0].id,access_token);
        });

        sleep(1);
    });
}