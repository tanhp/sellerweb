import { check, fail, sleep, group } from "k6";
import { getToken } from "../../Action/Seller/getToken.js";
import { getProductList, getProductDetail, putProduct } from "../../Action/Seller/product.js";
import { getRandomNumberInRange, getTokenFromArray } from "../../Action/general.js";

const users = JSON.parse(open("../../Data/sellerUsers.json"));

export let options = {
    vus: 1,
    duration: "15s"
};

export function setup(){
    var a = [];
    for(var i=0; i<users.length; i++){
        a[i] = getToken(`${users[i].username}`, `${users[i].password}`);
    }
    __ENV.TOKEN = a;
}

export default function() {
    group("Edit product", function () {
        var access_token;
        var productId;

        group("Step 1: Login", function () {
            let randomUser = getRandomNumberInRange(0, users.length - 1);
            access_token = getTokenFromArray(randomUser, __ENV.TOKEN);
        });

        group("Step 2: Load Product List", function () {
            let resProductList = getProductList("","","","",access_token);
            productId = resProductList.result.data[0].id;
        });

        group("Step 3: Load Product Detail", function () {
            let resProductDetail = getProductDetail(productId, access_token);
        });

        group("Step 4: Edit Product", function () {
            let resProduct = putProduct(productId,"thuc-an-va-dinh-duong-cho-ca",access_token);
        });

        group("Step 5: Load Product Detail", function () {
            let resProductDetail = getProductDetail(productId, access_token);
        });

        sleep(1);
    });
}