import { check, fail, sleep, group } from "./node_modules/k6";
import { getRandomNumberInRange, setProductAvailable, getMinValue, getMaxValue } from "../Action/general.js";
import login from "../../Action/Account/login.js";
import addToCart from "../../Action/Checkout/addToCart.js";
import checkoutInfo from "../../Action/Checkout/checkoutInfo.js"
import saveOrder from "../../Action/Checkout/saveOrder.js";

const products = JSON.parse(open("../../Data/products.json"));
const paymentMethods = JSON.parse(open("../../Data/paymentMethod.json"));
const shippingPackages = JSON.parse(open("../../Data/shippingPackage.json"));
const users = JSON.parse(open("../../Data/users.json"));

let hash;

export let options = {
    vus: 2,
    duration: "2s",
    thresholds: {
        http_req_duration: ["avg<100", "p(95)<200"]
    },
    noConnectionReuse: true
};

export function setup() {
    console.log("RUNNING ON " + __ENV.ENVIRONMENT + "!!!");
    let randomUser = getRandomNumberInRange(0, users.length - 1);
    let user = users[randomUser];
    __ENV.TOKEN = login(`${user.username}`, `${user.password}`);
    __ENV.ADDRESS_ID = user.address_id;
    //setProductAvailable(products);
};

export default function () {
    let randomProduct = getRandomNumberInRange(0, products.length - 1);
    let randomPaymentMethod = getRandomNumberInRange(0, paymentMethods.length - 1);
    let randomShippingPackage = getRandomNumberInRange(0, shippingPackages.length - 1);
    let product = products[randomProduct];
    let paymentMethod = paymentMethods[randomPaymentMethod];
    let shippingPackage = shippingPackages[randomShippingPackage];

    group("Checkout using logged in account", function () {
        group("Add to cart", function () {
            hash = addToCart(parseInt(`${product.shop_id}`), parseInt(`${product.product_id}`), __ENV.TOKEN);
        });

        group("Checkout info", function () {
            checkoutInfo(parseInt(`${product.shop_id}`), __ENV.TOKEN, hash);
        });

        group("Save order", function () {
            saveOrder(parseInt(`${__ENV.ADDRESS_ID}`), parseInt(`${product.shop_id}`), parseInt(`${product.product_id}`), `${shippingPackage.cptc}`, `${paymentMethod.cod}`, __ENV.TOKEN, hash);
        });
    });
};

export function teardown() {
    console.log("END!!!");
};