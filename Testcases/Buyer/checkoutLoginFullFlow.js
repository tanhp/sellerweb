import { check, fail, sleep, group } from "k6";
import { getRandomNumberInRange, setProductAvailable, getMinValue, getMaxValue } from "../../Action/general.js";
import login from "../../Action/Account/login.js";
import addToCart from "../../Action/Checkout/addToCart.js";
import loadCartInfo from "../../Action/ProductDiscovery/loadCartInfo.js";
import checkoutInfo from "../../Action/Checkout/checkoutInfo.js"
import saveOrder from "../../Action/Checkout/saveOrder.js";
import loadProductDetail from "../../Action/ProductDiscovery/loadProductDetail.js";
import search from "../../Action/ProductDiscovery/search.js";

const products = JSON.parse(open("../../Data/products.json"));
const paymentMethods = JSON.parse(open("../../Data/paymentMethod.json"));
const shippingPackages = JSON.parse(open("../../Data/shippingPackage.json"));
const users = JSON.parse(open("../../Data/users.json"));
const searchOptions = JSON.parse(open("../../Data/searchOptions.json"));

let accessToken;
let hash;

export let options = {
    vus: 2,
    duration: "1s"
};

export function setup() {
    console.log("RUNNING ON " + __ENV.ENVIRONMENT + "!!!");
    __ENV.LOGIN_DURATION = 0;
    //setProductAvailable(products);
};

export default function () {
    let randomUser = getRandomNumberInRange(0, users.length - 1);
    let randomProduct = getRandomNumberInRange(0, products.length - 1);
    let randomPaymentMethod = getRandomNumberInRange(0, paymentMethods.length - 1);
    let randomShippingPackage = getRandomNumberInRange(0, shippingPackages.length - 1);
    let randomSearchOption = getRandomNumberInRange(0, searchOptions.length - 1);
    let user = users[randomUser];
    let product = products[randomProduct];
    let paymentMethod = paymentMethods[randomPaymentMethod];
    let shippingPackage = shippingPackages[randomShippingPackage];
    let searchOption = searchOptions[randomSearchOption];

    group("Checkout login", function () {
        // group("Login", function () {
        //     accessToken = login(`${user.username}`, `${user.password}`);
        // });

        group("Search", function () {
            search(`${searchOption.keyword}`, `${searchOption.sort_type}`, `${searchOption.page}`);
        });

        // group("Load product detail", function () {
        //    loadProductDetail(parseInt(`${product.product_id}`), `${product.product_path}`, parseInt(`${product.shop_id}`));
        // });

        // group("Add to cart", function () {
        //     hash = addToCart(parseInt(`${product.shop_id}`), parseInt(`${product.product_id}`), accessToken);
        // });

        // group("Load cart info", function () {
        //     loadCartInfo(accessToken);
        // });

        // group("Checkout info", function () {
        //     checkoutInfo(parseInt(`${product.shop_id}`), accessToken, hash);
        // });

        // group("Save order", function () {
        //     saveOrder(parseInt(`${user.address_id}`), parseInt(`${product.shop_id}`), parseInt(`${product.product_id}`), `${shippingPackage.cptc}`, `${paymentMethod.cod}`, accessToken, hash);
        // });
        sleep(1);
    });
};

export function teardown() {
    console.log("END!!!");
};