package api;

import org.json.JSONObject;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class APIController {

    public static String getProductDetail(String url, int productId, String token){
        return "";
    }

    public static String sendApiRequest(String method, String source, String payload, String authorizationCode) {
            System.out.println("************************************************");
            System.out.println("Start to request with authorization ...");
            System.out.println("Method: " + method);
            System.out.println("Source: " + source);
            System.out.println("Payload: " + payload);
            System.out.println("Token: " + authorizationCode);
            assert (source != "" && source != null) : "API link is empty";

            String response = "";
            URL url = null;
            BufferedReader bufReader = null;
            HttpURLConnection conn = null;

            try {
                url = new URL(source);
                conn = (HttpURLConnection) url.openConnection();

                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setRequestProperty("ReCaptchaResponse", "smc@123456");
                //conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
                if(authorizationCode != null) {
                    conn.setRequestProperty("Authorization", authorizationCode);
                }
                conn.setDoOutput(true);
                if(!payload.equals("")){
                    DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                    outStream.write(payload.getBytes());
                    outStream.flush();
                    outStream.close();
                }

                System.out.println("ResponseCode: " + conn.getResponseCode());
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else {
                    bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
            } finally {
                try {
                    response = bufReader.readLine();
                    if (bufReader != null) {
                        bufReader.close();
                    }
                    conn.disconnect();
                }catch (Exception e){
                    System.out.println(123454);
                }
            }
            if(!response.contains("\"")) {
                //response = StringEscapeUtils.unescapeJava(response);
            }
            System.out.println("Get the response after run: " + response);
            System.out.println("************************************************");
        return response;
    }


    public static String getAuthorizationCode(String response){
        try {
            JSONObject fullResponse = new JSONObject(response);
            JSONObject result = (JSONObject) fullResponse.get("result");
            if(result.has("token"))
                return (String) result.get("token");
            else
                return (String) result.get("access_token");
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static Object getKeyFromUserContextDto(String key, String response){
        try {
            JSONObject fullResponse = new JSONObject(response);
            JSONObject result = (JSONObject) fullResponse.get("result");
            JSONObject userContextDto = (JSONObject) result.get("userContextDto");
            if(userContextDto.has(key) && !userContextDto.get(key).equals(null)){
                System.out.println(userContextDto.get(key));
                return userContextDto.get(key);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //@Test
    public void testApi(){
        String source = "http://apipwa.seller.test.sendo.vn/api/token/web";
        String payload = "{\"UserName\":\"0910000032\",\"reCaptchaResponse\":\"ABC\",\"Password\":123456}";
        String response = sendApiRequest("POST",source, payload, "");
        JSONObject fullResponse = new JSONObject(response);
        JSONObject result = (JSONObject) fullResponse.get("result");
        JSONObject userContextDto = (JSONObject) result.get("userContextDto");
        Object walletId = userContextDto.get("walletId");
        System.out.println(walletId);

        if(!walletId.equals(null)){
            System.out.println("ABCD: " + walletId);
            //return (String) userContextDto.get("walletId");
        }

        System.out.println("ABCDE");
    }

}