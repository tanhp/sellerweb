package api;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class RestAPI {


    public String getResponseContent(String apiUrl){
        RestAssured.baseURI = apiUrl;
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get ("/cate");
        return response.asString();
    }

    public int getResponseStatus(String apiUrl){
        RestAssured.baseURI = "API url";
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get ("/cate");
        return response.getStatusCode();
    }


}
