package database;

import constant.MyConstants;
import logger.MyLogger;

import java.sql.*;
import java.util.List;

public class Database {
    private Connection connect;
    private Statement statement;

    public Connection getConnection() {
        try {
            MyLogger.info("Start to get connection with DB server: " + MyConstants.DB_URL + ", " + "username: " + MyConstants.DB_USERNAME + ", " + "password: " + MyConstants.DB_PASSWORD);
            connect = DriverManager.getConnection(MyConstants.DB_URL, MyConstants.DB_USERNAME, MyConstants.DB_PASSWORD);
        } catch (Exception ex) {
            ex.printStackTrace();
            MyLogger.info("Failed to connect database: " + ex.getMessage());
        }
        return connect;
    }

    public Statement useDatabase(String databaseName) {
        try {
            MyLogger.info("Start to get use database: " + databaseName);
            statement = connect.createStatement();
            statement.execute("USE " + databaseName);
        } catch (Exception e) {
            MyLogger.info("Failed to use database: " + e.getMessage());
            e.printStackTrace();
        }
        return statement;
    }

    public void insertIntoDatabase(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ","
                + columns.get(12) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + values.get(5) + "','"
                + values.get(6) + "','"
                + values.get(7) + "','"
                + values.get(8) + "','"
                + values.get(9).replace("\\","\\\\") + "','"
                + values.get(10) + "','"
                + values.get(11) + "','"
                + values.get(12) + "')";
        try {
            MyLogger.info("Start to insert result to database with query: " + sql);
            statement.executeUpdate(sql);
        } catch (Exception e) {
            MyLogger.info("Failed to insert result to database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void insertIntoDatabase2(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "(";
        for (int i = 0; i < columns.size(); i++) {
            if(i == columns.size() - 1) {
                sql = sql + columns.get(i);
            } else {
                sql = sql + columns.get(i) + " , ";
            }
        }
        sql = sql + ") VALUES('";

        for (int i = 0; i < values.size(); i++) {
            if(i == values.size() - 1) {
                sql = sql + values.get(i);
            } else {
                sql = sql + values.get(i) + "' , '";
            }

        }
        sql = sql + "')";

        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void insertIntoTemporary(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + values.get(5) + "','"
                + values.get(6) + "','"
                + values.get(7) + "','"
                + values.get(8) + "','"
                + values.get(9).replace("\\","\\\\") + "','"
                + values.get(10) + "','"
                + values.get(11) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDatabase(String tableName, List<String> updateColumns, List<String> conditionColumns) {
        String sql = "UPDATE " + tableName + " SET ";
        for(int i = 0; i < updateColumns.size(); i++) {
            String[] splitUpdateColumn = updateColumns.get(i).split(" , ");
            if(updateColumns.size() == 1) {
                sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
            } else {
                if(i < updateColumns.size() - 1) {
                    sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'" + ", ";
                } else {
                    sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
                }
            }
        }

        if(conditionColumns != null) {
            sql = sql + " WHERE ";
            for(int i = 0; i < conditionColumns.size(); i++) {
                String[] splitConditionColumn = conditionColumns.get(i).split(" , ");
                if(conditionColumns.size() == 1) {
                    sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                } else {
                    if(i < conditionColumns.size() - 1) {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'" + " and ";
                    } else {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                    }
                }
            }
        }
        sql = sql + "AND environment='Seller - Production';";

        try {
            MyLogger.info("--Start to update database with query: " + sql);
            statement.executeUpdate(sql);
        } catch (Exception e) {
            MyLogger.info("--Failed to update database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void updateDatabase(String tableName, String column, String value, String id) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "='" + value + "'"
                + " WHERE Run_ID = '" + id + "';";
        System.out.println(sql);
        try {
            statement.executeUpdate(sql);
            System.out.println("Update database successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void updateDatabase2(String tableName, String column, String value, String suiteName) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "='" + value + "'"
                + " WHERE Test_suite = '" + suiteName + "';";
        try {
            statement.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void closeConnection(){
        try{
            MyLogger.info("Start to close connection");
            connect.close();
        } catch (Exception e) {
            MyLogger.info("Failed to close connection");
            e.printStackTrace();
        }
    }

    public String getValueFromSelectedColumn(String tableName, String columnName, String conditionColumn, String testSuite) {
        String sql = "SELECT " + columnName + " FROM " + tableName + " WHERE  " + conditionColumn + " = '" + testSuite + "';";
        String result = null;

        try {
            ResultSet rs = statement.executeQuery(sql);
            if(rs.next()){
                result = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
