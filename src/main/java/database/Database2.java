package database;

import logger.MyLogger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Database2 {

    public static String url = "jdbc:mysql://192.168.1.26";
    public static String username = "autoUser";
    public static String password = "abcd@1234";
    public static Connection connect;
    public static Statement statement;

    public static Connection getConnection() {
        try {
            MyLogger.info("Start to get connection to db with url: " + url + ", username: " + username + ", password: " + password);
            connect = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            MyLogger.error("Failed to connect database: " + ex.getMessage());
            ex.printStackTrace();
        }
        return connect;
    }

    public static Statement useDatabase(String databaseName) {
        try {
            MyLogger.info("Start to use database " + databaseName);
            statement = connect.createStatement();
            statement.execute("USE " + databaseName);
        } catch (SQLException e) {
            MyLogger.error("Failed to use database: " + e.getMessage());
            e.printStackTrace();
        }
        return statement;
    }

    public static void insertIntoDatabase(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "(";
        for (int i = 0; i < columns.size(); i++) {
            if(i == columns.size() - 1) {
                sql = sql + columns.get(i);
            } else {
                sql = sql + columns.get(i) + " , ";
            }
        }
        sql = sql + ") VALUES('";

        for (int i = 0; i < values.size(); i++) {
            if(i == values.size() - 1) {
                sql = sql + values.get(i);
            } else {
                sql = sql + values.get(i) + "' , '";
            }

        }
        sql = sql + "')";

        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDatabase(String tableName, List<String> updateColumns, List<String> conditionColumns) {
        String sql = "UPDATE " + tableName + " SET ";
        for(int i = 0; i < updateColumns.size(); i++) {
            String[] splitUpdateColumn = updateColumns.get(i).split(" , ");
            if(updateColumns.size() == 1) {
                sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
            } else {
                if(i < updateColumns.size() - 1) {
                    sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'" + ", ";
                } else {
                    sql = sql + splitUpdateColumn[0] + " = '" + splitUpdateColumn[1] + "'";
                }
            }
        }

        if(conditionColumns != null) {
            sql = sql + " WHERE ";
            for(int i = 0; i < conditionColumns.size(); i++) {
                String[] splitConditionColumn = conditionColumns.get(i).split(" , ");
                if(conditionColumns.size() == 1) {
                    sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                } else {
                    if(i < conditionColumns.size() - 1) {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'" + " and ";
                    } else {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                    }
                }
            }
        }
        sql = sql + ";";

        try {
            MyLogger.info("Start to update database with table " + tableName + " with sql: " + sql);
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            MyLogger.error("Failed to update database: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void insertIntoDatabase_(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + values.get(5) + "','"
                + values.get(6) + "','"
                + values.get(7) + "','"
                + values.get(8) + "','"
                + values.get(9).replace("\\","\\\\") + "','"
                + values.get(10) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertIntoDatabasePWA(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ","
                + columns.get(12) + ","
                + columns.get(13) + ") "
                + "VALUES('"
                + Integer.parseInt(values.get(0)) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + Double.parseDouble(values.get(5)) + "','"
                + Double.parseDouble(values.get(6)) + "','"
                + Double.parseDouble(values.get(7)) + "','"
                + Double.parseDouble(values.get(8)) + "','"
                + Double.parseDouble(values.get(9)) + "','"
                + values.get(10) + "','"
                + values.get(11) + "','"
                + values.get(12) + "','"
                + values.get(13) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertIntoDatabasePWAOverall(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ","
                + columns.get(9) + ","
                + columns.get(10) + ","
                + columns.get(11) + ","
                + columns.get(12) + ") "
                + "VALUES('"
                + values.get(0) + "','"
                + Integer.parseInt(values.get(1)) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + Double.parseDouble(values.get(5)) + "','"
                + Double.parseDouble(values.get(6)) + "','"
                + Double.parseDouble(values.get(7)) + "','"
                + Double.parseDouble(values.get(8)) + "','"
                + Double.parseDouble(values.get(9)) + "','"
                + values.get(10) + "','"
                + values.get(11) + "','"
                + values.get(12) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDatabase(String tableName, String column, String value, String testMethod, String id) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "='" + value
                + "' WHERE Test_method ='" + testMethod
                + "' AND Run_ID = '" + id + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDatabase(String tableName, String column, String value, String id) {
        String sql = "UPDATE " + tableName
                + " SET " + column + "= CONCAT(COALESCE(`" + column + "`,''),'" + value.replace("\\","\\\\") + "\n')"
                + " WHERE Run_ID = '" + id + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDatabasePWA(String tableName, String column, String score, String environment) {
        String sql = "UPDATE " + tableName
                + " SET " + column + " = " + column + " +1"
                + " WHERE score_type = '" + score + "' AND environment = '" + environment + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

/*    public static void updateDatabasePWA(String tableName, String column, String value, String id) {
    	String sql = "UPDATE " + tableName
    			+ " SET " + column + " = " + value
    			+ " WHERE url_id = " + Integer.parseInt(id) + ";";
    	try {
    		statement.executeUpdate(sql);
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    }*/

    public static void setColumnValuePWA(String tableName, String column, String value) {
        String sql = "UPDATE " + tableName
                + " SET " + column + " = " + value + ";";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet select() {
        ResultSet rs = null;
        String sql = "SELECT Test_suite, Execution_time, SUM(CASE WHEN Final_result = 'PASSED' THEN 1 ELSE 0 END) AS Passed," +
                "SUM(CASE WHEN Final_result = 'FAILED' THEN 1 ELSE 0 END) AS Failed " +
                "FROM `shoppingflow` " +
                "GROUP BY Test_suite;";
        try {
            rs = statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rs;
    }

    public static void insertIntoDatabasePerformance(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ") "
                + "VALUES('"
                + Integer.parseInt(values.get(0)) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + Double.parseDouble(values.get(4)) + "','"
                + Double.parseDouble(values.get(5)) + "','"
                + values.get(6) + "','"
                + values.get(7) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertIntoDatabasePerformanceComparision(String tableName, List<String> columns, List<String> values) {
        String sql = "INSERT INTO " + tableName + "("
                + columns.get(0) + ","
                + columns.get(1) + ","
                + columns.get(2) + ","
                + columns.get(3) + ","
                + columns.get(4) + ","
                + columns.get(5) + ","
                + columns.get(6) + ","
                + columns.get(7) + ","
                + columns.get(8) + ") "
                + "VALUES('"
                + Integer.parseInt(values.get(0)) + "','"
                + values.get(1) + "','"
                + values.get(2) + "','"
                + values.get(3) + "','"
                + values.get(4) + "','"
                + Double.parseDouble(values.get(5)) + "','"
                + Double.parseDouble(values.get(6)) + "','"
                + values.get(7) + "','"
                + values.get(8) + "')";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateDatabaseLogControl(String tableName, List<String> columns, List<String> values, String id) {
        String sql = "UPDATE " + tableName
                + " SET " + columns.get(0) + " = '" + values.get(0)
                + "' , " + columns.get(1) + " = '" + values.get(1)
                + "' , " + columns.get(2) + " = '" + values.get(2)
                + "' , " + columns.get(3) + " = '" + values.get(3)
                + "' , " + columns.get(4) + " = '" + values.get(4)
                + "' WHERE Run_ID = '" + id + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void clearTable(String table) {
        String sql = "DELETE FROM " + table;
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void synchLatestJSTable() {
        String sql = "call copyData();";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void synchJSSummaryTable() {
        String sql = "call getJSExecutionSummary();";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateJSExecutionId() {
        String sql = "call updateJSExecutionId();";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateTableFailureTracking(String tableName, String column, String value, String testSuite) {
        String sql = "UPDATE " + tableName + " SET " + column + " = '" + value + "' WHERE test_suite = '" + testSuite + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getValueFromSelectedColumn(String tableName, String columnName, String conditionColumn, String testSuite) {
        String sql = "SELECT " + columnName + " FROM " + tableName + " WHERE  " + conditionColumn + " = '" + testSuite + "';";
        String result = null;

        try {
            ResultSet rs = statement.executeQuery(sql);
            if(rs.next()){
                result = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<List<String>> getValuesListFromSelectedColumn(String tableName, String testSuite) {
        String sql = "SELECT Test_suite, Test_method, Execution_time_long, Final_result, Report, SUBSTRING_INDEX(Error_screenshot, \"\n\",1) as Error_screenshot FROM " + tableName + " WHERE Test_suite = '" + testSuite + "' And Final_result = 'FAILED' ORDER BY Execution_time_long DESC LIMIT 3;";
        List<List<String>> result = new ArrayList<List<String>>();

        try {
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                List<String> currentRowValues = new ArrayList<String>();
                currentRowValues.add(rs.getString("Test_suite"));
                currentRowValues.add(rs.getString("Test_method"));
                currentRowValues.add(rs.getString("Execution_time_long"));
                currentRowValues.add(rs.getString("Final_result"));
                currentRowValues.add(rs.getString("Report"));
                currentRowValues.add(rs.getString("Error_screenshot"));
                result.add(currentRowValues);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void setFailedCount(String tableName, String columnName, String testSuite) {
        String sql = "UPDATE " + tableName
                + " SET " + columnName + " = " + columnName + " +1 "
                + "WHERE test_suite = '" + testSuite + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void resetFailedCount(String tableName, String columnName, String testSuite) {
        String sql = "UPDATE " + tableName
                + " SET " + columnName + " = 0 "
                + "WHERE test_suite = '" + testSuite + "';";
        try {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<List<String>> getManualTestCaseList(String tableName, String testSuite, String method) {
        String sql = "SELECT manualTestCaseId, comment FROM " + tableName + " WHERE testClass = '" + testSuite+"' and testMethod = '" + method + "'";
        List<List<String>> result = new ArrayList<List<String>>();
        try {
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                List<String> currentRowValues = new ArrayList<String>();
                currentRowValues.add(rs.getString("manualTestCaseId"));
                currentRowValues.add(rs.getString("comment"));
                result.add(currentRowValues);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getValueFromSelectedColumnMultiCondition(String tableName, String columnName, List<String> conditionColumns) {
        String sql = "SELECT " + columnName + " FROM " + tableName;
        if(conditionColumns != null) {
            sql = sql + " WHERE ";
            for(int i = 0; i < conditionColumns.size(); i++) {
                String[] splitConditionColumn = conditionColumns.get(i).split(" , ");
                if(conditionColumns.size() == 1) {
                    sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                } else {
                    if(i < conditionColumns.size() - 1) {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'" + " and ";
                    } else {
                        sql = sql + splitConditionColumn[0] + " = '" + splitConditionColumn[1] + "'";
                    }
                }
            }
        }
        String result = null;

        try {
            ResultSet rs = statement.executeQuery(sql);
            if(rs.next()){
                result = rs.getString(1);
            }
        } catch (SQLException e) {
            MyLogger.error("Failed to get value from selected column multi condition: " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
}
