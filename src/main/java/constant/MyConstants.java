package constant;

public class MyConstants {



    /* PATH_CONSTANTS */

    public static String SYSTEM_PATH = System.getProperty("user.dir");

    public static String DRIVER_PATH = SYSTEM_PATH + "/drivers/";
    public static String getChromeDriverPath(){
        if(System.getProperty("os.name").startsWith("Windows"))
            return DRIVER_PATH + "chromedriver.exe";
        else if(System.getProperty("os.name").startsWith("Mac"))
            return DRIVER_PATH + "os_chromedriver";
        else
            return DRIVER_PATH + "linux_chromedriver";
    }
    public static String FIREFOX_DRIVER_PATH = DRIVER_PATH + "geckodriver.exe";
    public static String IE_DRIVER_PATH = DRIVER_PATH + "IEDriverServer.exe";


    public static String environment = "pro";
    public static String getEnvironmentProperty = System.getProperty("environment");

    /* HOST */
    public static String SELLER_TEST = "http://banpwa.seller.test.sendo.vn";
    public static String SELLER_PRO = "https://ban.sendo.vn";
    public static String SELLER_PILOT = "http://ban2.pilot.sendo.vn";
    public static String SELLER_API_STAGING = "https://seller-api-stg.sendo.vn";
    public static String SELLER_API_PRO = "https://seller-api.sendo.vn";
    public static String SELLER_API_PILOT = "http://seller-api.pilot.sendo.vn";
    public static String SELLER_STAGING = "https://ban-stg.sendo.vn";
    public static String SELLER_API_PWA = "http://apipwa.seller.test.sendo.vn";
    public static String BUYER_TEST = "http://test.sendo.vn";
    public static String BUYER_PRO = "https://sendo.vn";
    public static String BUYER_PILOT = "https://pilot.sendo.vn";
    public static String BUYER_STAGING = "https://stg.sendo.vn";


    /* Folders */
    public static String LINUX_PATH = "/opt/lampp/htdocs/RQM/seller";
    public static String CONFIG_PATH = SYSTEM_PATH + "/configs/";
    public static String INPUT_PATH = SYSTEM_PATH + "/input/";
    public static String OUTPUT_PATH = SYSTEM_PATH + "/output/";
    public static String EXCEL_OUTPUT_PATH = OUTPUT_PATH + "excelOutput/";
    public static String IMAGES_PATH = SYSTEM_PATH + "/images/";
    public static String OBJECT_PATH = SYSTEM_PATH + "/properties/";
    public static String SCREENSHOT_PATH = getScreenShotPath();

    public static String getScreenShotPath(){
        if(System.getProperty("os.name").startsWith("Linux")){
            if(getEnvironmentProperty != null)
                return SYSTEM_PATH + "/screenshots/";
            else
                return LINUX_PATH + "/screenshots/";
        }
        else
            return SYSTEM_PATH + "/screenshots/";
    }

    public static String HTMLREPORT_PATH = getReportPath();

    public static String getReportPath(){
        if(System.getProperty("os.name").startsWith("Linux")){
            if(getEnvironmentProperty != null)
                return SYSTEM_PATH + "/htmlreports/";
            else
                return LINUX_PATH + "/htmlreports/";
        }
        else
            return SYSTEM_PATH + "/htmlreports/";
    }

    public static String LOG_PATH = SYSTEM_PATH + "/logs/log4j/";

    // Files
    public static String HEADER_FILE = INPUT_PATH + "0Header.xlsx";
    public static String SMOKETEST_FILE = INPUT_PATH + "SmokeTest.xlsx";
    public static String getFileValidation(){
        if(getEnvironmentProperty!= null){
            switch (getEnvironmentProperty){
                case "pro":
                    return "TestValidation.xlsx";
                case "pilot":
                case "test":
                    return "TestValidation-Test.xlsx";
                case "staging":
                    return "TestValidation-Stg.xlsx";
            }
        }else{
            switch (environment){
                case "pro":
                    return "TestValidation.xlsx";
                case "test":
                    return "TestValidation-Test.xlsx";
                case "pilot":
                case "staging":
                    return "TestValidation-Stg.xlsx";
            }
        }
        return "";
    }

    /* URL_CONSTANTS */
    public static String getServerUrl(){
        if(System.getProperty("os.name").startsWith("Linux")){
            if(getEnvironmentProperty != null){
                return "http://192.168.1.49/RQM/seller";
            }else{
                return "http://192.168.1.47/RQM/seller";
            }
        }else{
            return "http://192.168.1.47/RQM/seller";
        }
    }
    public static String SCREENSHOT_URL = getServerUrl() + "/screenshots/";
    public static String HTMLREPORT_URL = getServerUrl() + "/htmlreports/";

    /* DATABASE */
    public static String DB_URL = "jdbc:mysql://192.168.1.47";
    public static String DB_USERNAME = "autoUser";
    public static String DB_PASSWORD = "abcd@1234";
    public static String DB_DATABASENAME = "test";
    public static String DB_TABLENAME = "seller";

    /* ENVIRONMENT */
    public static String ENVIRONMENT = "Seller - Production";



    /* DOMAIN */
    public static String getBaseSellerUrl(){
        if(getEnvironmentProperty!= null){
            switch (getEnvironmentProperty){
                case "test":
                    return SELLER_TEST;
                case "pilot":
                    return SELLER_PILOT;
                case "staging":
                    return SELLER_STAGING;
                case "pro":
                    return SELLER_PRO;
            }
        }else{
            switch (environment){
                case "pro":
                    return SELLER_PRO;
                case "pilot":
                    return SELLER_PILOT;
                case "staging":
                    return SELLER_STAGING;
                case "test":
                    return SELLER_TEST;
            }
        }
        return "";
    }

    public static String getBaseBuyerUrl(){
        if(getEnvironmentProperty != null){
            switch (getEnvironmentProperty){
                case "test":
                    return BUYER_TEST;
                case "pilot":
                    return BUYER_PILOT;
                case "staging":
                    return BUYER_STAGING;
                case "pro":
                    return BUYER_PRO;
            }
        }else{
            switch (environment){
                case "pro":
                    return BUYER_PRO;
                case "pilot":
                    return BUYER_PILOT;
                case "staging":
                    return BUYER_STAGING;
            }
        }
        return "";
    }

    public static String getSellerApi(){
        if(getEnvironmentProperty != null){
            switch (getEnvironmentProperty){
                case "pro":
                    return SELLER_API_PRO;
                case "pilot":
                    return SELLER_API_PILOT;
                case "staging":
                    return SELLER_API_STAGING;
                case "test":
                    return SELLER_API_PWA;
            }
        }else{
            switch (environment){
                case "pro":
                    return SELLER_API_PRO;
                case "pilot":
                    return SELLER_API_PILOT;
                case "staging":
                    return SELLER_API_STAGING;
                case "test":
                    return SELLER_API_PWA;
            }
        }
        return "";
    }

    public static int getRunIndex(){
        if(getEnvironmentProperty != null){
            switch (getEnvironmentProperty){
                case "pro":
                case "pilot":
                    return 6;
                case "staging":
                    return 7;
            }
        }else{
            switch (environment){
                case "pro":
                case "pilot":
                    return 6;
                case "staging":
                    return 7;
            }
        }
        return -1;
    }

    public static String getMainAccount(){
        if(getEnvironmentProperty != null){
            switch (getEnvironmentProperty){
                case "test":
                case "pro":
                case "pilot":
                    return "TC_006";
                case "staging":
                    return "TC_005";//5
            }
        }else{
            switch (environment){
                case "test":
                case "pro":
                case "pilot":
                    return "TC_006";
                case "staging":
                    return "TC_005";//5
            }
        }
        return "";
    }

    public static final String BROWSER = "chrome";

    // CONFIG
    public static int timezoneOffset = 6;

    // Buyer Account
    public static String BUYER_USERNAME="tester01gm@gmail.com";
    public static String BUYER_PASSWORD="123456";

    // Skype account
    public static String SKYPE_USERNAME="mon.mon236";
    public static String SKYPE_PASSWORD = "mon8245679311";
    public static String SKYPE_IDENTITY_TAN = "8:tanhopham1990";
    public static String SKYPE_IDENTITY_BICH = "8:bichbaby47";
    public static String SKYPE_IDENTITY_GROUP = "19:4899829c0e434b2e888c3b6eaae77797@thread.skype";

    // Configs
    //public static Boolean isWriteToDB = true;
    public static Boolean isWriteToDB() {
        if (System.getProperty("os.name").startsWith("Linux")) {
            if (getEnvironmentProperty.equals("pro")) {
                return true;
            }
        }
        return false;
    }
}
