package constant;

public enum SendoAccount {
    BUYER_USERNAME,
    BUYER_PASSWORD;

    @Override
    public String toString() {
        if(this == BUYER_USERNAME)
            return "tester01gm@gmail.com";
        if (this == BUYER_PASSWORD)
            return "123456";
        return "";
    }
}
