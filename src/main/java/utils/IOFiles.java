package utils;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOFiles {

    public static String readFile(String filePath) {
        try {
            String s = "";
            FileReader reader = new FileReader(filePath);
            int character;

            while ((character = reader.read()) != -1) {
                s = s + ((char) character);
            }
            reader.close();
            return s;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeFile(String s, String filePath){
        try {
            FileWriter writer = new FileWriter(filePath, false);
            writer.write(s);
            System.out.println("You have written " + s);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
