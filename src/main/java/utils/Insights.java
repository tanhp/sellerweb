package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Insights {

    public static String getPerformanceInfo(String link,String strategy) {
        String result = "";
        try {
            String keyAPI = "AIzaSyDVMQ7kKFE7UKw-n_CP76XKkAdM9dgwEaA";
            //Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.sendo.vn", 80));
            String url = "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=" + link + "&strategy=" + strategy + "&key=" + keyAPI;
            URL object = new URL(url);
            StringBuilder sb = new StringBuilder();
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            //Get performance score
            String[] split = sb.toString().split(",");
            for (int i = 0; i < split.length; i++) {
                if (split[i].contains("SPEED")) {
                    String[] detail = split[i].split(":");
                    String score = detail[3].replaceAll("}", "").trim();
                    return score;
                }
            }
            con.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
