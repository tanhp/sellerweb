package utils;

import constant.MyConstants;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;

public class HyperlinkWorker {

    static XSSFWorkbook workbook;
    static XSSFSheet spreadsheet;
    static XSSFCell cell;
    static CreationHelper createHelper;
    static XSSFCellStyle hlinkstyle;
    static XSSFFont hlinkfont;


    public static void setUp(String filePath, String sheetName){
        FileInputStream fis;
        try {
            fis = new FileInputStream(filePath);
            workbook = new XSSFWorkbook(fis);
            fis.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
            return;
        }
        spreadsheet = workbook.getSheet(sheetName);
        createHelper = workbook.getCreationHelper();
        hlinkstyle = workbook.createCellStyle();
        hlinkfont = workbook.createFont();
    }
    
    public static void setUp(XSSFWorkbook workbook, XSSFSheet sheet) {
    	spreadsheet = sheet;
        createHelper = workbook.getCreationHelper();
        hlinkstyle = workbook.createCellStyle();
        hlinkfont = workbook.createFont();
    }

    public static void cellStyleForHyperlink(){
        hlinkfont.setUnderline(XSSFFont.U_SINGLE);
        hlinkfont.setColor(HSSFColor.BLUE.index);
        hlinkstyle.setFont(hlinkfont);
    }

    public static void setUrlLink(int rowNum, int colNum, String urlLink){
        XSSFRow row = spreadsheet.getRow(rowNum);
        cell = row.getCell(colNum);
        XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(Hyperlink.LINK_URL);
        link.setAddress(urlLink);
        cell.setHyperlink(link);
        cell.setCellStyle(hlinkstyle);
    }

    public static void setHyperlinkToAFile(int rowNum, int colNum, String fileName){
        XSSFRow row = spreadsheet.getRow(rowNum);
        cell = row.getCell(colNum);
        XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(Hyperlink.LINK_FILE);
        link.setAddress(fileName);
        cell.setHyperlink(link);
        cell.setCellStyle(hlinkstyle);
    }

    public static void setEmailLink(int rowNum, int colNum, String emailLink){
        XSSFRow row = spreadsheet.getRow(rowNum);
        cell = row.getCell(colNum);
        XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(Hyperlink.LINK_EMAIL);
        link.setAddress("mailto:" + emailLink);
        cell.setHyperlink(link);
        cell.setCellStyle(hlinkstyle);
    }

    public static void setLinkToSheet(int rowNum, int colNum, String linkToSheet){
        XSSFRow row = spreadsheet.getRow(rowNum);
        cell = row.getCell(colNum);
        XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(Hyperlink.LINK_DOCUMENT);
        link.setAddress(linkToSheet);
        cell.setHyperlink(link);
        cell.setCellStyle(hlinkstyle);
    }

    public static void tearDown(String filePath){
        try {
            FileOutputStream out = new FileOutputStream(new File(filePath));
            workbook.write(out);
            out.close();
            System.out.println("Excel written successfully");
        }catch (Exception e){

        }
    }
    

    public static void main(String[]args) throws IOException {
    	String filePath = MyConstants.OUTPUT_PATH + "Test.xlsx";
    	setUp(filePath,"Sheet1");
    	//cellStyleForHyperlink();
    	setUrlLink(0,0,"https://google.com");
    	tearDown(filePath);
    }
}
