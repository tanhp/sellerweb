package operation;

import classes.MyClasses;
import constant.MyConstants;

import logger.MyLogger;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;

public class BaseTest extends ParallelTest{
    @BeforeSuite
    public void suiteSetUp(ITestContext iSuite){
    	String testSuiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        MyLogger.info("Start test suite: " + testSuiteName);
        setUpDatabase();
    }


    @BeforeClass
    public void classSetUp(ITestContext iSuite){
        String className = this.getClass().getName();
        MyLogger.info("Start test class: " + className);
        setUpReport(iSuite);
        setUpTestClass(className);
    }

    @Parameters("browser")
    @BeforeMethod(alwaysRun = true)
    public void testCaseSetUp(Method methodName, @Optional(MyConstants.BROWSER) String browser){
    	MyLogger.info("Start test case: " + methodName.getName());
    	if(environment!=null && environment.equals("pilot")){
    	    if(System.getProperty("manualExecution")!=null)
                updateManualTestExecutionStatus(System.getProperty("manualExecution"));
        }
        setDriver(initDriver(browser));
        new MyClasses();
        new ActionGen(getDriver());
        new ActionWeb();
        //setDriver(initDriverEmulation(browser,"iPhone 6"));
        setUpTestCase(methodName);
    }

    @Parameters("browser")
    @AfterMethod
    public void testCaseTearDown(ITestResult iResult, ITestContext iContext, @Optional(MyConstants.BROWSER) String browser){
        updateTestResult(iResult);
        insertIntoReport();
        insertIntoDatabase(iResult,iContext);
        closeDriver(browser);
        MyLogger.info("/***** FINISHED TESTCASE *****/");
    }

    @AfterClass
    public void classTearDown(){
        closeReport();
    }

    @Parameters("browser")
    @AfterSuite
    public void suiteTearDown(ITestContext testSuite, @Optional(MyConstants.BROWSER) String browser){
        if(flag)
            //closeExcel();
        insertResultToDB(testSuite);
        insertIntoHeaderFile(testSuite);
        //insertIntoJSONFile(testSuite);
        if(!System.getProperty("os.name").startsWith("Windows 10"))
            killProcess(browser);
        closeDatabase();
        MyLogger.info("/***** FINISHED TESTSUITE *****/");
    }
}
