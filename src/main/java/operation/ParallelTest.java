package operation;


import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;

//import autotestng.AutoTestNG;
import constant.MyConstants;
import database.Database;
import database.Database2;
import logger.MyLogger;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import report.ExtentReport;
import utils.*;

import java.awt.*;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class ParallelTest {

    private String oldFile = null;
    private WebDriver driver;

    XSSFWorkbook workbook = null;
    public static XSSFWorkbook wbHeader = null;
    private String fileValidation = null;
    String sheetName = "";
    String tempSheetName = "";
    int rowNum = -1;

    UIOperation action;
    ExtentReport report;
    public static Method method;
    public static String className;
    String reportPath = null;
    String reportUrl = null;
    private String id = null;
    private String executionDate = null;
    private String executionTime = null;
    private String executionTimeSuite = null;

    private String testResult = null;
    Database database;
    private List<String> columns = Arrays.asList("Run_ID", "Test_suite", "Test_class", "Test_method", "Execution_time",
            "Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot", "Environment");
    private List<String> columns2 = Arrays.asList("Environment","Test_suite",	"Test_method", "Running_status", "Execution_time", 
    		"Execution_time_long", "Duration_time", "First_run", "Second_run", "Final_result", "Report", "Error_screenshot");
    private List<String> values = new ArrayList<String>();
    private List<String> values2 = new ArrayList<String>();

    private String firstRun = "";
    private String secondRun = "";
    private String finalRun = "";

    private String screenshotPath = "";
    private String screenshotURL1 = "";
    private String screenshotURL2 = "";
    public static String exception = "";

    private String pSuiteName = null;
    private int count = 0;

    public Boolean flag = true;
    public Boolean flagForHeaderFile = false;
    public Boolean security = false;

    public String environment;

    public ParallelTest(){
        MyLogger.info("Automation tests are running on os: " + System.getProperty("os.name"));
        environment = MyConstants.getEnvironmentProperty;
        MyLogger.info("Environment: " + environment);
        report = new ExtentReport();
        if(MyConstants.isWriteToDB() && environment.equals("pro"));
            database = new Database();
    }

    public void setDriver(WebDriver driver){
        this.driver = driver;
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    public ExtentReport getExtentReport() {
    	return report;
    }

    public FirefoxOptions getFireFoxOptions(){
        FirefoxOptions ffOptions = new FirefoxOptions();
        ffOptions.addPreference("browser.download.manager.focusWhenStarting", true);
        ffOptions.addPreference("browser.download.useDownloadDir", false);
        ffOptions.addPreference("browser.helperApps.alwaysAsk.force", true);
        ffOptions.addPreference("browser.download.manager.alertOnEXEOpen", false);
        ffOptions.addPreference("browser.download.manager.closeWhenDone", true);
        ffOptions.addPreference("browser.download.manager.showAlertOnComplete", false);
        ffOptions.addPreference("browser.download.manager.useWindow", false);
        ffOptions.addPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
        return ffOptions;
    }

    public ChromeOptions getChromeOptions(){
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        if(System.getProperty("os.name").startsWith("Linux")){
            options.addArguments("--headless");
            options.addArguments("window-size=1400,900");
        }
        options.addArguments("--start-maximized");
        options.addArguments("disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("useAutomationExtension", false);
        return options;
    }

    public void setPermissionFileOnLinux(){
        try{
            String command = "chmod +x " + MyConstants.getChromeDriverPath();
            Runtime.getRuntime().exec(command);
        }catch (Exception e){

        }
    }

    public WebDriver initDriver(String browser){
        MyLogger.info("Start to launch browser: " + browser);
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
                driver = new FirefoxDriver(getFireFoxOptions());
                driver.manage().window().maximize();
                break;
            case "chrome":
                if(System.getProperty("os.name").startsWith("Linux"))
                    setPermissionFileOnLinux();
                System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
            	driver = new ChromeDriver(getChromeOptions());
                break;
            case "iexplorer":
                System.setProperty("webdriver.ie.driver",MyConstants.IE_DRIVER_PATH);
                driver = new InternetExplorerDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriver initDriverEmulation(String browser, String device){
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
                driver = new FirefoxDriver();
                break;
            case "chrome":
                System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
                Map<String, String> mobileEmulation = new HashMap<String, String>();
                mobileEmulation.put("deviceName", device);
                Map<String, Object> chromeOptions = new HashMap<String, Object>();
                chromeOptions.put("mobileEmulation", mobileEmulation);
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

                driver = new ChromeDriver(capabilities);
                break;
            case "iexplorer":
                System.setProperty("webdriver.ie.driver",MyConstants.IE_DRIVER_PATH);
                driver = new InternetExplorerDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    public static FirefoxProfile FirefoxDriverProfile() throws Exception {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.download.dir", "downloadPath"); // pass string downloadpath
        profile.setPreference("browser.helperApps.neverAsk.openFile",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
                "text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
        profile.setPreference("browser.download.manager.focusWhenStarting", false);
        profile.setPreference("browser.download.manager.useWindow", false);
        profile.setPreference("browser.download.manager.showAlertOnComplete", false);
        profile.setPreference("browser.download.manager.closeWhenDone", false);
        return profile;
    }

    public void closeDriver(String browser){
        Set<Cookie> cookiesForCurrentURL = driver.manage().getCookies();
        for (Cookie cookie : cookiesForCurrentURL) {
            System.out.println(" Cookie Name - " + cookie.getName()
                    + " Cookie Value - "  + cookie.getValue());
        }

        MyLogger.info("Start to close browser: " + browser);
        try{
            switch (browser){
                case "firefox":
                    driver.quit();
                    break;
                case "chrome":
                    driver.close();
                    break;
            }
        }catch (Exception e){
            MyLogger.error("Failed to close browser: " + e.getMessage());
        }

    }

    public void killProcess(String browser) {
        try {
            if(System.getProperty("os.name").startsWith("Windows")){
                if (browser.equals("chrome")) {
                    Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
                    Runtime.getRuntime().exec("TASKKILL /F /IM chrome.exe");
                } else if (browser.equals("firefox")) {
                    Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe");
                    Runtime.getRuntime().exec("TASKKILL /F /IM firefox.exe");
                } else if (browser.equals("iexplorer")) {

                }
            }else if(System.getProperty("os.name").startsWith("Linux")){
                if (browser.equals("chrome")) {
                    Runtime.getRuntime().exec("killall linux_chromedriver");
                }
            }


        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public void performTestCaseUsingExcel(String sheetName, String testId){
        MyLogger.info("Start to perform testcase with sheetName: " + sheetName + ", testId: " + testId);
        String fileAction = "TestAction.xlsx";
        String fileValidation = MyConstants.getFileValidation();
        XLSWorker.setSheetName(sheetName);
        if(this.workbook==null){
            this.workbook = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
        }

        if(!tempSheetName.equals(sheetName)){
            this.workbook = XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
            tempSheetName = sheetName;
        }

        int rowNum = XLSWorker.getRowNumByTestCaseID(XLSWorker.getSheet(workbook,sheetName),testId);

        this.fileValidation = fileValidation;
        this.sheetName = sheetName;
        this.rowNum = rowNum;

        Object[][] objects = XLSWorker.getDataForTestCase(fileAction, fileValidation, sheetName,rowNum);
        if(objects==null)
            return;
        for(int i=0; i< objects.length; i++){
            if(objects[i][1]==null || objects[i][2]==null || objects[i][3]==null)
                break;
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            UIOperation.perform(keyword,object,value);
        }
        //XLSWorker.updateExcel(XLSWorker.getSheet(workbook,sheetName),rowNum,"PASSED");
    }

    public void closeExcel(){
        XLSWorker.writeExcel(workbook);
    }

    public void setUpReport(ITestContext iSuite){
        Date date = new Date();
        String suiteName = iSuite.getCurrentXmlTest().getSuite().getName();
        pSuiteName = suiteName;
        if(suiteName.equals("Chrome") || suiteName.equals("Firefox") || suiteName.equals("UAT")) {
        	security = true;
        }
        String browser = StringUtils.capitalize(iSuite.getCurrentXmlTest().getParameter("browser"));
        reportPath = MyConstants.HTMLREPORT_PATH + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        reportUrl = MyConstants.HTMLREPORT_URL + suiteName + " - " + browser + " - "
                + FormatUtils.formatDate(date, "dd-MM-yyyy HH.mm.ss") + ".html";
        report.initExtent(reportPath);
        
    }

    public void setUpDatabase(){
        if(MyConstants.isWriteToDB()){
            if(System.getProperty("manualExecution")!=null){
                Database2.getConnection();
                Database2.useDatabase("testcases");
            }else{
                database.getConnection();
                database.useDatabase(MyConstants.DB_DATABASENAME);
            }
        }
    }
    

    public void setUpTestCase(Method methodName){
        report.startExtent(methodName.getName());
        method = methodName;
    }

    public void setUpTestClass(String className1) {
    	className = className1;
    }
    
    public void updateExcelFinalResult(){
    	MyLogger.info("Start to update final test results in excel for " + MyConstants.EXCEL_OUTPUT_PATH + "result_TestValidation_" + sheetName + ".xlsx");
        if(testResult.equals("FAILED")) {
            testResult = "FAILED: " + MyLogger.getFailedMessage();
        }
    	XSSFSheet sheet = XLSWorker.getSheet(workbook,sheetName);
        XLSWorker.updateExcel(sheet,rowNum,testResult,screenshotURL1);
        XLSWorker.writeExcel(workbook);
    }
    
    

    public void updateTestResult(ITestResult iResult){
    	try {
	        if (iResult.getStatus() == ITestResult.FAILURE || iResult.getStatus() == ITestResult.SKIP) {
	            testResult = "FAILED";
	            screenshotPath = ActionGen.takeScreenshot(method.getName());
	            screenshotURL1 = ActionGen.getImageUrl1();
	            screenshotURL2 = ActionGen.getImageUrl2();
	        } else if (iResult.getStatus() == ITestResult.SUCCESS) {
	            testResult = "PASSED";
	        }
	        MyLogger.info("Test result is: " + testResult);
    	}catch(Exception e) {
    		MyLogger.error("Failed to update test result: " + e.getMessage());
    		e.printStackTrace();
    	}
    }

    public void insertIntoReport(){
        //report.updateExtent(testResult, method.getName(), exception, screenshotPath);
        report.updateExtent(testResult, method.getName(), exception, screenshotURL2);
        report.endExtent();
    }
    
    public void insertIntoDatabase(ITestResult iResult, ITestContext iContext){
        String testSuiteName = iContext.getCurrentXmlTest().getSuite().getName();
        long duration = iResult.getEndMillis() - iResult.getStartMillis();
        long second = (duration / 1000) % 60;
        long minute = (duration / (1000 * 60)) % 60;
        long hour = (duration / (1000 * 60 * 60)) % 24;
        String durationStr = String.format("%02d:%02d:%02d", hour, minute, second, duration);
        String suiteName = iContext.getCurrentXmlTest().getSuite().getName();

        executionDate = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd");
        executionTime = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset,
                "yyyy-MM-dd HH:mm:ss");
        id = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset, "ddMMyyHHmmssSSS");

        if(firstRun.equals("")) {
        	firstRun = testResult;
        }
            
        if(firstRun.equals("PASSED")){
            secondRun = "PASSED";
            finalRun = secondRun;
            count = 2;
        }else{
        	count++;
        	if(count==2) {
        		secondRun = testResult;
                finalRun = secondRun;
                if(finalRun.equals("PASSED")) {
                    screenshotURL2 = "";
                }
        	}
        }

        if(environment!=null && System.getProperty("manualExecution")!=null)
            updateManualTestCaseResult(testResult,System.getProperty("manualExecution"),"tanhp",durationStr,reportUrl,screenshotURL2);

        if(count==2) {
        	values.add(0, id);
            values.add(1, suiteName);
            values.add(2, className);
            values.add(3, method.getName());
            values.add(4, executionDate);
            values.add(5, executionTime);
            values.add(6, durationStr);
            values.add(7, firstRun);
            values.add(8, secondRun);
            values.add(9, finalRun);
            values.add(10, reportUrl);
            values.add(11, screenshotURL2);
            values.add(12, MyConstants.ENVIRONMENT);
            MyLogger.info("Detailed Test Result: ");
            MyLogger.info("--Run_ID: " + id);
            MyLogger.info("--TestSuite: " + testSuiteName);
            MyLogger.info("--TestMethod: " + method.getName());
            MyLogger.info("--Execution Date: " + executionDate);
            MyLogger.info("--Execution Time: " + executionTime);
            MyLogger.info("--Duration: " + durationStr);
            MyLogger.info("--First Run: " + firstRun);
            MyLogger.info("--Second Run: " + secondRun);
            MyLogger.info("--Final Run: " + finalRun);
            MyLogger.info("--Report URL: " + reportUrl);
            MyLogger.info("--Screenshot URL: " + screenshotURL2);
            MyLogger.info("--Environment: " + MyConstants.ENVIRONMENT);

            if(MyConstants.isWriteToDB() && !testSuiteName.equals("Default Suite")){
                if(environment.equals("pro"))
                    database.insertIntoDatabase(MyConstants.DB_TABLENAME, columns, values);
            }


            updateExcelFinalResult();
            sendFailedResultToHangouts(suiteName);
            count = 0;
            firstRun = "";
            secondRun = "";
            finalRun = "";
            screenshotURL2 = "";
        }
        
    }

    public void closeReport(){
        report.closeExtent();
    }

    public void closeDatabase(){
        if(MyConstants.isWriteToDB()){
            if(environment.equals("pro"))
                database.closeConnection();
        }
    }

    public void insertIntoHeaderFile(ITestContext testSuite){
        String testSuiteName = testSuite.getCurrentXmlTest().getSuite().getName();
        if(!testSuiteName.equals("Default Suite")) {
            if(true){
                MyLogger.info("Start to update results in header file");
                int noOfPass = testSuite.getPassedTests().size();
                System.out.println(noOfPass);
                int noOfFail = testSuite.getFailedTests().size();
                System.out.println(noOfFail);
                if (wbHeader == null)
                    wbHeader = XLSWorker.getWorkbook(MyConstants.HEADER_FILE);
                XSSFSheet sheet = XLSWorker.getSheet(wbHeader, "Header");
                int row = XLSWorker.getRowNumByColName(sheet, 0, testSuiteName);
                XLSWorker.updateHeaderResult(sheet, row, noOfPass, noOfFail);
                XLSWorker.writeHeaderFile(wbHeader);
            }
        }
    }

    public void insertIntoJSONFile(ITestContext testSuite){
    	MyLogger.info("Write test result into Json file...");
    	executionTimeSuite = FormatUtils.getCurrentTimeByTimezoneOffset(
                MyConstants.timezoneOffset, "yyyy-MM-dd HH:mm:ss");
        JSONObject result = new JSONObject();
        result.put("Passed", testSuite.getPassedTests().size());
        result.put("Failed", testSuite.getFailedTests().size());
        // result.put("Pending", testSuite.getSkippedTests().size());
        result.put("ExecutionTime", executionTimeSuite.substring(5, executionTimeSuite.length()));

        JSONArray resultList = new JSONArray();
        resultList.put(result);

        JSONObject suite = new JSONObject();
        suite.put(testSuite.getCurrentXmlTest().getSuite().getName(), resultList);
        ActionGen.writeJSONFile(suite, "resultSeller.json");
    }

    public void insertResultToDB(ITestContext testSuite){
        if(MyConstants.isWriteToDB()) {
            if(environment.equals("pro")){
                MyLogger.info("Start to update database resultSellerProduction");
                List<String> result_list = new ArrayList<String>();
                int passedCount = testSuite.getPassedTests().size();
                int failedCount = testSuite.getFailedTests().size();
                result_list.add(testSuite.getCurrentXmlTest().getSuite().getName());
                result_list.add(passedCount + "");
                executionTimeSuite = FormatUtils.getCurrentTimeByTimezoneOffset(
                        MyConstants.timezoneOffset, "yyyy-MM-dd HH:mm:ss");
                result_list.add(executionTimeSuite.substring(5, executionTimeSuite.length()));
                result_list.add(failedCount + "");
                result_list.add("Seller - Production");
                List<String> Columns = Arrays.asList("testSuite", "Passed", "ExecutionTime", "Failed");
                String suitename = database.getValueFromSelectedColumn("resultProduction", "testSuite", "testSuite", testSuite.getCurrentXmlTest().getSuite().getName());
                if (suitename != null && !testSuite.equals("Default Suite")) {
                    List<String> update_list = new ArrayList<String>();
                    update_list.add("Passed , " + passedCount);
                    update_list.add("ExecutionTime , " + executionTimeSuite.substring(5, executionTimeSuite.length()));
                    update_list.add("Failed , " + failedCount + "");
                    List<String> condition_list = new ArrayList<String>();
                    condition_list.add("testSuite , " + testSuite.getCurrentXmlTest().getSuite().getName());
                    database.updateDatabase("resultProduction", update_list, condition_list);
                } else {
                    if(!testSuite.getCurrentXmlTest().getSuite().getName().equals("Default Suite"))
                        database.insertIntoDatabase2("resultProduction", Columns, result_list);
                }
            }
        }
    }

    public void sendFailedResultToSkype(String suiteName){
        if(testResult.contains("FAILED")){
        	MyLogger.info("Send failed result via Skype...");
            Message skypeMessage = Message.create()
                    .with(Text.rich("====SELLER - Automation - "))
                    .with(Text.rich("\n TestSuite: " + suiteName).withBold())
                    .with(Text.rich(" - "))
                    .with(Text.rich("WARNING REPORT").withColor(Color.RED).withBold())
                    .with(Text.rich("\n Date: " + executionTime))
                    .with(Text.rich("\n TestCase: " + method.getName()).withBold())
                    .with(Text.rich("\n Result: " + "FAILED: " + MyLogger.getFailedMessage()))
            		.with(Text.rich("\n Screenshot: " + screenshotURL2));
            /*if(suiteName.equals("UAT")) {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }else {
                SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
            }*/
        }
    }

    public void sendFailedResultToHangouts(String suiteName){
        System.out.println("Hangouts");
        if(testResult.contains("FAILED")){
            MyLogger.info("Send failed result via Hangouts...");
            StringBuilder messageBuilder = new StringBuilder();

            messageBuilder.append("====SELLER - Automation - ");
            messageBuilder.append("\n TestSuite: " + suiteName);
            messageBuilder.append(" - ");
            messageBuilder.append("WARNING REPORT");
            messageBuilder.append("\n Date: " + executionTime);
            messageBuilder.append("\n TestCase: " + method.getName());
            messageBuilder.append("\n Result: " + "FAILED: " + MyLogger.getFailedMessage());
            messageBuilder.append("\n Screenshot: " + screenshotURL1);

            com.google.api.services.chat.v1.model.Message hangoutsMessage = new com.google.api.services.chat.v1.model.Message();
            hangoutsMessage.setText(messageBuilder.toString());
            GoogleHangouts.sendHangoutsMessage("AAAAOye9IqE",hangoutsMessage);
        }
    }

    public void sendFailedResultToSlack(String suiteName){
        if(testResult.contains("FAILED")){
            MyLogger.info("Send failed result via Slack...");
            String emailMessage = "====SELLER - Automation - "
                    + "\n TestSuite: " + suiteName
                    + " - "
                    + "\n Date: " + executionTime
                    + "\n TestCase: " + method.getName()
                    + "\n Result: " + testResult
                    + "\n Screenshot: " + screenshotURL1;
            String subject = "Daily Automation Seller - " + FormatUtils.getToday();
            String to1 = "tanhp@sendo.vn";
            //EmailSender.sendNotificationMail(subject,to1,emailMessage,null);
            //SlackSender.pushNotifyToSlack("#qc-team","Auto Bot", slackMessage);
        }
    }

    public void updateManualTestCaseResult(String resultTest, String manualTestExecutionIDList, String executedBy, String duration, String report, String screenshot) {

        /*if (result == ITestResult.FAILURE) {
            resultTest = "FAILED";
        } else if (result == ITestResult.SUCCESS) {
            resultTest = "PASSED";
        } else if (result == ITestResult.SKIP) {
            resultTest = "FAILED";
        }*/
        if(manualTestExecutionIDList == null)
            return;
        MyLogger.info("Start to update manual testcase result with id=" + manualTestExecutionIDList + ", result=" + resultTest + ", executedBy=" + executedBy);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String[] manualTestExecutionID = manualTestExecutionIDList.split(",");
        for(String item : manualTestExecutionID) {
            List<String> conditionValues = new ArrayList<String>();
            conditionValues.add("manualExecution , " + item);
            String manualTestCaseList = Database2.getValueFromSelectedColumnMultiCondition("automationexecutions", "firstRun",conditionValues);
            MyLogger.info("Manual Test Case List: " + manualTestCaseList);
            if(manualTestCaseList.equals("FAILED"))
            {
                List<String> values = new ArrayList<String>();
                values.add("secondRun , " + resultTest);
                values.add("finalResult , " + resultTest);
                values.add("duration , " + duration);
                values.add("errorScreenshot , " + screenshot);
                values.add("report , " + report);
                //values.add("executedBy, " + "tanhp");
                //values.add("executedOn, " + FormatUtils.getToday3());
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                Database2.updateDatabase("automationexecutions", values, conditionValues);
                //
                List<String> valuesTestExcutions = new ArrayList<String>();
                valuesTestExcutions.add("result , " + resultTest);
                conditionValues = new ArrayList<String>();
                conditionValues.add("id , " + item);
                Database2.updateDatabase("testexecutions", valuesTestExcutions, conditionValues);
            }else if(manualTestCaseList.equals("RUNNING")) {
                List<String> values = new ArrayList<String>();
                values.add("firstRun , " + resultTest);
                if(resultTest.equals("PASSED")) {
                    values.add("finalResult , " + resultTest);
                    values.add("duration , " + duration);
                    values.add("errorScreenshot , " + screenshot);
                    values.add("report , " + report);
                    //values.add("executedBy, " + "tanhp");
                    //values.add("executedOn, " + FormatUtils.getToday3());
                    conditionValues = new ArrayList<String>();
                    conditionValues.add("manualExecution , " + item);
                    Database2.updateDatabase("automationexecutions", values, conditionValues);
                    List<String> valuesTestExcutions = new ArrayList<String>();
                    valuesTestExcutions.add("result , " + resultTest);
                    conditionValues = new ArrayList<String>();
                    conditionValues.add("id , " + item);
                    Database2.updateDatabase("testexecutions", valuesTestExcutions, conditionValues);
                }
                else {
                    conditionValues = new ArrayList<String>();
                    values.add("duration , " + duration);
                    values.add("errorScreenshot , " + screenshot);
                    values.add("report , " + report);
                    //values.add("executedBy, " + "tanhp");
                    //values.add("executedOn, " + FormatUtils.getToday3());
                    conditionValues.add("manualExecution , " + item);
                    Database2.updateDatabase("automationexecutions", values, conditionValues);
                }

            }else {
                List<String> values = new ArrayList<String>();
                values.add("firstRun , " + resultTest);
                values.add("duration , " + duration);
                values.add("errorScreenshot , " + screenshot);
                values.add("report , " + report);
                values.add("executedBy, " + "tanhp");
                values.add("executedOn, " + FormatUtils.getToday3());
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                Database2.updateDatabase("automationexecutions", values, conditionValues);
            }
        }

//		List<List<String>> manualTestCaseList = Database.getManualTestCaseList("automationexecutions", ExecutionWeb.className, ExecutionWeb.method.getName());

//		for (List<String> item : manualTestCaseList) {
//			List<String> values = new ArrayList<String>();
//			values.add(0,"result , " + resultTest);
//			values.add(0,"comment , " + item.get(1));
//			values.add(0,"autoexe , " + "yes");
//			values.add(0,"createdOn , " + formatter.format(date));
//			values.add(0,"createdBy , " + executedBy);
//			List<String> conditionValues = new ArrayList<String>();
//			conditionValues.add("testCaseID , " + item.get(0));
//			conditionValues.add("testRunID , " + testRunID);
//			Database.updateDatabase("testexecutions", values, conditionValues);
//		}
        //   }
    }

    public static void updateManualTestExecutionStatus(String manualTestExecutionIDList) {
        MyLogger.info("Start to update manual test execution status with id=" + manualTestExecutionIDList);
        if(manualTestExecutionIDList == null)
            return;
        String[] manualTestExecutionID = manualTestExecutionIDList.split(",");
        for(String item : manualTestExecutionID) {
            List<String> conditionValues = new ArrayList<String>();
            conditionValues.add("manualExecution , " + item);
            String manualTestCaseList = Database2.getValueFromSelectedColumnMultiCondition("automationexecutions", "firstRun",conditionValues);

            if(manualTestCaseList.equals("QUEUED")){
                List<String> values = new ArrayList<String>();
                values.add("firstRun , RUNNING");
                values.add("finalResult , RUNNING");
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                Database2.updateDatabase("automationexecutions", values, conditionValues);
            }else if(manualTestCaseList.equals("FAILED")) {
                List<String> values = new ArrayList<String>();
                values.add("secondRun , RUNNING");
                values.add("finalResult , RUNNING");
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                Database2.updateDatabase("automationexecutions", values, conditionValues);
            }
        }
    }

    /*public static void resetManualTestExecutionStatus(String manualTestExecutionIDList) {
        String[] manualTestExecutionID = manualTestExecutionIDList.split(",");
        for(String item : manualTestExecutionID) {
            List<String> conditionValues = new ArrayList<String>();
            conditionValues.add("manualExecution , " + item);
            String firstStatus = Database.getValueFromSelectedColumnMultiCondition("automationexecutions", "firstRun",conditionValues);
            String secondStatus = Database.getValueFromSelectedColumnMultiCondition("automationexecutions", "secondRun",conditionValues);
            if(firstStatus.equals("QUEUED") || firstStatus.equals("RUNNING")) {
                conditionValues = new ArrayList<String>();
                List<String> values = new ArrayList<String>();
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                values.add("firstRun , READY");
                values.add("secondRun , READY");
                values.add("finalResult , READY");
                Database.updateDatabase("automationexecutions", values, conditionValues);

            }else if(firstStatus.equals("FAILED") && secondStatus.equals("QUEUED") || firstStatus.equals("FAILED") && secondStatus.equals("RUNNING")){
                conditionValues = new ArrayList<String>();
                List<String> values = new ArrayList<String>();
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                values.add("firstRun , READY");
                values.add("secondRun , READY");
                values.add("finalResult , READY");
                Database.updateDatabase("automationexecutions", values, conditionValues);
            }
        }
    }*/

}
