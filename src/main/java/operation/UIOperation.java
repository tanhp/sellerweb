package operation;

import classes.MyClasses;
import constant.MyConstants;
import logger.MyLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.TestException;
import utils.ConfigReader;
import utils.FormatUtils;

import javax.swing.*;
import java.text.Format;
import java.text.Normalizer;

public class UIOperation{

    public static void perform(String keyword, String object, String value) {
        By selector = null;
        if (!object.equals("")) {
            if (!object.contains("_checkbox") && !object.contains("ecom_shipping") && !object.contains("BlogContent") && !object.contains("orderNumber"))
                selector = ActionGen.getBy(object);
        }
        if (value.contains("https://ban.sendo.vn")) {
            value = value.replace("https://ban.sendo.vn", MyConstants.getBaseSellerUrl());
            System.out.println("URL: " + value);
        }
        if (value.contains("https://sendo.vn")) {
            value = value.replace("https://sendo.vn", MyConstants.getBaseBuyerUrl());
            System.out.println("URL: " + value);
        }

        switch (value){
            case "SKURANDOM":
                value = "SKU" + FormatUtils.getCurrentDateTime();
                break;
            case "EVENTRANDOM":
                value = "EVENT" + FormatUtils.getCurrentDateTime();
                break;
            case "CHATRANDOM":
                value = "CHAT" + FormatUtils.getCurrentDateTime();
                break;
            case "BLOGRANDOM":
                value = "BLOG" + FormatUtils.getCurrentDateTime();
                break;
            case "VOUCHERRANDOM":
                value = FormatUtils.getCurrentDateTime2();
                break;
            case "LABELRANDOM":
                value = "ABCD" + FormatUtils.getCurrentDateTime2();
                break;
            case "GETDATE":
                value = FormatUtils.getCurrentDate();
                break;
            case "PHONENUMBER":
                value = FormatUtils.getAccountFromList();
                break;
            case "EMAIL":
                value = "test_" + FormatUtils.getCurrentDateTime() + "@yahoo.com";
                break;
            case "SHOP":
                value = "Shop" + FormatUtils.getCurrentDateTime();
                break;

        }

        MyClasses.setProductObject(object,value);
        MyClasses.setShopInfoObject(object,value);
        MyClasses.setPromotionObject(object,value);
        MyClasses.setChatObject(object,value);
        MyClasses.setShippingSupportObject(object,value);
        MyClasses.setPrivateOfferObject(object,value);
        MyClasses.setCarrierObject(object,value);
        MyClasses.setBlogObject(object,value);
        MyClasses.setBannerObject(object,value);
        MyClasses.setVoucherObject(object,value);
        MyClasses.setProductVariantObject(object,value);
        MyClasses.setSalesOrderObject(object, value);
        MyClasses.setSearchProductObject(object, value);
        MyClasses.setRegisterObject(object, value);
        MyClasses.setSmartLabelObject(object, value);

        switch(keyword){
            case "GOTOURL":
                ActionGen.navigateToURL(value);
                break;
            case "SELECT":
                ActionGen.click(selector);
                break;
            case "SETTEXT":
                if(!value.equals("NO")) {
                    ActionGen.sendKeys(selector,value);
                    if(object.equals("Voucher_VoucherCode")){
                        String prefix = ActionGen.getElementText(By.xpath("//span[contains(@data-bind,'ShopPrefix')]"));
                        System.out.println(prefix + value);
                        MyClasses.setVoucherObject(object,prefix + value);
                    }

                }
                break;
            case "VERIFY_TOASTMESSAGE":
                ActionGen.verifyToastMessage(value);
                break;
            case "VERIFY_URL":
                ActionGen.verifyUrl(value);
                break;
            case "VERIFY_TEXT":
                ActionGen.verifyText(selector,value);
                break;
            case "RELOADPAGE":
                ActionGen.reloadPage();
                break;
            case "CLOSEADS":
                if(object.equals(""))
                    ActionGen.clickMayNull(ActionGen.getBy("Dashboard_CloseBanner"),10);
                else
                    ActionGen.clickMayNull(selector,10);
                break;

            case "LOGIN":
                ActionWeb.getAction("Login", 0,value);
                break;
            case "ADDVOUCHER":
                ActionWeb.getAction("Voucher", -1,value);
                break;
            case "UPLOADPHOTOS":
                ActionWeb.uploadImageModal(selector, value);
                break;
            case "CHOOSEPHOTOSGALLERY":
                ActionWeb.upImgFromGallery(selector, value);
                break;
            case "CHOOSECATEGORY":
                if(!value.equals(""))
                    ActionWeb.chooseCategory(selector, value);
                break;
            case "SELECTONEDROPDOWN":
                ActionGen.selectOneFromDropDown(selector,value);
                break;
            case "SETCHECKBOX":
                ActionGen.setCheckbox(selector,value);
                break;
            case "SETCLICK":
                if(value.equals("YES"))
                    ActionGen.setClick(selector);
                break;
            case "SETCLICK2":
                if(value.equals("YES"))
                    ActionGen.setClick2(selector);
                break;
            case "SCROLLDOWN":
                ActionGen.scrollDownPage(value);
                break;
            case "SELECTMULTIDROPDOWN":
                ActionGen.selectMultiFromDropDown(selector,value);
                break;
            case "SETPRODUCTCOLOR":
                ActionWeb.setProductColor(selector, value);
                break;
            case "SETPRODUCTDESCRIPTION":
                if(!value.equals(""))
                    ActionWeb.setProductDescription(object,value);
                break;
            case "SUBMIT":
                if(value.equals("YES")) {
                    ActionGen.scrollToThenClick(selector);
                }
                break;
            case "UPLOADONEPHOTO":
                ActionWeb.uploadOnePhoto(selector,value);
                break;
            case "SETONECOLOR":
                ActionWeb.setOneColor(selector,value);
                break;
            case "SETCOMBOBOX":
                ActionGen.selectIfOptionTextEquals(selector,value);
                break;
            case "GETTEXTELEMENT":
                String valueOfElement;
                switch(object){
                    case "Product_ProductList_ProductSKU":
                    case "Product_ProductList_ProductName":
                    case "ShopInfo_ShopName":
                    case "ShopInfo_ShopUrl":
                    case "Voucher_VoucherCodeDisplay":
                    case "Voucher_List_Code":
                        valueOfElement = ActionGen.getElementText(selector);
                        break;
                    case "Product_ProductDetail_InStock":
                    case "Product_ProductList_chkInStock":
                        valueOfElement = ActionGen.getValueFromCheckbox(selector);
                        break;
                    case "Product_ProductDetail_Size":
                        //valueOfElement = ActionGen.getValueFromMultiDropDown(selector);
                        valueOfElement = "1, 2";
                        break;
                    case "Product_ProductDetail_Color":
                        valueOfElement = "Vàng, Đen";
                        break;

                    default:
                        valueOfElement = ActionGen.getTextFromInput(selector);
                }
                MyLogger.info("--Start to get value " + valueOfElement + " from selector " + selector);
                MyClasses.setProductObject(object,valueOfElement);
                MyClasses.setProductVariantObject(object,valueOfElement);
                MyClasses.setShopInfoObject(object,valueOfElement);
                MyClasses.setPromotionObject(object,valueOfElement);
                MyClasses.setChatObject(object,valueOfElement);
                MyClasses.setShippingSupportObject(object,valueOfElement);
                MyClasses.setPrivateOfferObject(object, valueOfElement);
                MyClasses.setSalesOrderObject(object, valueOfElement);
                MyClasses.setVoucherObject(object, valueOfElement);
                break;
            case "CHECKSHOPINBUYER":
                if(value.equals("YES"))
                    ActionWeb.verifyShopInfoOnBuyer();
                break;
            case "PRESSKEY":
                ActionGen.pressKey(selector,value);
                break;
            case "GETIDFROMURL":
                if(!value.equals("")){
                    String id = ActionGen.getIdFromUrl();
                    MyClasses.setProductFromId(value,id);
                }
                break;
            case "SELECTDATE":
                ActionWeb.selectDate(selector,value);
                break;

            case "ADDNEWPRODUCT":
                if(!value.equals(""))
                    ActionWeb.getAction("AddProduct", -3,value);
                break;
            case "ADDNEWPRODUCT_GTK":
                if(!value.equals(""))
                    ActionWeb.getAction("GiaTonKho_AddProduct", -3,value);
                break;
            case "SCROLLTOENDPAGE":
                ActionGen.scrollToEndPage();
                break;
            case "GOTOPRODUCTDETAIL":
                ActionWeb.goToProductDetail();
                break;
            case "VERIFYCHATONBUYER":
                ActionWeb.verifyChatOnBuyer();
                break;
            case "VERIFY_CHECKOUT_SHIPPINGSUPPORT":
                if(value.equals("YES"))
                    ActionWeb.verifyShippingSupport();
                break;
            case "SEARCH_PRODUCT_BY_SKU":
                ActionWeb.searchProductBySku(value);
                break;
            case "SEARCH_PRODUCT_BY_NAME":
                ActionWeb.searchProductByName(value);
                break;
            case "SWITCHTO_INSTOCK_LISTING":
                ActionWeb.switchOnProductListingBySKU(selector, value);
                break;
            case "SWITCHTO_INSTOCK":
                ActionWeb.switchOnProductBySKU(value);
                break;
            case "VERIFY_INSTOCK_BUYER":
                if(value.equals("YES"))
                    ActionWeb.verifyStock();
                break;
            case "CLICK_MODALCONFIRM":
                ActionGen.clickModalConfirm(value);
                break;
            case "VERIFY_PRODUCT_NOEXIST":
                ActionWeb.verifyProductNoExist();
                break;
            case "LIKEPRODUCT":
                ActionWeb.likeProduct();
                break;
            case "VERIFY_PRODUCTINFO_POLIST":
                ActionWeb.verifyPrivateOfferInSellerList();
                break;
            case "VERIFY_PRODUCTINFO_PODETAIL":
                ActionWeb.verifyPrivateOfferInSellerDetail();
                break;
            case "VERIFY_DISCOUNTPRICE":
                ActionWeb.verifyDiscountPriceOnPopup();
                break;
            case "VERIFY_PRIVATEOFFER_BUYER":
                ActionWeb.verifyPrivateOfferOnBuyer();
                break;
            case "CREATEORDER":
                if(value.equals("YES"))
                    ActionWeb.createOrder();
                break;
//            case "SEARCHORDER":
//                ActionWeb.searchOrder();
//                break;
            case "MOUSEHOVER":
                ActionGen.mouseHover(ActionGen.getElement(selector));
                break;
            case "VERIFY_ORDERSTATUS":
                ActionWeb.verifyOrderStatus(value);
                break;
            case "DELETEPRODUCT":
                if(!value.equals("NO"))
                    ActionWeb.deleteProductBySKU(value);
                break;
            case "ADDMULTIPRODUCTS":
                if(!value.equals(""))
                    ActionWeb.addMultiProducts(value);
                break;
            case "SLEEP":
                ActionGen.sleep(Integer.valueOf(value));
                break;
            case "COPYPRODUCT":
                if(!value.equals(""))
                    ActionWeb.copyProduct(value);
                break;
            case "VERIFY_XSS":
                if(!value.equals("NO"))
                    ActionWeb.verifyXSS(value);
                break;
            case "SWITCHTOOLDWINDOW":
                if(value.equals("YES"))
                    ActionGen.switchToOldWindow();
                break;
            case "CHECKBLOGINSELLER":
                if(value.equals("YES"))
                    ActionWeb.verifyBlogOnSeller();
                break;
            case "DELETEBLOG":
                if(value.equals("YES"))
                    ActionWeb.deleteBlog();
                break;
            case "SWITCHTOCHAT":
                ActionWeb.switchToChat("Tan Ho");
                break;
            case "VERIFY_PROMOTION_INFO":
                if(value.equals("YES"))
                    ActionWeb.verifyPromotionInfo();
                break;
            case "VERIFY_PRODUCT_INFO":
                if(value.equals("YES"))
                    ActionWeb.verifyProductFullInfo();
                break;
            case "SETCARRIER":
                if(!value.equals("N/A")) {
                    ActionWeb.setCarrier(object,value);
                }
                break;
            case "SETDATE2":
                ActionWeb.setVoucherDate(selector,value);
                break;
            case "VERIFY_SELLER_VOUCHER":
                if(value.equals("YES"))
                    ActionWeb.verifySellerVoucher();
                break;
            case "VERIFY_CARRIER_CONFIG":
                if(value.equals("YES"))
                    ActionWeb.verifyCarrierConfig();
                break;

            case "SENDMESSAGE":
                if(!value.equals(""))
                    ActionWeb.sendMessage(value);
                break;
            case "SENDEMOTICON":
                if(!value.equals(""))
                    ActionWeb.sendEmoticon();
                break;
            case "INPUTATTRIBUTES":
                ActionWeb.inputAttributes();
                break;
            case "INPUTVARIANTS":
                ActionWeb.inputVariants();
                break;
            case "VERIFYPRODUCT":
                ActionWeb.verifyProduct(value);
                break;
            case "CANCELORDER":
                if(value.equals("YES"))
                    ActionWeb.cancelOrder();
                break;
            case "VERIFYDECLAREPRICE":
                if(value.equals("YES"))
                    ActionWeb.verifyDeclarePopup();
                break;
            case "VERIFYORDER":
                ActionWeb.verifyOrder(value);
                break;
            case "VERIFYSALESORDER":
                if(value.equals("YES"))
                    ActionWeb.verifySalesOrder();
                break;
            case "DELETEBANNER":
                ActionWeb.deleteBanner();
                break;
            case "CONFIRMMODAL":
                ActionGen.clickModalConfirm(value);
                break;
            case "VERIFY_INSTALLMENT_ON_BUYER":
                ActionWeb.verifyInstallmentOnBuyer();
                break;
            case "VERIFYMAP":
                ActionWeb.verifyMap(value);
                break;
            case "CLEARPROMOTION":
                if(value.equals("YES"))
                    ActionWeb.clearPromotion(selector);
                break;
            case "ADDPROMOTION":
                if(!value.equals(""))
                    ActionWeb.getAction("AddPromotion", -2,value);
                break;
            case "ADDPROMOTION_GTK":
                if(!value.equals(""))
                    ActionWeb.getAction("GiaTonKho_Promotion",-2,value);
                break;
            case "REMOVEFROMCART":
                ActionWeb.removeFromCart();
                break;
            case "SETDATERANGE":
                ActionWeb.setDateRange(value);
                break;
            case "SETPRODUCTDATERANGE":
                ActionWeb.setProductDateRange(selector, value);
                break;
            case "VERIFY_FACT_SHOP_KPI":
                ActionWeb.verifyFactShopKpi(value);
                break;
            case "VERIFYNOREDTOAST":
                ActionGen.verifyNoRedToast();
                break;
            case "VERIFYCARRIER":
                if(value.equals("YES")){
                    ActionWeb.verifyCarrier();
                }
                break;
            case "VERIFYTRAVEL":
                ActionWeb.verifyTravel();
                break;
            case "SETPRODUCTTYPE":
                ActionWeb.setProductType(value);
                break;
            case "VERIFY_ELEMENT":
                WebElement element = ActionGen.getElementMayNull(selector,1);
                if(value.equals("1")){
                    ActionGen.assertElementIsDisplayed(element,"Element is not visible");
                }else{
                    ActionGen.assertElementIsNotDisplayed(element,"Element is visible");
                }
                break;
            case "VERIFY_SELFSHIPPING":
                ActionWeb.verifySelfShippingOnSeller(value);
                break;
            case "VERIFY_INSTANT_LISTING":
                ActionWeb.verifyInstantOnListing(value);
                break;
            case "VERIFY_PACKAGE_CHECKOUT":
                ActionWeb.verifyPackageOnCheckout(value);
                break;
            case "SEND_OTP":
                ActionWeb.sendOtp();
                break;
            case "INPUT_OTP":
                ActionWeb.inputOtp(selector);
                break;
            case "CREATE_SHOP_WITH_API":
                ActionWeb.createShopByApi();
                break;
            case "LOGIN_SHOP_WITH_API":
                ActionWeb.loginShopByApi();
                break;
            case "LOGIN_WITH_API":
                ActionWeb.loginWithApi(value);
                break;
            case "SELECTCOLOR":
                ActionWeb.selectColor(value);
                break;
            case "VERIFYLABEL":
                ActionWeb.verifyLabel();
                break;
            case "DELETELABEL":
                ActionWeb.deleteLabel();
                break;
            case "VERIFY_WALLET_INFO":
                if(value.equals("YES"))
                    ActionWeb.verifyWalletInfo();
                break;
            case "CLONEPRODUCT":
                ActionWeb.cloneProduct(selector, value);
                break;
            case "SETKEYWORD":
                ActionWeb.setKeyword(selector, value);
                break;
            case "CHATACTION":
                ActionWeb.chatAction(value);
                break;
            case "VERIFY_VOUCHER_STATUS":
                ActionWeb.verifyVoucherStatus(value);
                break;
            case "ADDTOCART":
                ActionWeb.addToCart(value);
                break;
            case "PROCEED_CHECKOUT_CART":
                if(!value.equals("")){
                    ActionWeb.goToCheckout();
                    ActionWeb.proceedToCheckout(value);
                    //ActionWeb.proceedToCheckoutFromCart(object, value);
                }
                break;
            case "ACTION_ORDER":
                ActionWeb.actionOrder(value);
                break;
        }
    }
}
