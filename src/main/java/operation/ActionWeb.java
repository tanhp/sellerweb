package operation;

import api.APIController;
import classes.*;
import constant.MyConstants;
import logger.MyLogger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import utils.FormatUtils;
import utils.XLSWorker;

import java.text.Format;
import java.util.List;

import static operation.ParallelTest.exception;
import static org.testng.Assert.assertTrue;

public class ActionWeb {
    private static CProduct product;
    private static CBlog blog;
    private static CVoucher voucher;
    private static CShopInfo shopInfo;
    private static CChat chat;
    private static CPrivateOffer privateOffer;
    private static CShippingSupport shipping;
    private static CSalesOrder order;
    private static CSearchProduct searchProduct;
    private static CCarrier carrier;
    private static CRegister register;
    private static CSmartLabel smartLabel;

    public ActionWeb(){
        product = MyClasses.getProductObject();
        blog = MyClasses.getBlogObject();
        voucher = MyClasses.getVoucherObject();
        shopInfo = MyClasses.getShopInfoObject();
        chat = MyClasses.getChatObject();
        privateOffer = MyClasses.getPrivateOfferObject();
        shipping = MyClasses.getShippingSupport();
        order = MyClasses.getSalesOrderObject();
        searchProduct = MyClasses.getSearchProductObject();
        carrier = MyClasses.getCarrierObject();
        register = MyClasses.getRegisterObject();
        smartLabel = MyClasses.getSmartLabelObject();
    }

    public static void getAction(String sheetName, int num, String tcID){
        if(tcID.equals("") || tcID.equals("NO"))
            return;
        if(tcID.equals("YES"))
            tcID = MyConstants.getMainAccount();
        String fileAction = "TestAction.xlsx";
        String fileValidation = MyConstants.getFileValidation();
        XSSFWorkbook workbook =  XLSWorker.getWorkbook(MyConstants.INPUT_PATH + fileValidation);
        int rowNum = XLSWorker.getRowNumByTestCaseID(XLSWorker.getSheet(workbook,sheetName),tcID);

        //Object[][] objects = XLSWorker.getDataForUAT(fileAction,sheetName);
        Object[][] objects = XLSWorker.getDataForTestCase(fileAction, fileValidation, sheetName,rowNum);


        if(objects==null)
            return;
        for(int i=0; i< objects.length + num; i++){
            String keyword = objects[i][1].toString();
            String object = objects[i][2].toString();
            String value = objects[i][3].toString();
            MyLogger.info(keyword + " - " + object + " - " + value);
            UIOperation.perform(keyword,object,value);
        }
    }

    /* Search */

    public static void goToProductDetail(){
        ActionGen.click(ActionGen.getBy("Product_ProductList_ProductName"));
        ActionGen.waitForPageLoad();
        ActionGen.clickMayNull(ActionGen.getBy("Product_ProductDetail_Popup"),10);
    }

    public static void searchProductBySku(String sku){
        if(sku.equals("NEWSKU"))
            sku = product.getProductSku();
        MyLogger.info("--Start to search product by SKU " + sku);
        ActionGen.navigateToURL("URL_Product");
        dismissPopupOnProductList();
        By inputSku = ActionGen.getBy("Product_ProductSearch_ProductSKU");
        ActionGen.sendKeys(inputSku,sku);
        //sendKeys(inputSku,"SKU09072018233643");
        By btnSearch = ActionGen.getBy("Product_ProductSearch_btnSearch");
        ActionGen.click(btnSearch);
        ActionGen.sleep(1000);
        ActionGen.waitForAjaxDisappear();
        if(ActionGen.getElementMayNull(By.xpath("//em[contains(@data-bind,'VariantsLength')]"),1)!=null)
            product.setConfig(true);
    }

    public static void searchProductByName(String name){
        MyLogger.info("--Start to search product by Name " + name);
        ActionGen.navigateToURL("URL_Product");
        dismissPopupOnProductList();
        By inputName = ActionGen.getBy("Product_ProductSearch_ProductName");
        ActionGen.sendKeys(inputName,name);
        By btnSearch = ActionGen.getBy("Product_ProductSearch_btnSearch");
        ActionGen.click(btnSearch);
        ActionGen.sleep(1000);
        ActionGen.waitForAjaxDisappear();
    }

    public static void searchVoucher(String voucherCode){
        ActionGen.sendKeys(By.xpath("//input[contains(@data-bind,'VoucherSearch().Code')]"),voucherCode);
        ActionGen.click(By.xpath("//button[contains(@data-bind,'SearchClick')]"));
    }

    public static void searchOrder(String orderNumber){
        ActionGen.reloadPage();
        ActionGen.navigateToURL("https://ban.sendo.vn/shop#salesorder/New");
        ActionGen.sendKeys(By.xpath("//input[@placeholder='Mã đơn hàng']"),orderNumber);
        //ActionGen.sendKeys(By.xpath("//*[@id=\"content-wrapper\"]/div/div/form[1]/div/div[1]/div/div[1]/div/input"),"14254747883");
        ActionGen.click(By.xpath("//button[contains(@data-bind,'SearchClick()')]"));
        ActionGen.click(By.xpath("//a[text()='Tất cả ']"));
        ActionGen.sleep(1000);
    }

    /* Switch */

    public static void switchToInStock(By selector, String value){
        Boolean inputInStock = ActionGen.getElement(By.xpath("//input[contains(@data-bind,'ProductStock') and @class='onoffswitch-checkbox']")).isSelected();
        if(value.equals("YES")){
            if(!inputInStock)
                ActionGen.click(selector);
        }else{
            if(inputInStock)
                ActionGen.click(selector);
        }
    }

    public static void switchOffProductBySKU(String sku){
        try {
            ActionGen.reloadPage();
            searchProductBySku(sku);
            By locator = By.xpath("//div[@class='onoffswitch']");
            switchToInStock(locator, "NO");
            ActionGen.sleep(1500);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void switchOnProductListingBySKU(By selector, String value){
        Boolean inputInStock = ActionGen.getElement(By.xpath("//input[contains(@data-bind,'ProductStock') and @class='onoffswitch-checkbox']")).isSelected();
        if(value.equals("YES")){
            if(!inputInStock)
                ActionGen.click(selector);
            else{
                ActionGen.click(selector);
                ActionGen.click(selector);
            }
        }else{
            if(inputInStock)
                ActionGen.click(selector);
            else{
                ActionGen.click(selector);
                ActionGen.click(selector);
            }
        }
        ActionGen.waitForPageLoad();
        ActionGen.waitForAjaxDisappear();
    }

    public static void switchOnProductBySKU(String sku){
        if(sku.equals(""))
            return;
        ActionGen.reloadPage();
        searchProductBySku(sku);
        Boolean inputInStock = ActionGen.getElement(ActionGen.getBy("Product_ProductList_ProductStock")).isSelected();
        if(!inputInStock){
            goToProductDetail();
            ActionGen.setCheckbox(ActionGen.getBy("Product_ProductDetail_InStock"),"YES");
            ActionGen.scrollDownPage("500");
            if(product.getConfig())
                ActionGen.sendKeys(ActionGen.getBy("Product_ProductVariant_StockQuantity"),"100");
            else
                ActionGen.sendKeys(ActionGen.getBy("Product_ProductDetail_StockQuantity"),"100");
            ActionGen.scrollToThenClick(ActionGen.getBy("Product_ProductDetail_SendProductSubmit1"));
            ActionGen.waitForPageLoad();
        }
    }

    public static void switchToChat(String user) {
        WebElement iChat = ActionGen.getElement(ActionGen.getBy("Chat_ChatIFrame"));
        ActionGen.switchToNewFrame(iChat);
        try {
            ActionGen.sleep(1000);
            WebElement element = ActionGen.getElementMayNull(ActionGen.getBy("Chat_lblChat"),1);
            if(element==null){
                ActionGen.mouseHover(ActionGen.getElement(ActionGen.getBy("Chat_dropBtn")));
                ActionGen.click(ActionGen.getBy("Chat_allLi"));
                ActionGen.mouseHover(ActionGen.getElement(ActionGen.getBy("Chat_searchBoxBtn")));
            }
            ActionGen.click(By.xpath("//div[contains(@class,'nameLeft')]/span[text()='" + user + "']"));
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    /* Verify */

    public static void verifyDiscountPriceInSeller(){
        MyLogger.info("--Start to verify promotion in seller.");
        if(product.getConfig()) {
            String tmp_finalPrice = product.getDiscountMoney() + "đ";
            String finalPrice = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'FinalPriceMin')]")).getText();
            ActionGen.assertTextEquals(tmp_finalPrice,finalPrice,"Final price is not matched");
            String stockQuantity = ActionGen.getElement(By.xpath("//em[contains(@data-bind,'StockQuantity')]")).getText();
            ActionGen.assertTextEquals(stockQuantity,product.getStockQuantity(),"Stock quantity is not matched");
            return;
        }
        String tmp_productPrice = product.getProductPrice() + "đ";
        String tmp_discountPrice = product.getDiscountMoney() + "đ";
        String productPrice = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'ProductPrice')]")).getText();
        String discountPrice = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'PromotionPrice')]")).getText();
        String stockQuantity = ActionGen.getElement(By.xpath("//em[contains(@data-bind,'StockQuantity')]")).getText();
        ActionGen.assertTextEquals(stockQuantity,product.getStockQuantity(),"Stock quantity is not matched");

        ActionGen.assertTextEquals(tmp_productPrice,productPrice,"Product old price is not matched!");
        ActionGen.assertTextEquals(tmp_discountPrice,discountPrice,"Product discount price is not matched!");
    }

    public static void verifyDiscountPriceInBuyer(){
        MyLogger.info("--Start to verify promotion in buyer");
        WebElement eleOldPrice = ActionGen.getElement(By.xpath("//span[contains(@class,'oldPrice')]"));
        String oldPrice = ActionGen.waitForEmptyValueChanged(eleOldPrice,10);
        String currentPrice = ActionGen.getElement(By.xpath("//strong[contains(@class,'currentPrice')]")).getText();
        String temp_oldPrice = product.getProductPrice() + "đ";
        temp_oldPrice = temp_oldPrice.replace(",",".");
        String temp_currentPrice = product.getDiscountMoney() + "đ";
        temp_currentPrice = temp_currentPrice.replace(",", ".");
        ActionGen.assertTextEquals(temp_oldPrice,oldPrice,"Old price is not matched!");
        ActionGen.assertTextEquals(temp_currentPrice,currentPrice,"Product discount price is not matched!");
    }

    public static void verifyPrivateOfferInSellerList(){
        String productName = ActionGen.getElement(By.xpath("//div[contains(@data-bind,'NoteDisplay')]")).getText();
        String temp_productName = "Đã thích sản phẩm " + privateOffer.getProductName();
        ActionGen.assertTextEquals(productName,temp_productName,"Product name is not matched!");
    }

    public static void verifyPrivateOfferInSellerDetail(){
        ActionGen.sleep(2000);
        String productName = ActionGen.getElement(By.className("name_product")).getText();
        String productPrice = ActionGen.getElement(By.className("price_product")).getText();
        String tmp_productPrice = privateOffer.getOriginalMoneyFormat() + "đ";
        ActionGen.assertTextEquals(productName,privateOffer.getProductName(),"Product name verified on PO detail!");
        ActionGen.assertTextEquals(productPrice,tmp_productPrice,"Product price verified on PO detail");
    }

    public static void verifyDiscountPriceOnPopup(){
        ActionGen.sleep(1000);
        String currentPrice = ActionGen.getElement(By.className("cur_pri")).getText();
        String oldPrice = ActionGen.getElement(By.xpath("//*[@id=\"send_code_discount\"]/div[2]/div/div[2]/div/div[2]/p[2]/span[2]")).getText();
        String tmp_newPrice = privateOffer.getDiscountPrice() + "đ";
        String tmp_oldPrice = privateOffer.getOriginalMoneyFormat() + "đ";
        ActionGen.assertTextEquals(currentPrice,tmp_newPrice,"New price is not matched!");
        ActionGen.assertTextEquals(oldPrice,tmp_oldPrice,"Old price is not matched!");
    }

    public static void verifyProductNoExist(){
        WebElement element = ActionGen.getElementMayNull(By.xpath("//tbody[contains(@data-bind,'ProductList')]/tr"),1);
        ActionGen.assertElementIsNotDisplayed(element, "Product is still visible!");
    }

    public static void verifyProductNoDuplicate(){
        List<WebElement> products = ActionGen.getElements(By.xpath("//tbody[contains(@data-bind,'ProductList')]/tr"));
        ActionGen.assertBooleanEquals(products.size() == 1, true, "Product is still duplicated!");
    }

    public static void verifyPromotionInfo(){
        searchProductBySku(product.getProductSku());
        if(product.getPromotion()){
            verifyDiscountPriceInSeller();
            ActionGen.clickByJS(ActionGen.getElement(By.xpath("//i[@class='fa fa-eye']")));
            ActionGen.switchToNewWindow();
            verifyDiscountPriceInBuyer();
            ActionGen.switchToOldWindow();
            switchOffProductBySKU(product.getProductSku());
        }else{
            verifyNoPromotion();
        }

    }

    public static void verifyNoPromotion(){
        if(product.getConfig()){
            String tmp_finalPrice = product.getProductPrice() + "đ";
            String finalPrice = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'FinalPriceMin')]")).getText();
            ActionGen.assertTextEquals(tmp_finalPrice,finalPrice,"Final price is not matched");
        }else{
            String tmp_productPrice = product.getProductPrice() + "đ";
            String productPrice = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'ProductPrice')]")).getText();
            WebElement discountPrice = ActionGen.getElementMayNull(By.xpath("//span[contains(@data-bind,'PromotionPrice') or contains(@data-bind,'FinalPriceMin')]"),1);
            ActionGen.assertElementIsNotDisplayed(discountPrice,"Promotion is not off");
            ActionGen.assertTextEquals(tmp_productPrice,productPrice,"Product price is not matched");
        }
        ActionGen.clickByJS(ActionGen.getElement(By.xpath("//i[@class='fa fa-eye']")));
        ActionGen.switchToNewWindow();
        WebElement eleOldPrice = ActionGen.getElementMayNull(By.xpath("//span[contains(@class,'oldPrice')]"),1);
        ActionGen.assertElementIsNotDisplayed(eleOldPrice,"Promotion is not off");
        String currentPrice = ActionGen.getElement(By.xpath("//strong[contains(@class,'currentPrice')]")).getText();
        String temp_currentPrice = product.getProductPrice() + "đ";
        temp_currentPrice = temp_currentPrice.replace(",", ".");
        ActionGen.assertTextEquals(temp_currentPrice,currentPrice,"Product price is not matched");
        ActionGen.switchToOldWindow();
        switchOffProductBySKU(product.getProductSku());
    }



    public static void verifyProductFullInfoInSeller(){
        MyLogger.info("--Start to verify product full info in seller");
        String aProductSku = ActionGen.getElement(ActionGen.getBy("Product_ProductList_ProductSKU")).getText();
        String aProductName = ActionGen.getElement(ActionGen.getBy("Product_ProductList_ProductName")).getText() ;
        String sProductPrice;
        String sStockQuantity;
        if(product.getConfig()){
            sProductPrice = ActionGen.getElement(ActionGen.getBy("Product_ProductList_FinalPriceMin")).getText();
            sStockQuantity = ActionGen.getElement(ActionGen.getBy("Product_ProductList_StockQuantity")).getText();
        }else{
            sProductPrice = ActionGen.getElement(ActionGen.getBy("Product_ProductList_ProductPrice")).getText();
            sStockQuantity = ActionGen.getElement(ActionGen.getBy("Product_ProductList_StockQuantity")).getText();
        }
        Boolean inputInStock = ActionGen.getElement(ActionGen.getBy("Product_ProductList_chkInStock")).isSelected();
        WebElement eyeIcon = ActionGen.getElement(ActionGen.getBy("Product_ProductList_EyeIcon"));

        if(product.getProductType()==2){
            WebElement voucher_ic = ActionGen.getElementMayNull(By.className("voucher_ic"),1);
            ActionGen.assertElementIsDisplayed(voucher_ic, "Voucher IC is not displayed.");
        }else if(product.getProductType()==3){
            WebElement evoucher_ic = ActionGen.getElementMayNull(By.className("evoucher_ic"),1);
            ActionGen.assertElementIsDisplayed(evoucher_ic, "EVoucher IC is not displayed.");
        }

        ActionGen.assertTextEquals(aProductSku,product.getProductSku(),"Product SKU is not matched!");
        ActionGen.assertTextEquals(aProductName,product.getProductName(),"Product name is not matched!");
        String productPrice = product.getProductPrice() + "đ";
        ActionGen.assertTextEquals(sProductPrice,productPrice,"Product price is not matched!");
        ActionGen.assertTextEquals(sStockQuantity,product.getStockQuantity(),"Stock quantity is not matched");
        ActionGen.assertBooleanEquals(inputInStock,product.getInStock(),"Product instock is not matched!");
        ActionGen.assertElementIsDisplayed(eyeIcon,"Product eye icon is not visible!");
    }

    public static void verifyProductFullInfoInBuyer(){
        MyLogger.info("--Start to verify product full info in buyer");
        String price = ActionGen.getElement(ActionGen.getBy("Buyer_ProductDetail_CurrentPrice")).getText();
        String name_sku = ActionGen.getElement(ActionGen.getBy("Buyer_ProductDetail_ProductName")).getText();
        String productNameSku = product.getProductName() + " - " + product.getProductSku();
        ActionGen.assertTextEquals(name_sku,productNameSku,"Product name & SKU buyer are not matched!");
        String temp_productPrice = product.getProductPrice() + "đ";
        temp_productPrice = temp_productPrice.replace(",", ".");
        ActionGen.assertTextEquals(price,temp_productPrice,"Product price is not matched!");
        ActionGen.getElement(By.xpath("//button[contains(@class,'btn-disabled') or contains(@class,'buynow')]"));
        verifyInStockOnBuyer();
    }

    public static void verifyProductFullInfo(){
        searchProductBySku(product.getProductSku());
        verifyProductNoDuplicate();
        verifyProductFullInfoInSeller();
        MyLogger.info("Is save draft?: " + product.getSaveDraft());
        if(!product.getProductName().contains("A_") && !product.getSaveDraft()) {
            ActionGen.clickByJS(ActionGen.getElement(ActionGen.getBy("Product_ProductList_EyeIcon")));
            ActionGen.switchToNewWindow();
            verifyProductFullInfoInBuyer();
            ActionGen.switchToOldWindow();
            switchOffProductBySKU(product.getProductSku());
        }
    }


    public static void verifyCarrierConfig(){
        //searchProductBySkuWithCarrierConfig(product.getProductSku());
        WebElement iconOnSeller = ActionGen.getElement(By.xpath("//img[contains(@data-bind,'shipping_images_resize')]"));
        if(iconOnSeller == null || !iconOnSeller.isDisplayed())
            ActionGen.logFail("Icon is failed");
        MyLogger.info("Icon is passed");
        ActionGen.click(By.xpath("//a[contains(@data-bind,'ProductLink')]"));
        ActionGen.switchToNewWindow();
        WebElement iconOnBuyer = ActionGen.getElement(By.xpath("//img[contains(@class,'shippingExpress')]"));
        if(iconOnBuyer == null || !iconOnBuyer.isDisplayed())
            ActionGen.logFail("Icon is failed");
        MyLogger.info("Icon is passed");
    }

    public static void verifyProduct(String type){
        ActionGen.sleep(1000);
        List<WebElement> products = ActionGen.getElements(By.xpath("//tbody[contains(@data-bind,'ProductList')]/tr"));
        ActionGen.assertBooleanEquals(products.size() > 0, true, "No product is displayed!");
        for(int i=0; i<products.size(); i++){
            if(!searchProduct.getProductCategory().equals("")){
                String productCate = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductCategory")).getText();
                ActionGen.assertTextContains(productCate, FormatUtils.matchAndReplaceNonEnglishChar(searchProduct.getProductCategory()), "Category is not matched!");
            }
            if(!searchProduct.getProductName().equals("")) {
                String productName = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductName")).getText();
                ActionGen.assertTextContains(productName,searchProduct.getProductName(),"There is a product name is not correct");
            }
            if(!searchProduct.getProductSku().equals("")){
                String productSku = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductSKU")).getText();
                ActionGen.assertTextContains(productSku,searchProduct.getProductSku(),"There is a product sku is not correct");
            }
            if(!searchProduct.getProductStatus().equals("")){
                String productStatus = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductStatus")).getText();
                ActionGen.assertTextContains(productStatus,searchProduct.getProductStatus(),"There is a product status is not correct");
            }
            if(!searchProduct.getProductStock().equals("")){
                WebElement productStock = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductStock"));
                ActionGen.assertElementIsNotSelected(productStock,"There is a product stock is not correct");
            }
            if(!searchProduct.getProductPackage().equals("")){
                WebElement productPackage = products.get(i).findElement(ActionGen.getBy("Product_InstantIcon"));
                ActionGen.assertElementIsDisplayed(productPackage,"Icon hoa toc is not displayed!");
            }

            if(!searchProduct.getProductDate().equals("")){
                String[] dates = searchProduct.getProductDate().split(" - ");
                System.out.println(dates[0]);
                System.out.println(dates[1]);
                String productDate = products.get(i).findElement(ActionGen.getBy("Product_ProductList_ProductDate")).getText();
                System.out.println(productDate);
                ActionGen.assertBooleanEquals(FormatUtils.checkDate(dates[0], dates[1], productDate),true,"Product date is not correct");
            }
        }
    }

    public static void verifyPrivateOfferOnBuyer(){
        ActionGen.navigateToURL("https://www.sendo.vn/notification/index/");
        ActionGen.clickMayNull(By.xpath("//*[@id=\"om-uztnvnc7kn8abvvgs6dp-optin\"]/div/button"),10);
        List<WebElement> notifys = ActionGen.getElements(By.xpath("//a[contains(@href,'https://www.sendo.vn/uu-dai/')]"));
        String content = "Shop shopbaby47 giảm giá "
                + privateOffer.getDiscountPercent()
                + "% cho riêng bạn đến hết ngày "
                + privateOffer.getExpiredDate()
                + "!";
        ActionGen.assertTextEquals(notifys.get(0).getText(),content,"Content notify is not verified!");
        String link = notifys.get(0).getAttribute("href");
        ActionGen.clickByJS(notifys.get(0));

        ActionGen.switchToNewWindow();
        String url = ActionGen.getCurrentURL();
        ActionGen.assertTextEquals(url,link,"URL private offer is not verified!");

        String productName = ActionGen.getElement(By.xpath("//strong[@class='fn']/span")).getText();
        String productNameSku = product.getProductName() + " - " + product.getProductSku();
        ActionGen.assertTextEquals(productName,productNameSku,"Product name is not verified!");

        String oldPrice = ActionGen.getElement(By.xpath("//div[@class='old_price']/span")).getText();
        String currentPrice = ActionGen.getElement(By.xpath("//div[@class='new_price']/span")).getText();
        String temp_oldPrice = "Giá cũ: " + privateOffer.getOriginalMoneyFormat() + " đ";
        String temp_currentPrice = privateOffer.getDiscountPrice() + " đ";
        ActionGen.assertTextEquals(temp_oldPrice,oldPrice,"Product old price is not verified!");
        ActionGen.assertTextEquals(temp_currentPrice,currentPrice,"Product discount price is not verified!");

        String text = ActionGen.getElement(By.xpath("//div[@class='new_price']/i")).getText();
        ActionGen.assertTextEquals(text,"Ưu đãi bí mật","Text is not verified!");
        WebElement clock = ActionGen.getElement(By.xpath("//div[@class='count-down']"));
        ActionGen.assertElementIsDisplayed(clock,"Clock is not verified!");
        goToCheckout();
        WebElement voucher = ActionGen.getElement(By.xpath("//div[contains(@class,'voucherInputed')]"));
        ActionGen.assertElementIsDisplayed(voucher,"Voucher is not verified!");
        ActionGen.switchToOldWindow();
    }

    public static void verifySellerVoucher(){
        searchVoucher(voucher.getVoucherCode());
        verifyVoucherInListing();
        verifyVoucherDetail();
        verifyVoucherCard();
    }

    public static void verifyVoucherStatus(String value){
        searchVoucher(voucher.getVoucherCode());
        String voucherStatus = ActionGen.getElementText(ActionGen.getBy("Voucher_List_VoucherStatus"));
        ActionGen.assertTextEquals(voucherStatus,value,"VoucherStatus is not matched");

    }

    public static void verifyVoucherInListing(){
        MyLogger.info("Start to verify voucher in listing");
        if(voucher.getDiscountPercent()){
            WebElement img = ActionGen.getElement(ActionGen.getBy("Voucher_List_PercentIcon"));
            ActionGen.assertElementIsDisplayed(img, "Voucher percent logo is not matched");
            String expected_VoucherValue = voucher.getPercentAmount() + "%";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_List_Value"));
            ActionGen.assertTextEquals(expected_VoucherValue,actual_VoucherValue,"Voucher value is not matched");
        }else if(voucher.getDiscountMoney()){
            WebElement img = ActionGen.getElement(ActionGen.getBy("Voucher_List_MoneyIcon"));
            ActionGen.assertElementIsDisplayed(img, "Voucher money logo is not matched");
            String expected_VoucherValue = FormatUtils.formatNumber(String.valueOf(voucher.getDiscountAmount()).replace(".0", ""), "#,000") + "đ";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_List_Value"));
            ActionGen.assertTextEquals(expected_VoucherValue,actual_VoucherValue,"Voucher value is not matched");
        }
        String actual_VoucherCode = ActionGen.getElementText(ActionGen.getBy("Voucher_List_Code"));
        ActionGen.assertTextContains(actual_VoucherCode,voucher.getVoucherCode(),"Voucher code is not matched");
        String expected_MinOrderAmount = FormatUtils.formatNumber(String.valueOf(voucher.getMinOrderAmount()).replace(".0", ""), "#,000") + "đ";
        String actual_MinOrderAmount = ActionGen.getElementText(ActionGen.getBy("Voucher_List_MinOrderAmount"));
        ActionGen.assertTextEquals(actual_MinOrderAmount,expected_MinOrderAmount,"Min Order Amount is not matched");
        String expected_NumOfUse = "0/" + voucher.getNumOfUse();
        String actual_NumOfUse = ActionGen.getElementText(ActionGen.getBy("Voucher_List_NumOfUsedText"));
        ActionGen.assertTextEquals(actual_NumOfUse,expected_NumOfUse,"Num of use is not matched");
        String timeStatus = ActionGen.getElementText(ActionGen.getBy("Voucher_List_TimeStatus"));
        ActionGen.assertTextEquals(timeStatus,"Chưa bắt đầu","TimeStatus is not matched");
        String voucherStatus = ActionGen.getElementText(ActionGen.getBy("Voucher_List_VoucherStatus"));
        ActionGen.assertTextEquals(voucherStatus,"Đang kích hoạt","VoucherStatus is not matched");
        String expected_ApprovedDate = voucher.getApprovedDate() + " " + "00:00";
        String actual_ApprovedDate = ActionGen.getElementText(ActionGen.getBy("Voucher_List_ApprovedDate"));
        ActionGen.assertTextEquals(actual_ApprovedDate,expected_ApprovedDate,"ApprovedDate is not matched");
        String expected_ExpiredDate = voucher.getApprovedDate() + " " + "23:59";
        String actual_ExpiredDate = ActionGen.getElementText(ActionGen.getBy("Voucher_List_ExpireDate"));
        ActionGen.assertTextEquals(actual_ExpiredDate,expected_ExpiredDate,"ExpireDate is not matched");
    }

    public static void verifyVoucherDetail(){
        MyLogger.info("Start to verify voucher in detail");
        ActionGen.click(ActionGen.getBy("Voucher_List_View"));
        if(voucher.getDiscountPercent()){
            String expected_VoucherValue = voucher.getPercentAmount() + "%";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_Detail_Value"));
            ActionGen.assertTextEquals(actual_VoucherValue,expected_VoucherValue,"Voucher value is not matched");
        }else if(voucher.getDiscountMoney()){
            String expected_VoucherValue = FormatUtils.formatNumber(String.valueOf(voucher.getDiscountAmount()).replace(".0", ""), "#,000") + "đ";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_Detail_Value"));
            ActionGen.assertTextEquals(actual_VoucherValue,expected_VoucherValue,"Voucher value is not matched");
        }

        String actual_VoucherCode = ActionGen.getElementText(ActionGen.getBy("Voucher_Detail_Code"));
        ActionGen.assertTextContains(actual_VoucherCode,voucher.getVoucherCode(),"Voucher code is not matched");
        String expected_NumOfUse = "0/" + voucher.getNumOfUse();
        String actual_NumOfUse = ActionGen.getElementText(ActionGen.getBy("Voucher_Detail_NumOfUsedText"));
        ActionGen.assertTextEquals(actual_NumOfUse,expected_NumOfUse,"Num of use is not matched");
        ActionGen.click(ActionGen.getBy("Voucher_Detail_btnHistory"));
    }

    public static void verifyVoucherCard(){
        MyLogger.info("Start to verify voucher card");
        ActionGen.click(ActionGen.getBy("Voucher_Card_Download"));
        if(voucher.getDiscountPercent()){
            String expected_VoucherValue = voucher.getPercentAmount() + "%";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_Card_Value"));
            ActionGen.assertTextEquals(actual_VoucherValue,expected_VoucherValue,"Voucher value is not matched");
            String actual_MaxVoucherAmount = ActionGen.getElementText(ActionGen.getBy("Voucher_Card_MaxVoucherAmount"));
            String expected_MaxVoucherAmount = FormatUtils.formatNumber(String.valueOf(voucher.getMaxVoucherAmount()).replace(".0", ""), "#,000") + "đ";
            ActionGen.assertTextEquals(actual_MaxVoucherAmount,expected_MaxVoucherAmount,"Max voucher amount is not matched");
        }else if(voucher.getDiscountMoney()){
            String expected_VoucherValue = FormatUtils.formatNumber(String.valueOf(voucher.getDiscountAmount()).replace(".0", ""), "#,000") + "đ";
            String actual_VoucherValue = ActionGen.getElementText(ActionGen.getBy("Voucher_Card_Value"));
            ActionGen.assertTextEquals(actual_VoucherValue,expected_VoucherValue,"Voucher value is not matched");
        }

        String actual_VoucherCode = ActionGen.getElementText(ActionGen.getBy("Voucher_Card_Code"));
        ActionGen.assertTextContains(actual_VoucherCode,voucher.getVoucherCode(),"Voucher code is not matched");
        String expected_MinOrderAmount = FormatUtils.formatNumber(String.valueOf(voucher.getMinOrderAmount()).replace(".0", ""), "#,000") + "đ";
        String actual_MinOrderAmount = ActionGen.getElementText(ActionGen.getBy("Voucher_Card_MinOrderAmount"));
        ActionGen.assertTextEquals(actual_MinOrderAmount,expected_MinOrderAmount,"Min Order Amount is not matched");
        ActionGen.click(ActionGen.getBy("Voucher_Card_btnDownload"));
        ActionGen.verifyToastMessage("");
    }

    public static void chatAction(String value){
        List<WebElement> buttons = ActionGen.getElements(By.xpath("//div[contains(@class,'container_3TGy')]"));
        switch(value){
            case "quickText":
                ActionGen.click(By.xpath("//button[contains(@class,'quickReplyTextBtn')]"));
                String quickText = ActionGen.getElementText(By.xpath("//button[contains(@class,'quickReplyTextBtn')]/span"));
                chat.setQuickText(quickText);
                break;
            case "orderNumber":
                ActionGen.click(By.xpath("//button/img[@alt='order-icon']"));
                WebElement order = ActionGen.getElement(By.xpath("//div[contains(@class,'orderItem')]"));
                ActionGen.mouseHover(order);
                String orderNumber = order.findElement(By.xpath("*//div[contains(@class,'title')]/span[contains(@class,'label')]")).getText().replace("Đơn hàng: #","");
                chat.setOrderNumber(orderNumber);
                ActionGen.clickByJS(order.findElement(By.xpath("*//button[contains(@class,'sendButton')]")));
                break;
            case "product":
                ActionGen.click(By.xpath("//*[local-name()='svg' and contains(@class,'productIcon')]"));
                WebElement product = ActionGen.getElement(By.xpath("//div[contains(@class,'productItem')]"));
                String productName = product.findElement(By.xpath("*//h3[contains(@class,'heading')]/span[contains(@class,'truncateMedium')]")).getText();
                chat.setProductName(productName);
                String productPrice = product.findElement(By.xpath("*//div[contains(@class,'pricing')]/p[contains(@class,'finalPrice')]")).getText();
                chat.setProductPrice(productPrice);
                ActionGen.clickByJS(product);
                ActionGen.click(By.xpath("//button[contains(@class,'selectionButton')]"));
                break;
            case "photo":
                ActionGen.click(By.xpath("//span[text()='Gửi hình ảnh']/preceding-sibling::button"));
                ActionGen.uploadByRobot("voucher.jpg");
                break;
            case "voucher":
                ActionGen.click(By.xpath("//span[text()='Tạo mã giảm giá']/preceding-sibling::button"));
                String money = "20000";
                ActionGen.sendKeys(By.id("voucherValue"),money);
                chat.setVoucher(money);
                ActionGen.click(By.xpath("//button[contains(@class,'submitBtn')]"));
                break;
            case "message":
                String message = "Hello Shop!";
                ActionGen.sendKeys(ActionGen.getBy("Chat_ChatTextBox"),"Hello Shop!");
                chat.setMessage(message);
                ActionGen.pressKey(ActionGen.getBy("Chat_ChatTextBox"),"ENTER");
                break;
            case "emoticon":
                ActionGen.click(By.xpath("//button[contains(@class,'emojiBtnContainer')]"));
                ActionGen.click(By.xpath("//button[contains(@class,'iconBtn')]"));
                ActionGen.pressKey(ActionGen.getBy("Chat_ChatTextBox"),"ENTER");
                break;
        }

        verifyChatOnSeller(value);
    }

    public static void verifyChatOnSeller(String value){
        ActionGen.sleep(1000);
        List<WebElement> messageItems = ActionGen.getElements(By.xpath("//div[contains(@class,'item')]"));
        System.out.println(messageItems.size());
        WebElement messageItem = messageItems.get(messageItems.size()-1);
        switch(value){
            case "quickText":
                String quickText = messageItem.findElement(By.xpath("*//div[contains(@class,'textMessage')]/span")).getText();
                ActionGen.assertTextEquals(quickText, chat.getQuickText(), "Quick message is not correct!");
                break;
            case "orderNumber":
                String orderNumber = messageItem.findElement(By.xpath("*//div[contains(@class,'orderContent')]/div/span")).getText().replace("Đơn hàng: #","");
                ActionGen.assertTextEquals(orderNumber, chat.getOrderNumber(), "Order Number is not correct!");
                break;
            case "product":
                String productName = messageItem.findElement(By.xpath("*//h3[contains(@class,'heading')]/span[contains(@class,'truncateMedium')]")).getText();
                String productPrice = messageItem.findElement(By.xpath("*//div[contains(@class,'pricing')]/p[contains(@class,'finalPrice')]")).getText();
                ActionGen.assertTextEquals(productName, chat.getProductName(), "Product name is not correct!");
                ActionGen.assertTextEquals(productPrice, chat.getProductPrice(), "Product price is not correct!");
                break;
            case "photo":
                messageItem.findElement(By.xpath("*//div[contains(@class,'imagesMessage')]/a/img[contains(@src,'.scdn.vn')]"));
                break;
            case "voucher":
                String money = messageItem.findElement(By.xpath("*//p[contains(@class,'voucherText')]")).getText().replace(".","");
                ActionGen.assertTextContains(money, chat.getVoucher(), "Money is not correct");
                break;
            case "message":
                String message = messageItem.findElement(By.xpath("*//div[contains(@class,'textMessage')]/span")).getText();
                ActionGen.assertTextEquals(message, chat.getMessage(), "Message is not correct!");
                break;
            case "emoticon":
                messageItem.findElement(By.xpath("*//div[contains(@class,'biggestEmoji')]"));
                break;

        }
    }

    public static void verifyChatOnBuyer() {
        ActionGen.navigateToURL("URL_BaseBuyerUrl");
        dismissPopup();
        ActionGen.click(ActionGen.getBy("Buyer_ChatButton"));
        ActionGen.sleep(1000);
        loginOnPopup();
        switchToChat("shopbaby47");
        List<WebElement> content = ActionGen.getElements(ActionGen.getBy("Chat_Content"));
        String contentMessage = content.get(content.size()-1).getText();
        ActionGen.assertTextContains(contentMessage,chat.getContentMessage(),"Content message is not matched!");
    }

    public static void verifyBlogOnSeller(){
        ActionGen.reloadPage();
        ActionGen.navigateToURL("URL_Blog");
        List<WebElement> blogs = ActionGen.getElements(By.className("list_blog"));
        List<WebElement> titles = ActionGen.getElements(By.className("title"));
        List<WebElement> descs = ActionGen.getElements(By.className("desc"));
        ActionGen.scrollToElement(ActionGen.getElements(By.className("title")).get(blogs.size()-1));
        String title = ActionGen.getElements(By.className("title")).get(blogs.size()-1).getText();
        ActionGen.assertTextEquals(title,blog.getName(),"Blog title is not matched!");
        String desc =ActionGen.getElements(By.className("desc")).get(blogs.size()-1).getText();
        ActionGen.assertTextEquals(desc,blog.getDescription(),"Blog description is not matched!");
        ActionGen.getElements(By.className("title")).get(blogs.size()-1).click();
    }

    public static void verifyBlogDetail(){
        ActionGen.reloadPage();
        ActionGen.setClick(By.className("shop_name"));
        ActionGen.switchToNewWindow();
        ActionGen.navigateToURL(blog.getBlogUrl());
        String title = ActionGen.getElement(By.xpath("//h3[@class='title']")).getText();
        String desc = ActionGen.getElement(By.className("txt-news-desc")).getText();
        String content = ActionGen.getElement(By.xpath("//*[@id=\"content\"]/div/div/p[3]")).getText();
        if(!title.equals(blog.getName()))
            ActionGen.logFail("Blog title is not matched!");
        if(!desc.equals(blog.getDescription()))
            ActionGen.logFail("Blog desc is not matched!");
        if(!content.equals(blog.getContent()))
            ActionGen.logFail("Blog content is not matched!");
    }

    public static void verifyShopInfoOnBuyer(){
        ActionGen.navigateToURL(shopInfo.getShopUrl());
        ActionGen.click(ActionGen.getBy("Buyer_ShopInfo_Tab"));
        String name = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Name")).getText();
        String slogan = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Slogan")).getText();
        String address = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Address")).getText();
        String phone = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Phone")).getText();
        String email = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Email")).getText();
        String website = ActionGen.getElement(ActionGen.getBy("Buyer_ShopInfo_Website")).getText() + "/";
        ActionGen.assertTextEquals(shopInfo.getShopName(),name,"Shop name is not matched!");
        ActionGen.assertTextEquals("\"" + shopInfo.getSlogan() + "\"",slogan,"Slogan is not matched!");

        String fullAddress = shopInfo.getShopAddress()
                + ", "
                + shopInfo.getShopWard()
                + ", "
                + shopInfo.getShopDistrict()
                + ", "
                + shopInfo.getShopCity();
        ActionGen.assertTextEquals(fullAddress,address,"Shop address is not matched!");
        ActionGen.assertTextEquals(shopInfo.getPhone(),phone,"Shop phone is not matched!");
        ActionGen.assertTextEquals(shopInfo.getEmail(),email,"Shop email is not matched!");

        String temp_url = shopInfo.getShopUrl() + "/";
        ActionGen.assertTextEquals(temp_url,website,"Shop url is not matched!");
    }

    public static void verifyMap(String isMapDisplayed){
        By a = ActionGen.getBy("Map_VisualMap");
        if(isMapDisplayed.equals("YES")){
            WebElement map = ActionGen.getElement(a);
            if(map == null)
                ActionGen.logFail("Map is not displayed");
            ActionGen.click(a);
            ActionGen.verifyToastMessage("");
        }else{
            WebElement map = ActionGen.getElement(a);
            if(map != null)
                ActionGen.logFail("");
        }

    }

    public static void verifyShippingSupport(){
        goToCheckout();
        int m = -1;
        int n = -1;

        WebElement ele_supportFee = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Checkout_SupportFee"),1);
        if(ele_supportFee != null && ele_supportFee.isDisplayed())
            n = 1;
        if(m!=-1) {
            if(!shipping.getEffect01() && !shipping.getEffect02()) {
                if(n == 1) {
                    exception = "[FAILURE]: Find support fee element in checkout page in case the config has no effects";
                    assertTrue(false);
                }
            }

            else {
                if(n != 1) {
                    exception = "[FAILURE]: Cannot find support fee element in checkout page in case the config has effects";
                    assertTrue(false);
                }
            }

        }



        long totalOrderAmount = Long.valueOf(ActionGen.getElement(ActionGen.getBy("Buyer_Checkout_TotalAmount"))
                .getText().replace("đ","").replace(".",""));
        long shippingFee = Long.valueOf(ActionGen.getElement(ActionGen.getBy("Buyer_Checkout_ShippingFee"))
                .getText()
                .replace("đ","")
                .replace(".","")
                .replace("-",""));

        long supportFee = Long.valueOf(ActionGen.getElement(ActionGen.getBy("Buyer_Checkout_SupportFee"))
                .getText()
                .replace("đ","")
                .replace(".","")
                .replace("-",""));

        if(shipping.getEffect01()) {
            if (totalOrderAmount < Long.valueOf(shipping.getOrderAmount01())) {
                if (n == -1) {
                    exception = "[FAILURE]: Cannot find support fee element in checkout page in case current total amount is smaller than configed order amount 01";
                    assertTrue(false);
                }
            }

            if(!shipping.getEffect02()){
                if (shippingFee < Long.valueOf(shipping.getSupportFee01())) {
                    if(supportFee != shippingFee) {
                        exception = "[FAILURE]: Support fee is not equals to shipping fee in checkout page in case current shipping fee is smaller than configed support fee 01";
                        assertTrue(false);
                    }
                }

                else {
                    if(supportFee != Long.valueOf(shipping.getSupportFee01())) {
                        exception = "[FAILURE]: Support fee is not equals to configed support fee 01 in checkout page in case current shipping fee is larger than configed support fee 01";
                        assertTrue(false);
                    }

                }
            }else{
                if(totalOrderAmount < Long.valueOf(shipping.getOrderAmount02())){
                    if (shippingFee < Long.valueOf(shipping.getSupportFee01())) {
                        if(supportFee != shippingFee) {
                            exception = "[FAILURE]: Support fee is not equals to shipping fee in checkout page in case current shipping fee is smaller than configed support fee 01";
                            assertTrue(false);
                        }
                    }else {
                        if(supportFee != Long.valueOf(shipping.getSupportFee01())) {
                            exception = "[FAILURE]: Support fee is not equals to configed support fee 01 in checkout page in case current shipping fee is larger than configed support fee 01";
                            assertTrue(false);
                        }
                    }
                }else{
                    if (shippingFee < Long.valueOf(shipping.getSupportFee02())) {
                        if(supportFee != shippingFee) {
                            exception = "[FAILURE]: Support fee is not equals to shipping fee in checkout page in case current shipping fee is smaller than configed support fee 02";
                            assertTrue(false);
                        }
                    }
                    else {
                        if(supportFee != Long.valueOf(shipping.getSupportFee02())) {
                            exception = "[FAILURE]: Support fee is not equals to configed support fee 02 in checkout page in case current shipping fee is larger than configed support fee 02";
                            assertTrue(false);
                        }
                    }
                }
            }
        }else{
            if (totalOrderAmount < Long.valueOf(shipping.getOrderAmount02())) {
                if (n == -1) {
                    exception = "[FAILURE]: Cannot find support fee element in checkout page in case current total amount is smaller than configed order amount 02";
                    assertTrue(false);
                }
            }
            if (shippingFee < Long.valueOf(shipping.getSupportFee02())) {
                if(supportFee != shippingFee) {
                    exception = "[FAILURE]: Support fee is not equals to shipping fee in checkout page in case current shipping fee is smaller than configed support fee 02";
                    assertTrue(false);
                }
            }
            else {
                if(supportFee != Long.valueOf(shipping.getSupportFee02())) {
                    exception = "[FAILURE]: Support fee is not equals to configed support fee 02 in checkout page in case current shipping fee is larger than configed support fee 02";
                    assertTrue(false);
                }
            }
        }
    }

    public static void verifyOrderStatus(String value){
        searchOrder(order.getOrderNumber());
        String orderStatusName = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderList_OrderStatusName")).getText();
        AssertJUnit.assertTrue(orderStatusName.equals(value));
    }

    public static void verifyDeclarePopup(){
        searchOrder(order.getOrderNumber());
        ActionGen.click(ActionGen.getBy("SalesOrder_DeclarePrice"));
        WebElement popup = ActionGen.getElement(ActionGen.getBy("SalesOrder_DeclareValueFee"));
        System.out.println(popup.isDisplayed());
        ActionGen.assertElementIsDisplayed(popup,"Declare price is failed!");
    }

    public static void verifyOrder(String type){
        ActionGen.sleep(1000);
        List<WebElement> orders = ActionGen.getElements(By.xpath("//div[contains(@data-bind,'SalesOrderList')]/div"));
        switch(type){
            case "OrderNumber":
                String orderNumber = ActionGen.getElementText(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.assertTextEquals(orderNumber,"#" + order.getOrderNumber(),"There is a order number is not correct");
                break;
            case "OrderStatus":
                String orderStatus = ActionGen.getElementText(ActionGen.getBy("SalesOrder_OrderList_OrderStatus"));
                ActionGen.assertTextEquals(orderStatus,"Hủy","There is a order status is not correct");
                break;
        }
    }

    public static void verifySalesOrder(){
        searchOrder(order.getOrderNumber());
        ActionGen.click(By.xpath("//a[contains(@data-bind,'OrderNumber') and text()='#" + order.getOrderNumber() + "']"));
        ActionGen.verifyToastMessage("");
        WebElement element = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderDetail_OrderNumber"));
        String orderNumber = ActionGen.waitForEmptyValueChanged(element,10);
        ActionGen.assertTextEquals(orderNumber,"#" + order.getOrderNumber(),"Order number is not correct");
    }

    public static void verifyCarrier(){
        ActionGen.reloadPage();
        WebElement cptc = ActionGen.getElement(ActionGen.getBy("Carrier_dispatch_cptc"));
        WebElement cpbk = ActionGen.getElement(ActionGen.getBy("Carrier_dispatch_cptk"));
        WebElement cptn = ActionGen.getElement(ActionGen.getBy("Carrier_dispatch_cpn"));
        WebElement cpht = ActionGen.getElement(ActionGen.getBy("Carrier_dispatch_instant"));
        ActionGen.assertBooleanEquals(cptc.isSelected(),carrier.getCptc(),"CPTC is not matched");
        ActionGen.assertBooleanEquals(cpbk.isSelected(),carrier.getCpbk(),"CPBK is not matched");
        ActionGen.assertBooleanEquals(cptn.isSelected(),carrier.getCptn(),"CPTN is not matched");
        ActionGen.assertBooleanEquals(cpht.isSelected(),carrier.getCpht(),"CPHT is not matched");

    }


    /* Delete */

    public static void deleteProductBySKU(String sku) {
        switchOffProductBySKU(sku);
        ActionGen.clickByJS(ActionGen.getElement(ActionGen.getBy("Product_ProductList_DeleteIcon")));
        ActionGen.clickModalConfirm("sản phẩm");
    }

    public static void addMultiProducts(String num){
        for(int i=0; i<Integer.valueOf(num); i++){
            getAction("AddProduct", -3,"TC_002");
        }
    }

    public static void deleteAlbum(){

    }

    public static void deletePhotoFromAlbum(){

    }

    public static void deleteBlog(){
        //ActionGen.switchToOldWindow();
        ActionGen.navigateToURL("URL_Blog");
        WebElement element = ActionGen.getElement(By.xpath("//div[@class='title' and text()='" + blog.getName() + "']/following-sibling::div/div/input"));
        ActionGen.clickByJS(element);
        ActionGen.click(ActionGen.getBy("Blog_Delete"));
        ActionGen.clickModalConfirm("bài viết");
    }

    public static void deleteBanner(){
        ActionGen.reloadPage();
        ActionGen.navigateToURL("URL_Banner");
        List<WebElement> banners = ActionGen.getElements(ActionGen.getBy("Banner_Delete"));
        ActionGen.clickByJS(banners.get(banners.size()-1));
        ActionGen.clickModalConfirm("banner");
    }

    /* Set value */

    public static void chooseCategory(By selector, String category){
        if(System.getProperty("os.name").startsWith("Linux")) {
            ActionGen.scrollDownPage("100");
        }
        String[] cateList;
        if(category.contains(" -> "))
            cateList = category.split(" -> ");
        else
            cateList = category.split(", ");
        String catelv1 = cateList[0];
        String catelv2 = cateList[1];
        String catelv3 = cateList[2];
        ActionGen.clickByJS(ActionGen.getElement(selector));
        ActionGen.sleep(1000);
        ActionGen.clickByJS(ActionGen.getElement(By.xpath("//span[text()='"+ catelv1 + "']")));
        ActionGen.clickByJS(ActionGen.getElement(By.xpath("//span[text()='"+ catelv2 + "']")));
        ActionGen.clickByJS(ActionGen.getElement(By.xpath("//label[text()='"+ catelv3 + "']")));
        ActionGen.verifyToastMessage("");

        WebElement catePath = ActionGen.getElement(By.xpath("//*[@id='content-wrapper']/div/div[1]/div[3]/div[2]/div[2]/div/form/div[1]/div/div[2]/span"));
        if(catePath == null)
            assertTrue(false);
        else{
            if(!catePath.getText().toLowerCase().equals(catelv1.toLowerCase() + " > " + catelv2.toLowerCase() + " > " + catelv3.toLowerCase()))
                assertTrue(false);
        }
    }

    public static void setProductType(String type){
        ActionGen.click(By.xpath("//label[@for='ProductType_" + type + "']"));
        product.setProductType(Integer.valueOf(type));
        ActionGen.scrollDownPage("100");
    }

    public static void setProductColor(By selector, String colors){
        if(colors.equals(""))
            return;
        ActionGen.click(selector);
        String[] colorList = colors.split(", ");
        for(int i=1; i<=colorList.length; i++){
            ActionGen.click(By.xpath("//div[@class='item-color' and @title='"+ colorList[i-1] +"']"));
        }
        ActionGen.clickByJS(ActionGen.getElement(By.className("list-color-close")));
    }

    public static void setProductDescription(String xpath, String value){
        String locator = ActionGen.getElementAttribute(By.tagName("iframe"),"id");
        ActionGen.switchToNewFrame(locator);
        ActionGen.switchToActiveElement().sendKeys(value);
        ActionGen.switchToOldFrame();
    }

    public static void selectDate(By selector, String value){
        if(value.equals(""))
            return;
        ActionGen.click(selector);
        List<WebElement> dates = ActionGen.getElements(By.xpath("//li[text()='" + value + "']"));
        for(int i=0; i< dates.size(); i++)
            ActionGen.clickByJS(dates.get(i));
    }

    public static void inputVariants(){
        WebElement configVariant = ActionGen.getElementMayNull(By.xpath("//div[contains(@data-bind,'setConfigVariant(true)')]"),1);
        if(configVariant!=null)
            configVariant.click();
        WebElement attr_block = ActionGen.getElement(By.xpath("//div[@class='block-thuoc-tinh clearfix']"));
        WebElement size_name = ActionGen.getElementMayNull(By.xpath("//span[contains(@data-bind,'text: Name') and text()='Kích thước']"),1);
        if(size_name != null){
            WebElement size_attr = attr_block.findElement(By.xpath("//div[contains(@class,'multi')]/div/input"));
            size_attr.click();
            ActionGen.clickByJS(size_attr);
            List<WebElement> options = ActionGen.getElements(By.xpath("//div[contains(@class,'multi')]/div[@class='selectize-dropdown-content']/div"));
            for(int a = 0; a < options.size(); a++){
                if(!options.get(a).getText().equals("")){
                    options.get(a).click();
                    break;
                }
            }
            ActionGen.pressKeyOnElement(size_attr,"ESC");
        }


        WebElement color_name = ActionGen.getElementMayNull(By.xpath("//span[contains(@data-bind,'text: Name') and text()='Màu sắc']"),1);
        if(color_name != null){
            ActionGen.click(By.className("bg-color"));
            List<WebElement> colors = ActionGen.getElements(By.xpath("//div[contains(@data-bind,'SeletedColor')]"));
            for(int a = 0; a < colors.size(); a++){
                if(!colors.get(a).getText().equals("")){
                    if(!colors.get(a).getAttribute("class").contains("active"))
                        colors.get(a).click();
                    break;
                }
            }
            ActionGen.click(By.className("list-color-close"));
        }

        ActionGen.sendKeys(By.id("newAttributeName"),"ABCD");
        ActionGen.sendKeys(By.id("newAttributeValue"),"12345");
        ActionGen.click(By.id("newAttributeAddButton"));
        product.setConfig(true);
    }

    public static void inputAttributes(){
        WebElement attr_block = ActionGen.getElement(By.xpath("//div[contains(@data-bind,'ProductDetail().Attributes') and @class='attr_not_checkout']"));
        List<WebElement> attr_texts = attr_block.findElements(By.xpath(".//input[contains(@data-bind,'ProductDetail().CanEdit()')]"));
        for(int i = 0; i < attr_texts.size(); i++){
            attr_texts.get(i).sendKeys("Test");
        }
        List<WebElement> attr_multiselects = attr_block.findElements(By.xpath(".//select[contains(@data-bind,'selectizeMultiple')]/following-sibling::div/div/input"));
        for(int j=0; j<attr_multiselects.size(); j++){
            ActionGen.clickByJS(attr_multiselects.get(j));
            List<WebElement> options = attr_block.findElements(By.xpath(".//div[contains(@class,'multi')]/div[@class='selectize-dropdown-content']/div"));
            for(int a = 0; a < options.size(); a++){
                if(!options.get(a).getText().equals("")){
                    options.get(a).click();
                    break;
                }
            }
            ActionGen.pressKeyOnElement(attr_multiselects.get(j),"ESC");
        }

        List<WebElement> attr_selects = attr_block.findElements(By.xpath(".//select[contains(@data-bind,'SelectedAtt')]/following-sibling::div/div/input"));
        List<WebElement> attr_items = attr_block.findElements(By.xpath(".//select[contains(@data-bind,'SelectedAtt')]/following-sibling::div/div/div[@class='item']"));
        for(int k = 0; k < attr_selects.size(); k++){
            if(attr_items.size() > 0){
                attr_items.get(k).click();
            }else{
                attr_selects.get(k).click();
            }
            List<WebElement> options = attr_block.findElements(By.xpath(".//div[contains(@class,'single')]/div[@class='selectize-dropdown-content']/div"));
            for(int a = 0; a < options.size(); a++){
                if(!options.get(a).getText().equals("")){
                    options.get(a).click();
                    break;
                }
            }
        }
    }

    public static void setVoucherDate(By selector, String value){
        if(value.equals(""))
            return;
        if(value.equals("TOMORROW"))
            value = FormatUtils.getCurrentDate();
        String[] expParts = value.split("/");
        String exp_day = expParts[0];
        if(exp_day.charAt(0)=='0')
            exp_day = String.valueOf(exp_day.charAt(1));
        int exp_month = Integer.valueOf(expParts[1]);
        int exp_year = Integer.valueOf(expParts[2]);
        ActionGen.click(selector);
        int cur_month;
        int cur_year;

        do{
            String monthYear = ActionGen.getElementText(By.xpath("//div[@class='daterangepicker dropdown-menu single opensright show-calendar' and contains(@style,'display: block;')]/div[contains(@style,'display: block;')]/div/table/thead/tr/th[@class='month']"));
            String[] curParts = monthYear.split(" ");
            cur_month = Integer.valueOf(curParts[1]);
            cur_year = Integer.valueOf(curParts[2]);
            if(cur_month == exp_month && cur_year == exp_year)
                break;
            ActionGen.getElement(By.xpath("//div[@class='daterangepicker dropdown-menu single opensright show-calendar' and contains(@style,'display: block;')]/div[contains(@style,'display: block;')]/div/table/thead/tr/th[@class='next available']")).click();
        }while(cur_month < exp_month || cur_year < exp_year);

        List<WebElement> rows = ActionGen.getElement(By.xpath("//div[@class='daterangepicker dropdown-menu single opensright show-calendar' and contains(@style,'display: block;')]/div[contains(@style,'display: block;')]/div/table/tbody")).findElements(By.tagName("tr"));
        myFlag:
        for(int i=0; i<rows.size(); i++){
            List<WebElement> days = rows.get(i).findElements(By.xpath("//td[@class='available']"));
            for(int j=0; j<days.size(); j++){
                if(days.get(j).getText().equals(exp_day)){
                    days.get(j).click();
                    break myFlag;
                }
            }
        }
    }

    public static void setOneColor(By selector, String color){
        if(color.equals(""))
            return;
        ActionGen.click(selector);
        List<WebElement> colors = ActionGen.getElements(By.xpath("//div[contains(@class,'colorPicker-swatch')]"));
        ActionGen.clickByJS(colors.get(Integer.valueOf(color)));
    }

    /* Upload */

    public static void uploadOnePhoto(By selector, String image){
        if(image.equals(""))
            return;
        //clickByJS(getElement(selector));
        //click(selector);
        ActionGen.scrollToThenClick(selector);
        ActionGen.uploadByRobot(image);
        ActionGen.verifyToastMessage("");
    }

    public static void uploadImageModal(By selector, String images){
        if(images.equals(""))
            return;
        //click(By.xpath("//*[@id='content-wrapper']/div/div[1]/div[3]/div[1]/div[1]/div[3]/button[1]"));
        ActionGen.clickByJS(ActionGen.getElement(selector));
        ActionGen.sleep(1000);
        String[] imageList = images.split(", ");
        for(int i=1; i<=imageList.length; i++){
            ActionGen.click(By.xpath("//a[@id='uploader_browse' or @id='CertificateFileUploader_browse']"));
            ActionGen.uploadByRobot(imageList[i-1]);
            ActionGen.verifyToastMessage("");
            WebElement uploadStatus = ActionGen.getElement(By.xpath("//span[@class='plupload_upload_status' and text()='Đã xong " + i + "/" + i + "']"));
            ActionGen.assertElementIsDisplayed(uploadStatus,"");
            ActionGen.verifyToastMessage("");
        }
        ActionGen.click(By.xpath("//div[@class='modal fade in']/div/div/div[3]/button"));
        ActionGen.verifyToastMessage("");
    }

    public static void upImgFromGallery(By selector, String imgCate){
        if(imgCate.equals(""))
            return;
        ActionGen.sleep(2000);
        ActionGen.clickByJS(ActionGen.getElement(selector));
        ActionGen.sleep(1000);
        if(imgCate.contains("0")){
            List<WebElement> galleryList = ActionGen.getElements(By.className("img_thumb"));
            String[] imageIndex = imgCate.split(", ");
            for(int i=1; i<=imageIndex.length; i++){
                System.out.println("Before Click image: " + i);
                ActionGen.clickByJS(galleryList.get(Integer.valueOf(imageIndex[i-1])));
            }
        }else{
            ActionGen.click(By.xpath("//h3[text()='" + imgCate + "']"));
            ActionGen.sleep(2000);
            List<WebElement> galleryList = ActionGen.getElements(By.className("img_thumb"));
            ActionGen.clickByJS(galleryList.get(0));
        }
        ActionGen.clickByJS(ActionGen.getElement(By.xpath("//button[contains(@data-bind,'SelectComplete')]")));
        ActionGen.verifyToastMessage("");
    }

    /* Other */

    public static void copyProduct(String sku){
        searchProductBySku(sku);
        ActionGen.click(ActionGen.getBy("Product_ProductList_ProductName"));
        ActionGen.click(ActionGen.getBy("Product_CopyProduct"));
    }

    public static void cloneProduct(By selector, String keyword){
        if(keyword.equals(""))
            return;
        ActionGen.click(selector);
        ActionGen.sendKeys(By.id("InputKeywordSearchProductForCopy"), keyword);
        ActionGen.click(By.xpath("//button[contains(@data-bind,'ClickSearchProductForCopy')]"));
        ActionGen.click(By.xpath("//button[contains(@data-bind,'CopyProductAtttribute')]"));
    }



    public static void sendMessage(String message){

    }

    public static void sendEmoticon(){
        ActionGen.click(By.xpath("//button[contains(@class,'emojiBtnContainer')]"));
        ActionGen.click(By.xpath("//button/span[contains(@aria-label,'face savoring food')]"));
        ActionGen.pressKey(By.xpath("//textarea[contains(@class,'inputBox')]"),"ENTER");
    }

    public static void goToCheckout(){
        ActionGen.sleep(1500);
        dismissPopup();
        ActionGen.click(ActionGen.getBy("Buyer_BuyNow"));
        ActionGen.sleep(1000);
        loginOnPopup();
        ActionGen.waitForPageLoad();
    }

    public static void addToCart(String url){
        if(url.equals(""))
            return;
        ActionGen.navigateToURL(url);
        ActionGen.sleep(1500);
        dismissPopup();
        ActionGen.click(ActionGen.getBy("Buyer_AddToCart"));
    }

    public static void likeProduct(){
        ActionGen.navigateToURL(product.getBuyerUrl());
        dismissPopup();
        ActionGen.clickByJS(ActionGen.getElement(ActionGen.getBy("Buyer_Like")));
        loginOnPopup();
        ActionGen.sleep(2000);
        WebElement element = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Like"),1);
        if(element!=null && element.isDisplayed())
            ActionGen.clickByJS(element);
        ActionGen.waitForPageLoad();
    }

    public static void verifyStock(){
        ActionGen.sleep(1500);
        ActionGen.clickByJS(ActionGen.getElement(ActionGen.getBy("Product_ProductList_EyeIcon")));
        ActionGen.switchToNewWindow();
        verifyInStockOnBuyer();
        ActionGen.switchToOldWindow();
    }

    public static void verifyInStockOnBuyer(){
        ActionGen.getElement(By.xpath("//button[contains(@class,'btn-disabled') or contains(@class,'buynow')]"));
        WebElement buttonOutstock = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_btnOutStock"),1);
        if(!product.getInStock())
            ActionGen.assertElementIsDisplayed(buttonOutstock,"Out of stock button is not displayed");
        else
            ActionGen.assertElementIsNotDisplayed(buttonOutstock,"Out of stock is displayed");
    }

    public static void verifyXSS(String value){
        ActionGen.navigateToURL(product.getBuyerUrl());
        ActionGen.verifySecurity(value);
        ActionGen.navigateToURL(product.getBuyerUrl().replace("https://www.sendo.vn/","https://www.sendo.vn/shop/shopbaby47/"));
        ActionGen.verifySecurity(value);
    }

    public static void verifyInstallmentOnBuyer(){
        ActionGen.navigateToURL("https://www.sendo.vn/spqc-pheu-dung-trung-installment-17379905.html");
        WebElement element = ActionGen.getElement(ActionGen.getBy("Buyer_InstallmentIcon"));
        if(element == null)
            ActionGen.logFail("Installment is failed");
    }

    public static void createOrder(){
        goToCheckout();
        ActionGen.setCheckbox(By.xpath("//input[contains(@value,'ecom_shipping_dispatch_cptc')]"),"YES");
        ActionGen.sleep(1500);
        ActionGen.click(By.xpath("//span[text()='Đặt hàng']"));
        ActionGen.waitForPageLoad();
        MyLogger.info(ActionGen.getCurrentURL());
        //String orderNumber = ActionGen.getElementText(By.xpath("//div[contains(@class,'otherInfo')]/div/p/b"));
        ActionGen.waitForAjaxDisappear();
        ActionGen.assertTextContains(ActionGen.getCurrentURL(),".sendo.vn/success?order=","Cannot get order number from buyer");
        String orderNumber = ActionGen.getCurrentURL().replace("https://checkout.sendo.vn/success?order=","");
        order.setOrderNumber(orderNumber);
    }

    public static void proceedToCheckout(String packageName){
        ActionGen.setCheckbox(By.xpath("//input[contains(@value,'" + packageName + "')]"),"YES");
        ActionGen.sleep(1500);
        ActionGen.click(By.xpath("//span[text()='Đặt hàng']"));
        ActionGen.waitForPageLoad();
        MyLogger.info(ActionGen.getCurrentURL());
        //String orderNumber = ActionGen.getElementText(By.xpath("//div[contains(@class,'otherInfo')]/div/p/b"));
        ActionGen.waitForAjaxDisappear();
        ActionGen.assertBooleanEquals(ActionGen.getCurrentURL().contains("success?order"),true,"Failed to get order number from buyer");
        String orderNumber = ActionGen.getCurrentURL()
                .replace("https://checkout.sendo.vn/success?order=","")
                .replace("https://checkout-stg.sendo.vn/success?order=","")
                .replace("https://checkout-pilot.sendo.vn/success?order=","");

        order.setOrderNumber(orderNumber);
    }

    public static void proceedToCheckoutFromCart(String object, String value){
        if(value.equals(""))
            return;
        ActionGen.click(ActionGen.getBy("Buyer_CartIcon"));
        ActionGen.click(ActionGen.getBy("Buyer_CartCheckout"));
        ActionGen.sleep(1000);
        loginOnPopup();
        ActionGen.waitForPageLoad();
        proceedToCheckout(value);
        ActionGen.assertBooleanEquals(ActionGen.getCurrentURL().contains("success?order"),true,"Failed to get order number from buyer");
        String orderNumber = ActionGen.getCurrentURL()
                .replace("https://checkout.sendo.vn/success?order=","")
                .replace("https://checkout-stg.sendo.vn/success?order=","")
                .replace("https://checkout-pilot.sendo.vn/success?order=","");

        switch(object){
            case "orderNumber":
                order.setOrderNumber(orderNumber);
                break;
            case "orderNumber1":
                order.setOrderNumber(orderNumber);
                order.setOrderNumber1(orderNumber);
                break;
            case "orderNumber2":
                order.setOrderNumber2(orderNumber);
                break;
        }
    }

    public static void actionOrder(String type){
        ActionGen.navigateToURL("URL_SalesOrderNew");
        switch(type){
            case "TC_001":
                verifyOrderNotify(order.getOrderNumber(), false);
                searchOrder(order.getOrderNumber());
                verifyOrderInList(order.getOrderNumber(), "Mới");
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                verifyOrderInDetail(order.getOrderNumber());
                verifyDeclarePrice(false);
                cancelOrder();
                searchOrder(order.getOrderNumber());
                verifyOrderInList(order.getOrderNumber(), "Hủy");
                ActionGen.click(ActionGen.getBy("SalesOrder_ExportExcel"));
                ActionGen.verifyToastMessage("");
                break;
            case "TC_002":
                searchOrder(order.getOrderNumber());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                verifyDeclarePrice(true);
                break;
            case "TC_003":
                verifyOrderNotify(order.getOrderNumber(), true);
                searchOrder(order.getOrderNumber());
                ActionGen.getElement(ActionGen.getBy("SalesOrder_TagInstant"));
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                String confirmTimeText = ActionGen.getElement(ActionGen.getBy("SalesOrder_ConfirmTime")).getText();
                ActionGen.assertBooleanEquals(confirmTimeText.contains("Bạn còn") || confirmTimeText.contains("Phải xác nhận đơn hàng trước 08:45"), true, "Confirm time is not correct!");
                break;
            case "TC_004":
                verifyOrderNotify(order.getOrderNumber(), true);
                searchOrder(order.getOrderNumber());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                break;
            case "TC_005":
                searchOrder(order.getOrderNumber());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                By byKiemHang = ActionGen.getBy("SalesOrder_OrderDetail_KiemHang");
                ActionGen.click(byKiemHang);
                confirmOrder(1);
                ActionGen.assertBooleanEquals(ActionGen.getElement(byKiemHang).isSelected(), true, "Kiem hang is not selected!");
                break;
            case "TC_007":
                searchOrder(order.getOrderNumber());
                verifyOrderInList(order.getOrderNumber(), "Mới");
            case "TC_008":
                searchOrder(order.getOrderNumber());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                delayOrder();
                searchOrder(order.getOrderNumber());
                verifyOrderInList(order.getOrderNumber(), "Đang hoãn");
                break;
            case "TC_010":
                searchOrder(order.getOrderNumber1());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                confirmOrder(2);
                mergeOrder(order.getOrderNumber1(), order.getOrderNumber2());
                break;
            case "TC_012":
                searchOrder(order.getOrderNumber());
                ActionGen.click(ActionGen.getBy("SalesOrder_OrderList_OrderNumber"));
                ActionGen.waitForPageLoad();
                ActionGen.waitForAjaxDisappear();
                splitOrder();
                searchOrder(order.getOrderNumber());
                verifyOrderInList(order.getOrderNumber(), "Chờ lấy hàng");
//            1. Tạo đơn hàng thường --> Kiểm tra notify --> Search đơn --> Kiểm tra thông tin đơn --> Kiểm tra khai giá <3tr --> Hủy đơn hàng
//            2. Tạo đơn hàng Sorting --> Kiểm tra khai giá >=3tr
//            3. Tạo đơn hàng hỏa tốc --> Kiểm tra thời gian xác nhận
//            4. Tạo đơn hàng travel
//            5. Tạo đơn hàng TVC
//            6. Tạo đơn hàng --> Hoãn đơn
//            7. Tạo đơn hàng --> Xác nhận đơn --> Check shop qua bưu cục
//            8. Tạo đơn hàng Senmall --> Xác nhận đơn --> In đơn truck
//            8. Tạo 2 đơn hàng --> Gộp 2 đơn
//            9. Tạo đơn hàng 2 sp --> tách 2 đơn
            default:
        }
    }

    public static void verifyOrderInList(String orderNumber, String status){
        String actualOrderNumber = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderList_OrderNumber")).getText();
        ActionGen.assertTextEquals(actualOrderNumber, "#" + orderNumber, "OrderNumber is not correct");
        String actualStatus = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderList_OrderStatus")).getText();
        ActionGen.assertTextEquals(actualStatus, status, "OrderStatus is not correct");
    }

    public static void verifyOrderInDetail(String orderNumber){
        ActionGen.verifyToastMessage("");
        String actualOrderNumber = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderDetail_OrderNumber")).getText();
        ActionGen.assertTextEquals(actualOrderNumber, "#" + orderNumber, "Order is not correct!");
        String actualAddress = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderDetail_StorePickingAddress")).getText();
        ActionGen.assertTextContains(actualAddress, "Hồ Chí Minh", "Address is not correct");
    }

    public static void verifyDeclarePrice(boolean value){
        String text = ActionGen.getElement(ActionGen.getBy("SalesOrder_OrderDetail_DeclarePrice")).getText();
        if(value){
            ActionGen.assertBooleanEquals(!text.contains("Miễn phí"), true, "Declare price does not have Mien phi");
            ActionGen.click(ActionGen.getBy("SalesOrder_OrderDetail_DeclarePrice"));
            ActionGen.sleep(2000);
            String orderNumber = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'DeclareValueOrderNumber')]")).getText();
            ActionGen.assertTextContains(orderNumber, order.getOrderNumber(), "Order number is not correct");
            String declareValueFee = ActionGen.getElement(By.xpath("//span[contains(@data-bind,'DeclareValueFee')]")).getText();
            ActionGen.assertTextContains(declareValueFee, "đ","Fee is not correct");
        }else{
            ActionGen.assertTextContains(text,"Miễn phí", "Declare price does not have Mien phi");
        }
    }

    public static void cancelOrder() {
        ActionGen.click(ActionGen.getBy("SalesOrder_CanCancel"));
        ActionGen.sleep(3000);
        ActionGen.click(By.xpath("//div[@id='cancelorderwithattr']//span[text()='Hết hàng']"));
        ActionGen.sleep(3000);
        ActionGen.click(By.xpath("//div[@id='cancelorderwithattr']//button[contains(@data-bind,'CancelOrder')]"));
        ActionGen.verifyToastMessage("Hủy đơn hàng thành công.");
    }

    public static void confirmOrder(int num){
        ActionGen.click(ActionGen.getBy("SalesOrder_CanProcess"));
        WebElement element  = ActionGen.getElementMayNull(By.id("sogrouplist"),1);
        if(element!=null){
            if(num==1)
                mergeOrder(order.getOrderNumber());
            if(num==2)
                mergeOrder(order.getOrderNumber1(), order.getOrderNumber2());
        }
        ActionGen.verifyToastMessage("ABCD");
    }

    public static void mergeOrder(String orderNumber1, String orderNumber2){
        List<WebElement> orderNumbers = ActionGen.getElements(By.xpath("//span[@class='ordercode']"));
        List<WebElement> checkboxes = ActionGen.getElements(By.xpath("//input[contains(@data-bind,'IsSelect')]"));
        for(int i=0; i<orderNumbers.size(); i++){
            String orderNumber = orderNumbers.get(i).getText();
            if(orderNumber.equals(orderNumber1) || orderNumber.equals(orderNumber2))
                continue;
            ActionGen.clickByJS(checkboxes.get(i));
        }
        ActionGen.click(By.xpath("//button[contains(@data-bind,'GroupOrderDTClick')]"));
        ActionGen.verifyToastMessage("Đã gộp thành công 2 đơn hàng");
    }

    public static void mergeOrder(String orderNumber){
        ActionGen.click(ActionGen.getBy("SalesOrder_CanProcess"));
        List<WebElement> orderNumbers = ActionGen.getElements(By.xpath("//span[@class='ordercode']"));
        List<WebElement> checkboxes = ActionGen.getElements(By.xpath("//input[contains(@data-bind,'IsSelect')]"));
        for(int i=0; i<orderNumbers.size(); i++){
            String actualOrderNumber = orderNumbers.get(i).getText();
            if(actualOrderNumber.equals(orderNumber))
                continue;
            ActionGen.clickByJS(checkboxes.get(i));
        }
        ActionGen.click(By.xpath("//button[contains(@data-bind,'GroupOrderDTClick')]"));
        ActionGen.verifyToastMessage("Đã gộp thành công 2 đơn hàng");
    }

    public static void delayOrder(){
        ActionGen.click(ActionGen.getBy("SalesOrder_CanDelay"));
        ActionGen.click(By.id("date_delay"));
        ActionGen.click(By.xpath("//div[contains(@class,'daterangepicker') and contains(@style,'display: block')]//td[contains(@class,'active')]/following-sibling::td"));
        ActionGen.sendKeys(By.xpath("//textarea[contains(@data-bind,'DelayReason')]"),"Đơn hàng hoãn do QC Sendo test. Vui lòng không xử lý đơn hàng này!");
        ActionGen.click(By.xpath("//button[contains(@data-bind,'DelayOrder')]"));
        ActionGen.verifyToastMessage("Hoãn đơn hàng thành công.");
    }

    public static void splitOrder(){
        ActionGen.click(By.xpath("//label[text()='Còn hàng']"));
        //ActionGen.click(ActionGen.getBy("SalesOrder_CanSplit"));
        ActionGen.verifyToastMessage("Tách đơn hàng thành công.");
    }

    public static void verifyOnGmail(){
        String gmailUrl = "https://gmail.com";
        String username = "tester01gm@gmail.com";
        String password = "12345678Ac";

        ActionGen.navigateToURL(gmailUrl);
        ActionGen.sendKeys(By.id("Email"),username);
        ActionGen.click(By.id("next"));
        ActionGen.sleep(1000);
        ActionGen.sendKeys(By.id("Passwd"),password);
        ActionGen.click(By.id("signIn"));

        ActionGen.click(By.xpath("//*[text()='Xác nhận email khôi phục của bạn']"));

        ActionGen.sendKeys(By.name("email"),"tanhopham1990@gmail.com");
        ActionGen.click(By.id("submit"));

        System.out.println(ActionGen.getPageSource());

        By mail = By.xpath("//div[@role='link']/div/div/span/span[contains(text(),'Thông báo đặt hàng thành công')]");
        ActionGen.clickByJS(ActionGen.getElement(mail));

        WebElement element = ActionGen.getElement(By.xpath("//b[text()='Đơn hàng của bạn #']/following-sibling::a"));
        if(!element.getText().equals(order.getOrderNumber()))
            ActionGen.logFail("Order is failed");
        System.out.println("ORDER PASSED");
    }

    public static void dismissPopup(){
        WebElement popup = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_ClosePopup"),1);
        while(popup!=null && popup.isDisplayed()) {
            popup.click();
            popup = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_ClosePopup"),1);
        }
        ActionGen.sleep(10000);
        WebElement popup2 = ActionGen.getElementMayNull(By.xpath("//button[contains(@class,'closeBtn')]"), 1);
        System.out.println(popup2);
        while(popup2!=null && popup2.isDisplayed()) {
            popup2.click();
            popup2 = ActionGen.getElementMayNull(By.xpath("//button[contains(@class,'closeBtn')]"),1);
        }
    }

    public static void dismissPopupOnProductList(){
        WebElement closePopup = ActionGen.getElementMayNull(ActionGen.getBy("Product_ProductList_Popup"),5);
        if(closePopup!=null) {
            ActionGen.clickByJS(closePopup);
        }
    }

    public static void loginOnPopup(){
        MyLogger.info("Login into buyer...");
        WebElement loginForm = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Login_Popup"),5);
        if(loginForm != null && loginForm.isDisplayed()){
            ActionGen.click(ActionGen.getBy("Buyer_Login_Popup"));
            ActionGen.sendKeys(ActionGen.getBy("Buyer_Login_Email"), MyConstants.BUYER_USERNAME);
            ActionGen.sendKeys(ActionGen.getBy("Buyer_Login_Password"),MyConstants.BUYER_PASSWORD);
            ActionGen.click(ActionGen.getBy("Buyer_Login_btnLogin"));
            ActionGen.waitForPageLoad();
        }
    }

    public static void clearPromotion(By selector){
        ActionGen.clickByJS(ActionGen.getElement(selector));
        ActionGen.clickModalConfirm("");
        product.setPromotion(false);
    }

    public static void removeFromCart(){
        ActionGen.navigateToURL("https://www.sendo.vn/gio-hang");
        List<WebElement> list_shops = ActionGen.getElements(ActionGen.getBy("Buyer_Cart_ShopInfo"));
        System.out.println("List shops: " + list_shops.size());
        for(int i=0; i<list_shops.size(); i++){
            String shopName = list_shops.get(i).findElement(By.tagName("strong")).getText();
            System.out.println("Shop name: " + shopName);
            if(shopName.equals("shopbaby47")){
                WebElement btnDelete = list_shops.get(i).findElement(By.tagName("button"));
                btnDelete.click();
                ActionGen.click(ActionGen.getBy("Buyer_Cart_btnOk"));
                return;
            }

        }
    }

    public static void setDateRange(String value){
        if(value.equals("Cũ hơn")) {
            ActionGen.selectIfOptionTextEquals(By.xpath("//select[contains(@data-bind,'setDateRange')]"), "Cũ hơn");
            ActionGen.click(By.id("inputDate"));
            ActionGen.sendKeys(By.xpath("//div[contains(@class,'daterangepicker') and contains(@style,'display: block')]//input[@name='daterangepicker_start']"), "01012019");
            ActionGen.click(By.xpath("//div[contains(@class,'daterangepicker') and contains(@style,'display: block')]//button[contains(@class,'applyBtn')]"));
            ActionGen.click(By.xpath("//li[@class='orderTotal']/a"));
        }else{
            ActionGen.click(By.id("inputDate"));
            ActionGen.sendKeys(By.xpath("//div[contains(@class,'daterangepicker') and contains(@style,'display: block')]//input[@name='daterangepicker_start']"), "01012019");
            ActionGen.click(By.xpath("//div[contains(@class,'daterangepicker') and contains(@style,'display: block')]//button[contains(@class,'applyBtn')]"));
            ActionGen.click(By.xpath("//li[@class='orderTotal']/a"));
        }
    }

    public static void setProductDateRange(By selector, String value){
        if(value.equals(""))
            return;
        String[] dates = value.split(" - ");
        String fromDate = dates[0];
        String toDate = dates[1];

        ActionGen.click(selector);
        ActionGen.sendKeys(ActionGen.getBy("General_Calendar_fromDate"), fromDate);
        ActionGen.sendKeys(ActionGen.getBy("General_Calendar_ToDate"), toDate);
        ActionGen.click(ActionGen.getBy("General_Calendar_btnApply"));

    }

    public static void verifyFactShopKpi(String kpi){
        switch(kpi){
            case "PercentCompleted":
                String percentComplete1 = ActionGen.waitForEmptyValueChanged(ActionGen.getElement(ActionGen.getBy("Dashboard_PercentComplete1")),5);
                String percentComplete2 = ActionGen.getElement(ActionGen.getBy("Dashboard_PercentComplete2")).getText();
                ActionGen.assertTextNotEquals(percentComplete1,"0%","Percent complete is not correct");
                ActionGen.assertTextEquals(percentComplete1,percentComplete2,"Percent completes are not matched!");
                break;
            case "PercentOrderClaim":
                String percentOrderClaim1 = ActionGen.waitForEmptyValueChanged(ActionGen.getElement(ActionGen.getBy("Dashboard_PercentOrderClaim1")),5);
                String percentOrderClaim2 = ActionGen.getElement(ActionGen.getBy("Dashboard_PercentOrderClaim2")).getText().replace(",",".");
                ActionGen.assertTextNotEquals(percentOrderClaim1,"0%","Percent order claim is not correct");
                ActionGen.assertTextEquals(percentOrderClaim1,percentOrderClaim2,"Percent order claims are not matched!");
                break;
        }
    }

    public static void setCarrier(String locator, String value){
        if(locator.contains("checkbox")){
            By packageSelector = By.id(locator);
            By lblSelector = By.xpath("//label[@for='" + locator + "']");
            WebElement packageElement = ActionGen.getElement(packageSelector);
            WebElement lblElement = ActionGen.getElement(lblSelector);
            if(value.equals("YES")){
                if(!packageElement.isSelected() && lblElement!=null)
                    ActionGen.clickByJS(lblElement);
            }else{
                if(packageElement.isSelected() && lblElement!=null)
                    ActionGen.clickByJS(lblElement);
            }
        }else{
            By carrierSelector = By.xpath("//input[@value='" + locator + "']");
            By lgSelector = By.id(locator);
            WebElement carrierElement = ActionGen.getElementMayNull(carrierSelector,1);
            WebElement lgElement = ActionGen.getElementMayNull(lgSelector,1);
            if(value.equals("YES")){
                if(carrierElement!=null && !carrierElement.isSelected() && lgElement!=null)
                    ActionGen.clickByJS(lgElement);
            }else{
                if(carrierElement!=null && carrierElement.isSelected() && lgElement!=null)
                    ActionGen.clickByJS(lgElement);
            }
        }

        By a = By.xpath("//div[@class='modal fade in']//button[contains(@class,'close_uncheck_config_package_cpn')]");
        WebElement popup = ActionGen.getElementMayNull(a,1);
        if(popup != null)
            ActionGen.click(a);
        By b = By.xpath("//div[@class='modal fade in']//a[@id='btn_config_package_instant_product' or @id='btn_config_package_cpn_product']");
        WebElement btnAllProduct = ActionGen.getElementMayNull(b,1);
        if(btnAllProduct != null)
            ActionGen.click(b);
    }

    public static void verifyTravel(){
        searchProductBySku(product.getProductSku());
        verifyProductFullInfoInSeller();
        String productStatusText = ActionGen.getElementText(By.xpath("//span[contains(@data-bind,'ProductStatusName')]"));
        ActionGen.assertTextEquals(productStatusText,"Chờ duyệt","Product status is not matched");
        switch (product.getProductType()){
            case 2:
                WebElement voucher = ActionGen.getElement(By.className("voucher_ic"));
                ActionGen.assertTextEquals(voucher.getText(),"Vé giấy", "Icon is not matched");
                break;
            case 3:
                WebElement evoucher = ActionGen.getElement(By.className("evoucher_ic"));
                ActionGen.assertTextEquals(evoucher.getText(),"Vé điện tử", "Icon is not matched");
                break;
        }
    }

    public static void verifySelfShippingOnSeller(String regionName){
        String fromRegionName = "Hồ Chí Minh";
        String toRegionName = regionName;
        List<WebElement> fromRegionNames = ActionGen.getElements(ActionGen.getBy("SelfShipping_FromRegionName"));
        List<WebElement> toRegionNames = ActionGen.getElements(ActionGen.getBy("SelfShipping_ToRegionName"));
        for(int i=0; i<fromRegionNames.size(); i++){
            ActionGen.assertTextEquals(fromRegionNames.get(i).getText(),fromRegionName, "FromRegionName is not found!");
        }
        if(regionName.contains("rm_")){
            toRegionName = regionName.substring(3);
            boolean flag = true;
            for(int i=0; i<toRegionNames.size(); i++){
                if(toRegionNames.get(i).getText().equals(toRegionName))
                    flag = false;
            }
            ActionGen.assertBooleanEquals(flag,true,"ToRegionName is found!");
        }else{
            boolean flag = false;
            for(int i=0; i<toRegionNames.size(); i++){
                if(toRegionNames.get(i).getText().equals(toRegionName))
                    flag = true;
            }
            ActionGen.assertBooleanEquals(flag,true,"ToRegionName is not found!");
        }
    }

    public static void verifyInstantOnListing(String value){
        ActionGen.navigateToURL("URL_Product");
        searchProductBySku("skuConfig");
        WebElement icon = ActionGen.getElementMayNull(ActionGen.getBy("Product_InstantIcon"),1);
        if(value.equals("YES")){
            ActionGen.assertElementIsDisplayed(icon,"Instant icon is not visible!");
            MyLogger.info("--Icon hoa toc is visible. PASSED!");
        }else{
            ActionGen.assertElementIsNotDisplayed(icon,"Instant icon is still visible!");
            MyLogger.info("--Icon hoa toc is not visible. PASSED!");
        }
    }

    public static void verifyPackageOnCheckout(String packName){
        ActionGen.navigateToURL("https://www.sendo.vn/spqc-ao-khoac-nam-sku-17380496.html");
        goToCheckout();
        WebElement inDayPack = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Checkout_inDayPack"),3);
        WebElement instantPack = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Checkout_instantPack"),3);
        WebElement selfDeliveryPack = ActionGen.getElementMayNull(ActionGen.getBy("Buyer_Checkout_selfDeliveryPack"),3);

        switch(packName){
            case "ecom_shipping_dispatch_cpn":
                ActionGen.assertElementIsDisplayed(inDayPack, "Chuyen phat trong ngay is not visible");
                ActionGen.assertElementIsNotDisplayed(instantPack, "Chuyen phat hoa toc is still visible");
                ActionGen.assertElementIsNotDisplayed(selfDeliveryPack, "Shop TVC is still visible");
                MyLogger.info("--CPTN is visible. PASSED!");
                break;
            case "ecom_shipping_dispatch_instant":
                ActionGen.assertElementIsDisplayed(instantPack, "Chuyen phat hoa toc is not visible");
                ActionGen.assertElementIsNotDisplayed(inDayPack, "Chuyen phat trong ngay is still visible");
                ActionGen.assertElementIsNotDisplayed(selfDeliveryPack, "Shop TVC is still visible");
                MyLogger.info("--CPHT is visible. PASSED!");
                break;
            case "ecom_shipping_shop_tvc":
                ActionGen.assertElementIsDisplayed(selfDeliveryPack, "Shop TVC is not visible");
                ActionGen.assertElementIsNotDisplayed(inDayPack, "Chuyen phat trong ngay is still visible");
                ActionGen.assertElementIsNotDisplayed(instantPack, "Chuyen phat hoa toc is still visible");
                MyLogger.info("--TVC is visible. PASSED!");
                break;
        }
    }

    public static void verifyOrderNotify(String orderNumber, boolean instant){
        ActionGen.navigateToURL("https://ban.sendo.vn");
        ActionGen.mouseHover(ActionGen.getElement(By.id("notificationLink")));
        ActionGen.click(By.id("showall"));
        ActionGen.waitForPageLoad();
        List<WebElement> notifies = ActionGen.getElements(By.xpath("//div[contains(@data-bind,'NotifyText')]"));
        WebElement notify = null;
        boolean flag = false;
        for(int i=0; i<notifies.size(); i++){
            notify = notifies.get(i);
            String expectedNotify;
            if(instant)
                expectedNotify = "Đơn hàng " + orderNumber + " (gói dịch vụ giao hàng 3h) đang chờ xác nhận.";
            else
                expectedNotify = "Bạn có đơn hàng mới " + orderNumber + ", vào chi tiết đơn hàng để xử lý";
            System.out.println(notify.getText());
            System.out.println(expectedNotify);
            if(notify.getText().contains(expectedNotify)){
                flag = true;
                break;
            }
        }
        ActionGen.assertBooleanEquals(flag,true,"Cannot find the notify of salesorder " + order.getOrderNumber());
        ActionGen.clickByJS(notify.findElement(By.tagName("a")));
        ActionGen.assertTextContains(ActionGen.getCurrentURL(),MyConstants.getBaseSellerUrl() + "/shop#salesorder/detail/","The link is not activated!");
    }

    public static void sendOtp(){
        JSONObject obj = new JSONObject();
        obj.put("phone",register.getPhoneNumber());
        obj.put("type","register");
        obj.put("reCaptchaResponse","ABC");

        String url = MyConstants.getSellerApi() + "/api/web/SendoId/sendotp";
        APIController.sendApiRequest("POST",url,obj.toString(),"");
    }

    public static String getOtp(String phoneNumber){
        if(MyConstants.environment.equals("staging")){
            String url = "http://bapi.sendo.aws/customer/send-otp";
            JSONObject obj = new JSONObject();
            obj.put("type","register");
            obj.put("phone",phoneNumber);
            obj.put("message","%s la ma xac thuc OTP de dat hang tren Sendo");
            String result = APIController.sendApiRequest("POST",url,obj.toString(),"");
            JSONObject token = new JSONObject(result);
            return (String) token.get("token");
        }else if(MyConstants.environment.equals("test")){
            String url = "http://bapi.test.sendo.vn/customer/get-otp?phone=" + phoneNumber;
            String result = APIController.sendApiRequest("GET",url,"","");
            JSONArray tokens = new JSONArray(result);
            JSONObject token = (JSONObject) tokens.get(0);
            return (String) token.get("token");
        }
        return null;
    }

    public static void inputOtp(By selector){
        String otp = getOtp(register.getPhoneNumber());
        register.setOtp(otp);
        ActionGen.sendKeys(selector, otp);
    }

    public static void createShopByApi(){
        if(MyConstants.environment.equals("test")){
            XLSWorker.updateAccountList(register.getPhoneNumber(), 1, "false");
            return;
        }
        WebElement element = ActionGen.getElementMayNull(ActionGen.getBy("Register_email"),10);
        System.out.println("Element: " + element);
        if(element != null){
            XLSWorker.updateAccountList(register.getPhoneNumber(), 1, "false");
            return;
        }
        String url = MyConstants.getSellerApi() + "/api/web/SendoId/Create";
        JSONObject payload = new JSONObject();
        payload.put("phone",register.getPhoneNumber());
        payload.put("otp", register.getOtp());
        payload.put("password",register.getPassword());
        payload.put("confirmPassword",register.getConfirmPassword());
        payload.put("reCaptchaResponse","ABCD");

        String result = APIController.sendApiRequest("POST",url,payload.toString(),"");
        ActionGen.assertTextContains(result, "200", "Create shop with api is failed!");
        XLSWorker.updateAccountList(register.getPhoneNumber(), 1, "false");
    }

    public static void loginShopByApi(){
        ActionGen.deleteCookie();
        ActionGen.reloadPage();
        String url = MyConstants.getSellerApi() + "/api/token/web";
        JSONObject object = new JSONObject();
        object.put("UserName",register.getPhoneNumber());
        object.put("Password",register.getPassword());
        object.put("reCaptchaResponse","ABC");
        String response = APIController.sendApiRequest("POST", url, object.toString(), "");
        String token = APIController.getAuthorizationCode(response);
        String walletId = (String) APIController.getKeyFromUserContextDto("walletId", response);
        int storeId = -1;
        if(walletId != null)
            storeId = (int) APIController.getKeyFromUserContextDto("storeId", response);
        register.setWalletId(walletId);
        register.setStoreId(String.valueOf(storeId));
        ActionGen.assertBooleanEquals(token!=null, true, "Login shop with api is failed!");
        ActionGen.addCookie("token_seller_api",token);
        ActionGen.reloadPage();
    }

    public static void loginWithApi(String payload){
        String url = MyConstants.getSellerApi() + "/api/token/web";
        String response = APIController.sendApiRequest("POST", url, payload, "");
        String token = APIController.getAuthorizationCode(response);
        System.out.println(token);
        ActionGen.assertBooleanEquals(token!=null, true, "Login shop with api is failed!");
        ActionGen.addCookie("token_seller_api",token);
        ActionGen.reloadPage();
    }

    public static void selectColor(String colorRgb){
        WebElement addColor = ActionGen.getElement(ActionGen.getBy("SalesOrder_AddLabelColor"));
        if(addColor!=null && addColor.isDisplayed()){
            addColor.findElement(By.xpath("*//label[contains(@style, '" + colorRgb + "')]//ancestor::span")).click();
            return;
        }
        WebElement editColor = ActionGen.getElement(ActionGen.getBy("SalesOrder_EditLabelColor"));
        if(editColor!=null && editColor.isDisplayed()){
            editColor.findElement(By.xpath("*//label[contains(@style, '" + colorRgb + "')]//ancestor::span")).click();
        }
    }

    public static void verifyLabel(){
        ActionGen.sendKeys(ActionGen.getBy("SalesOrder_SearchLabel"), smartLabel.getLabelName());
        String labelName = ActionGen.getElementText(ActionGen.getBy("SalesOrder_List_LabelName"));
        ActionGen.assertTextEquals(labelName, smartLabel.getLabelName(), "Label name is not found!");
        String labelColor = ActionGen.getElementAttribute(ActionGen.getBy("SalesOrder_List_LabelColor"),"style");
        ActionGen.assertTextContains(labelColor, smartLabel.getLabelColor(), "Label color is not found!");

        Boolean flag1 = false;
        Boolean flag2 = false;
        List<WebElement> labelItems = ActionGen.getElements(By.xpath("//div[@class='label_item']"));
        for(int i=0; i<labelItems.size(); i++){
            String labelName1 = labelItems.get(i).findElement(By.tagName("span")).getText();
            if(labelName1.equals(smartLabel.getLabelName()))
                flag1 = true;
            String labelColor1 = labelItems.get(i).getAttribute("style");
            if(labelColor1.contains(smartLabel.getLabelColor()))
                flag2 = true;
        }
        ActionGen.assertBooleanEquals(flag1, true, "Label name is not found");
        ActionGen.assertBooleanEquals(flag2, true, "Label color is not found");
    }

    public static void deleteLabel(){
        ActionGen.sendKeys(ActionGen.getBy("SalesOrder_SearchLabel"), smartLabel.getLabelName());
        ActionGen.click(ActionGen.getBy("SalesOrder_RemoveLabel"));
        ActionGen.click(By.xpath("//button[contains(@data-bind,'deleteSmartLabel')]"));
        ActionGen.verifyToastMessage("");
    }

    public static void verifyWalletInfo(){
        String source = "http://seller-wallet.test.sendo.vn/api/internal/wallets/" + register.getWalletId();
        String response = APIController.sendApiRequest("GET", source, "", "");
        System.out.println(response);
        JSONObject fullResponse = new JSONObject(response);
        JSONObject result = (JSONObject) fullResponse.get("Result");
        String walletId = (String) result.get("walletId");
        String storeId = (String) result.get("shopId");
        String email = (String) result.get("email");
        String walletPhone = (String) result.get("walletPhone");
        ActionGen.assertTextEquals(walletId, register.getWalletId(), "WalletId is not correct!");
        ActionGen.assertTextEquals(storeId, register.getStoreId(), "StoreId is not correct");
        ActionGen.assertTextEquals(email, register.getEmail(), "Email is not correct");
        ActionGen.assertTextEquals(walletPhone, register.getPhoneNumber(), "Phone is not correct");
    }

    public static void setKeyword(By selector, String value){
        if(value.equals("") || value.equals("NO"))
            return;
        ActionGen.setClick(selector);
        WebElement keyword = ActionGen.getElementMayNull(By.className("ms-sel-item "),1);
        ActionGen.assertElementIsDisplayed(keyword, "Keyword is not dispplayed!");
    }
}
