package operation;

import constant.MyConstants;
import logger.MyLogger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestException;
import utils.FormatUtils;
import utils.GoogleDrive;
import utils.XLSWorker;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Rectangle;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class ActionGen {

    static WebDriver driver;
    static Actions builder;
    static String exception;
    static String originalHandle;
    static JavascriptExecutor js;
    static Actions actions;
    static WebDriverWait wait;
    static String imageFilePath;
    static String imageUrl1;
    static String imageUrl2;

    public ActionGen(WebDriver driver1){
        driver = driver1;
        builder = new Actions(driver);
        js = (JavascriptExecutor) driver;
    }

    /* My Basics */

    public static String getPageTitle() {
        try {
            return driver.getTitle();
        } catch (Exception e) {
            throw new TestException(String.format("Current page title is: %s", driver.getTitle()));
        }
    }

    public static String getCurrentURL() {
        try {
            return driver.getCurrentUrl();
        } catch (Exception e) {
            throw new TestException(String.format("Current URL is: %s", driver.getCurrentUrl()));
        }
    }

    public static String getPageSource(){
        return driver.getPageSource();
    }

    public static WebElement getElement(By selector) {
        try {
            waitForElementToBeVisible(selector, 10);
            MyLogger.info("--Start to get element with selector: " + selector);
            return driver.findElement(selector);
        } catch (Exception e) {
            MyLogger.error("--Failed to get element: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static WebElement getElementMayNull(By selector, int timeout){
        try {
            waitForElementToBeVisibleMayNull(selector, timeout);
            MyLogger.info("--Start to get element may null with selector: " + selector);
            return driver.findElement(selector);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getElementText(By selector) {
        waitForAjaxDisappear();
        waitForElementToBeVisible(selector,20);
        waitUntilElementIsDisplayedOnScreen(selector, 10);
        MyLogger.info("--Start to get element text with selector " + selector);
        WebElement element = getElement(selector);
        int count = 0;
        while(count < 2){
            try {
                return StringUtils.trim(element.getText());
            }catch (StaleElementReferenceException e) {
                element = getElement(selector);
                count++;
            }catch (Exception e) {
                MyLogger.error("--Failed to get element text: " + e.getMessage());
                throw new TestException(e);
            }
        }
        return null;
    }

    public static String getElementCssValue(By selector, String property){
        try{
            return driver.findElement(selector).getCssValue(property);
        } catch (Exception e){
            exception = String.format("Element %s does not exist - proceeding", selector);
            MyLogger.error(exception);
        }
        return null;
    }

    public static String getElementAttribute(By selector, String property){
        try{
            return driver.findElement(selector).getAttribute(property);
        } catch (Exception e){
        }
        return null;
    }

    public String getElementAttribute(WebElement element, String property){
        try{
            return element.getAttribute(property);
        } catch (Exception e){
            MyLogger.error(exception);
        }
        return null;
    }


    public static List<WebElement> getElements(By Selector) {
        waitForElementToBeVisible(Selector, 10);
        try {
            MyLogger.info("--Start to get elements with selector: " + Selector);
            return driver.findElements(Selector);
        } catch (Exception e) {
            MyLogger.error("--Failed to get elements: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public List<String> getListOfElementTexts(By selector) {
        List<String> elementList = new ArrayList<String>();
        List<WebElement> elements = getElements(selector);

        for (WebElement element : elements) {
            if (element == null) {
                throw new TestException("Some elements in the list do not exist");
            }
            if (element.isDisplayed()) {
                elementList.add(element.getText().trim());
            }
        }
        return elementList;
    }

    public WebElement getElementNotVisible(By selector){
        // Solution for element is not visible
        int elementSize = getElements(selector).size();
        return getElements(selector).get(elementSize-1);
    }

    public void clickStaleElement(By selector){
        // Solution for stale element
        for(int i=0; i<3; i++){
            try{
                getElement(selector).click();
                break;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static void click(By selector) {
        MyLogger.info("--Start to click with selector: " + selector);
        WebElement element = getElement(selector);
        waitForAjaxDisappear();
        waitForElementToDisplay(selector);
        waitForElementToBeClickable(selector, 10);
        int count = 0;
        while(count < 2){
            try{
                element.click();
                break;
            }catch (StaleElementReferenceException e){
                element = getElement(selector);
                count++;
            }catch (Exception e){
                MyLogger.error("--Failed to click selector: " + e.getMessage());
                throw new TestException(e);
            }

        }
    }

    public static void clickMayNull(By selector, int times){
        MyLogger.info("--Start to click may null with selector: " + selector);
        WebElement element;
        int time = 0;
        do{
            System.out.println(time);
            element = getElementMayNull(selector,1);
            if(element != null && element.isDisplayed()){
                click(selector);
                break;
            }
            time++;
        }while(element==null && time < times);
    }

    public static void sendKeys(By selector, String value) {
        WebElement element = getElement(selector);
        waitForAjaxDisappear();
        waitForElementToDisplay(selector);
        int count = 0;
        while(count < 5){
            try{
                clearField(element);
                MyLogger.info("--Start to send keys with selector: " + selector + ", value: " + value);
                element.sendKeys(value);
                break;
            }catch (StaleElementReferenceException e){
                element = getElement(selector);
                count++;
            }catch (Exception e){
                MyLogger.error("--Failed to click selector: " + e.getMessage());
                throw new TestException(e);
            }

        }
    }

    public static void clearField(WebElement element) {
        try {
            MyLogger.info("--Start to clear field");
            element.clear();
            waitForElementTextToBeEmpty(element);
        } catch (Exception e) {
            MyLogger.error("--Failed to clear field: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static void sleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void selectIfOptionTextContains(By selector, String searchCriteria) {
        waitForElementToBeClickable(selector, 10);
        Select dropdown = new Select(getElement(selector));

        List<WebElement> options = dropdown.getOptions();

        String optionText = "";

        if (options == null) {
            throw new TestException("Options for the dropdown list cannot be found.");
        }

        for (WebElement option : options) {

            optionText = option.getText().trim();
            boolean isOptionDisplayed = option.isDisplayed();

            if (optionText.contains(searchCriteria) && isOptionDisplayed) {
                try {
                    dropdown.selectByVisibleText(optionText);
                    break;
                } catch (Exception e) {
                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
                }
            }
        }
    }

    public static void selectIfOptionTextEquals(By selector, String searchCriteria) {

        if(searchCriteria.equals(""))
            return;

        waitForElementToBeClickable(selector, 10);
        Select dropdown = new Select(getElement(selector));

        List<WebElement> options = dropdown.getOptions();

        String optionText = "";

        if (options == null) {
            throw new TestException("Options for the dropdown list cannot be found.");
        }

        for (WebElement option : options) {

            optionText = option.getText().trim();
            boolean isOptionDisplayed = option.isDisplayed();

            if (optionText.equals(searchCriteria) && isOptionDisplayed) {
                try {
                    dropdown.selectByVisibleText(optionText);
                    System.out.println(optionText + " is selected");
                    break;
                } catch (Exception e) {
                    throw new NoSuchElementException(String.format("The following element did not display: [%s] ", selector.toString()));
                }
            }
        }
    }

    public List<String> getDropdownValues(By selector) {
        waitForElementToDisplay(selector);
        Select dropdown = new Select(getElement(selector));
        List<String> elementList = new ArrayList<String>();

        List<WebElement> allValues = dropdown.getOptions();

        if (allValues == null) {
            throw new TestException("Some elements in the list do not exist");
        }

        for (WebElement value : allValues) {
            if (value.isDisplayed()) {
                elementList.add(value.getText().trim());
            }
        }
        return elementList;
    }

    public static String getTextFromInput(By selector){
        WebElement element = getElement(selector);
        return element.getAttribute("value");
    }

    public static String getValueFromCheckbox(By selector){
        WebElement element = getElement(selector);
        if(element.isSelected())
            return "YES";
        else
            return "NO";
    }

    public String getValueFromMultiDropDown(By selector){
        WebElement input = getElement(selector);
        List<WebElement> values = input.findElements(By.className("item"));
        System.out.println("Sizes list " + values.size());
        String result = "";
        for(int i=0; i<values.size(); i++){
            if(i==0)
                result = result + values.get(i).getText().replace("\n×","");
            else
                result = result + ", " + values.get(i).getText().replace("\n×","");
        }
        System.out.println(result);
        return result;
    }

    public void setElementWait(int millisecond) {
        // Wait for element
        driver.manage().timeouts().implicitlyWait(millisecond, TimeUnit.MILLISECONDS);
    }

    public static void selectFromDropDown(By selector, String value){
        MyLogger.info("--Start to select from dropdown: " + selector + ", value: " + value);
        Select dropdown = new Select(getElement(selector));
        dropdown.selectByVisibleText(value);
    }

    public static void selectOneFromDropDown(By selector, String option){
        MyLogger.info("--Start to select one from dropdown: " + selector + ", value: " + option);
        if(option.equals(""))
            return;
        click(selector);
        click(By.xpath("//div[contains(@class,'option') and text()='" + option + "']"));
        //clickByJS(driver.findElement(By.xpath("//div[contains(@class,'option) and text()='" + option + "']")));
    }

    public static void setCheckbox(By selector, String value){
        MyLogger.info("--Start to set checkbox with selector: " + selector + ", value: " + value);
        if(value.equals("YES")){
            WebElement checkBox = getElement(selector);
            if(!checkBox.isSelected())
                clickByJS(checkBox);
        }else{
            WebElement checkBox = getElementMayNull(selector, 1);
            if(checkBox!=null && checkBox.isSelected())
                clickByJS(checkBox);
        }
    }

    public static void setClick(By selector){
        sleep(2000);
        clickByJS(getElementMayNull(selector,1));
        verifyToastMessage("");
    }

    public static void setClick2(By selector){
        sleep(2000);
        clickByJS(driver.findElement(selector));
    }

    public static void selectMultiFromDropDown(By selector, String options){
        if(options.equals(""))
            return;
        clickByJS(driver.findElement(selector));
        String[] optionList = options.split(", ");
        for(int i=1; i<=optionList.length; i++){
            click(By.xpath("//div[contains(@class,'option') and text()='" + optionList[i-1] + "']"));
        }
        driver.findElement(selector).sendKeys(Keys.ESCAPE);
    }

    public static String getIdFromUrl(){
        String url = driver.getCurrentUrl();
        String[] sections = url.split("/");
        String id = sections[sections.length-1];
        MyLogger.info("--Product ID from url: " + id);
        return id;
    }

    public static void waitAjaxDisappear(By selector){
        try {
            waitForElementToBeInvisible(selector, 10);
        } catch (Exception e) {
            exception = String.format("Element %s does not exist - proceeding", selector);
            System.out.println(exception);
        }
    }

    public static boolean isAlertPresent(){
        boolean foundAlert;
        WebDriverWait wait = new WebDriverWait(driver, 2);
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            foundAlert = true;
        } catch (TimeoutException e) {
            foundAlert = false;
        }
        return foundAlert;
    }

    public static void verifySecurity(String value){
        sleep(1500);
        clickMayNull(By.xpath("//button[contains(@class,'CloseButton')]"),10);
        By selector = By.name(value);
        WebElement element = getElement(selector);
        switch(value) {
            case "click":
                if(element!=null) {
                    scrollDownPage("500");
                    mouseHover(element);
                    click(selector);
                }
                break;
            case "mouseover":
            case "mousemove":
                if(element!=null) {
                    scrollToElement(element);
                    mouseHover(element);
                }
                break;
            case "popup":
                WebElement popup = getElement(By.name("btnOK"));
                if(popup==null)
                    assertTrue(false);
        }
        if(isAlertPresent()) {
            logFail("XSS detected!");
        }
        System.out.println("Verify Security!");
    }

    public static void clickModalConfirm(String value){
        MyLogger.info("Start to click on confirmation modal popup");
        sleep(2000);
        WebElement btnOk = getElement(By.xpath("//a[@name='btnOk']"));
        WebElement btnCancel = getElement(By.xpath("//a[@name='btnCancel']"));
        if(true){
            clickByJS(btnOk);
            verifyToastMessage(value);
        }
        else
            clickByJS(btnCancel);
    }

    /* My Navigates */

    public static void navigateToURL(String URL) {
        String baseUrl = getUrl("URL_BaseSellerUrl");
        System.out.println("BaseURL: " + baseUrl);
        if(URL.contains("URL_")){
            if(!URL.equals("URL_BaseSellerUrl") && !URL.equals("URL_BaseBuyerUrl"))
                URL = baseUrl + getUrl(URL);
            else{
                URL = getUrl(URL);
            }
        }
        System.out.println("URL: " + URL);
        if(URL.contains("https://ban.sendo.vn")){
            URL = URL.replace("https://ban.sendo.vn",MyConstants.getBaseSellerUrl());
            System.out.println("URL: " + URL);
        }
        if(URL.contains("https://sendo.vn") || URL.contains("https://www.sendo.vn")){
            URL = URL.replace("www.","").replace("https://sendo.vn",MyConstants.getBaseBuyerUrl());
            System.out.println("URL: " + URL);
        }
        try {
            MyLogger.info("--Start to navigate to url: " + URL);
            driver.navigate().to(URL);
            reloadPage();
            verifyToastMessage("");
            waitForPageLoad();
        }catch (Exception e) {
            MyLogger.error("--Failed to navigate to url: " + e.getMessage());
            throw new TestException(e);
        }

    }

    public void navigateBack() {
        try {
            driver.navigate().back();
        } catch (Exception e) {
            System.out.println("FAILURE: Could not navigate back to previous page.");
            throw new TestException("Could not navigate back to previous page.");
        }
    }

    public static void reloadPage(){
        driver.navigate().refresh();
        waitForPageLoad();
    }

    /* My Waits */

    public static void waitForElementToDisplay(By Selector) {
        int count = 0;
        WebElement element = getElement(Selector);
        while (!element.isDisplayed() && count < 20) {
            count++;
            sleep(200);
        }
    }

    public static void waitForElementTextToBeEmpty(WebElement element) {
        String text;
        try {
            text = element.getText();
            int maxRetries = 10;
            int retry = 0;
            while ((text.length() >= 1) || (retry < maxRetries)) {
                retry++;
                text = element.getText();
            }
        } catch (Exception e) {
            System.out.print(String.format("The following element could not be cleared: [%s]", element.getText()));
        }

    }

    public static void waitForElementToBeVisible(By selector, int timeout) {
        try {
            MyLogger.info("--Start to wait for " + selector + " to display to be presence");
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
            MyLogger.error("--Failed to wait for element to be visible: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static void waitForElementToBeVisibleMayNull(By selector, int timeout){
        try {
            MyLogger.info("--Start to wait for " + selector + " to display to be presence");
            wait = new WebDriverWait(driver, 1);
            wait.until(ExpectedConditions.presenceOfElementLocated(selector));
        } catch (Exception e) {
        }
    }

    public static void waitForElementToBeInvisible(By selector, int timeout){
        try {
            MyLogger.info("--Start to wait for " + selector + " to display to be invisible");
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
        } catch (Exception e) {
            MyLogger.error("--Failed to wait for element to be invisible: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static void waitUntilElementIsDisplayedOnScreen(By selector, int timeout) {
        try {
            MyLogger.info("--Start to wait for " + selector + " to display on screen");
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
        } catch (Exception e) {
            MyLogger.error("--Failed to wait for element to display: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static void waitForElementToBeClickable(By selector, int timeout) {
        try {
            MyLogger.info("--Start to wait for " + selector + " to be clickable");
            wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.elementToBeClickable(selector));
            waitForAjaxDisappear();
        } catch (Exception e) {
            MyLogger.error("--Failed to wait for element to be clickable: " + e.getMessage());
            throw new TestException(e);
        }
    }

    public static void waitForAjaxDisappear() {
        WebElement ajaxLoading;
        int time = 0;
        do {
            ajaxLoading = getElementMayNull(By.id("loading"),1);
            sleep(1000);
            time++;
            if(time==10)
                break;
        }while(ajaxLoading != null && ajaxLoading.isDisplayed());
    }

    public static void waitForPageLoad() {
        String state = null;
        String oldstate = null;
        try {
            int i = 0;
            while (i < 5) {
                Thread.sleep(1000);
                state = ((JavascriptExecutor)driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + Character.toUpperCase(state.charAt(0)) + ".");
                if (state.equals("interactive") || state.equals("loading"))
                    break;
                /*
                 * If browser in 'complete' state since last X seconds. Return.
                 */

                if (i == 1 && state.equals("complete")) {
                    //System.out.println();
                    return;
                }
                i++;
            }
            i = 0;
            oldstate = null;
            Thread.sleep(2000);

            /*
             * Now wait for state to become complete
             */
            while (true) {
                state = ((JavascriptExecutor)driver).executeScript("return document.readyState;").toString();
                //System.out.print("." + state.charAt(0) + ".");
                if (state.equals("complete"))
                    break;

                if (state.equals(oldstate))
                    i++;
                else
                    i = 0;
                /*
                 * If browser state is same (loading/interactive) since last 60
                 * secs. Refresh the page.
                 */
                if (i == 15 && state.equals("loading")) {
                    System.out.println("\nBrowser in " + state + " state since last 60 secs. So refreshing browser.");
                    driver.navigate().refresh();
                    System.out.print("Waiting for browser loading to complete");
                    i = 0;
                } else if (i == 6 && state.equals("interactive")) {
                    System.out.println(
                            "\nBrowser in " + state + " state since last 30 secs. So starting with execution.");
                    return;
                }

                Thread.sleep(4000);
                oldstate = state;

            }
            System.out.println();

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    /* My Actions */

    public void dragAndDrop(WebElement elementFrom, WebElement elementTo){
        Action dragAndDrop = builder.clickAndHold(elementFrom)
                .moveToElement(elementTo)
                .release(elementTo)
                .build();
        dragAndDrop.perform();
    }

    public static void mouseHover(WebElement element){
        try {
            builder.moveToElement(element).build().perform();
        }catch(Exception e) {
            System.out.println("Exception: " + e);
        }
        sleep(2000);
    }

    public void rightClick(WebElement element){
        builder.contextClick(element).build().perform();
    }

    public void doubleClick(WebElement element){
        builder.doubleClick(element).build().perform();
    }

    /* My switches */

    public static void switchToNewWindow(){
        originalHandle = driver.getWindowHandle();
        Set handles = driver.getWindowHandles();
        for (String handle1 : driver.getWindowHandles()) {
            MyLogger.info("--Start to switch to new window: " + handle1);
            driver.switchTo().window(handle1);
        }
        waitForPageLoad();
    }

    public static void switchToOldWindow(){
        for(String handle : driver.getWindowHandles()) {
            if (!handle.equals(originalHandle)) {
                MyLogger.info("--Start to switch to old window: " + handle);
                driver.switchTo().window(handle);
                driver.close();
            }
        }
        driver.switchTo().window(originalHandle);
    }

    public static void switchToNewFrame(String locator){
        MyLogger.info("--Start to switch to new frame with locator: " + locator);
        driver.switchTo().frame(locator);
    }

    public static void switchToNewFrame(WebElement element) {
        MyLogger.info("--Start to switch to new frame");
        driver.switchTo().frame(element);
    }


    public static void switchToOldFrame(){
        MyLogger.info("--Start to switch to old frame");
        driver.switchTo().defaultContent();
    }

    public static WebElement switchToActiveElement(){
        MyLogger.info("--Start to switch to text editor");
        return driver.switchTo().activeElement();
    }

    /* My Verifys */

    public static void verifyText(By selector, String message){
        MyLogger.info("--Start to verify text with selector: " + selector + ", message: " + message);
        List<WebElement> validationMessages = ActionGen.getElements(selector);
        if(message.equals(""))
            assertBooleanEquals(validationMessages.size() == 0, true, "");
        else{
            assertBooleanEquals(validationMessages.size() > 0, true, "");
            WebElement validationMessage = validationMessages.get(validationMessages.size()-1);
            waitForElementToDisplay(selector);
            if(validationMessage!=null){
                String text = validationMessage.getText();
                if(validationMessage.getAttribute("disabled")!=null && validationMessage.getAttribute("disabled").equals("true"))
                    text = validationMessage.getAttribute("value");
                assertTextContains(text.toLowerCase(),message.toLowerCase(),"Message is not matched");
            }

        }
    }

    public static void verifyToastMessage(String message){
        MyLogger.info("--Start to verify toast message with message: " + message);
        WebElement toastMessage = getElementMayNull(getBy("General_toastMessage"),1);
        if(message.equals("")){
            //assertElementIsNotDisplayed(toastMessage,"[FAILURE]: Find toast message displayed in case no message set from excel.");
            if(toastMessage != null)
                MyLogger.error("[FAILURE]: Find toast message displayed in case no message set from excel.");
        }
        else if(message.equals("Popup")) {
            WebElement popup = getElement(getBy("General_modalDialog"));
            assertElementIsDisplayed(popup,"");
        }else{
            int time = 0;
            while(toastMessage==null) {
                toastMessage = getElementMayNull(getBy("General_toastMessage"),1);
                time++;
                if(time==10) {
                    //ActionGen.logFail("[FAILURE]: Cannot find toast message displayed in case of a message set from excel.");
                    //return;
                    break;
                }
            }
            waitForElementToDisplay(getBy("General_toastMessage"));
            assertTextContains(toastMessage.getText(),message,"[FAILURE]: Toast message is not matched with expected message set from excel.");
        }
    }

    public static void verifyUrl(String url){
        MyLogger.info("--Start to verify Url: " + url);
        waitForPageLoad();
        String currentUrl = driver.getCurrentUrl();
        assertTextContains(currentUrl,url,"Current url is not matched with expected url set from excel.");
    }

    public static void verifyNoRedToast(){
        WebElement toastMessage = getElementMayNull(By.xpath("//*[@class='toast toast-error' or @class='toast toast-warning']"),1);
        if (toastMessage != null){
            logFail("[FAILURE]: Find toast message displayed in case no message set from excel.");
        }
    }

    /* My Keyboards */

    public static void pressKey(By selector, String key){
        MyLogger.info("--Start to press key: " + key);
        switch (key){
            case "ENTER":
                getElement(selector).sendKeys(Keys.ENTER);
                break;
            case "ESC":
                getElement(selector).sendKeys(Keys.ESCAPE);
                break;
        }
    }

    public static void pressKeyOnElement(WebElement element, String key){
        MyLogger.info("--Start to press key: " + key);
        switch (key){
            case "ENTER":
                element.sendKeys(Keys.ENTER);
                break;
            case "ESC":
                element.sendKeys(Keys.ESCAPE);
                break;
        }
    }

    public static void pressKeyByRobot(String key){
        try{
            Robot r=new Robot();
            switch (key){
                case "ENTER":
                    r.keyPress(KeyEvent.VK_ENTER);
                    r.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case "ESC":
                    r.keyPress(KeyEvent.VK_ESCAPE);
                    r.keyRelease(KeyEvent.VK_ESCAPE);
                    break;
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /* My Javascript */

    public WebElement getElementByIdByJS(String id){
        WebElement element;
        element =  (WebElement) js.executeScript("return document.getElementById('"+ id + "')");
        return element;
    }

    public List<WebElement> getElementsByClassNameByJS(String className){
        List<WebElement> elements;
        elements =  (List<WebElement>) js.executeScript("return document.getElementsByClassName('"+ className + "')");
        return elements;
    }

    public String getTextByJS(String xpath){
        return (String) (js.executeScript("angular.element($x('"+ xpath +"')).text()"));
    }

    public void getCSSValue(By selector){
        WebElement switchLabel = driver.findElement(By.cssSelector(".className"));
        String colorRGB = ((JavascriptExecutor)driver)
                .executeScript("return window.getComputedStyle(arguments[0], ':before').getPropertyValue('background-color');",switchLabel).toString();
    }

    public static void clickByJS(WebElement element){
        MyLogger.info("--Start to set click by JS");
        try {
            js.executeScript("arguments[0].click();", element);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void hideElement(WebElement element) {
        js.executeScript("arguments[0].style.display='none';", element);
    }

    public static void typeByJS(WebElement element, String value){
        js.executeScript("arguments[0].value='"+ value +"'", element);
    }

    public static void scrollToEndPage(){
        MyLogger.info("--Start to scroll to end page by JS");
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public static void scrollToElement(WebElement element){
        MyLogger.info("--Start to scroll to element by JS");
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void scrollToThenClick(By selector) {
        MyLogger.info("--Start to scroll to element then click by JS");
        WebElement element = getElement(selector);
        actions = new Actions(driver);
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            actions.moveToElement(element).perform();
            actions.click(element).perform();
            //ActionGen.waitForPageLoad();
        } catch (Exception e) {
            System.out.println("Failed to click element: " + e.getMessage());
            throw new TestException(String.format("The following element is not clickable: [%s]", element.toString()));
        }
    }

    public static void scrollDownPage(String value){
        MyLogger.info("--Start to scroll down page by JS with value: " + value);
        js.executeScript("window.scrollBy(0,"+ value +")");
    }

    public void highlightElement(WebElement element) {
        js.executeScript("arguments[0]" + ".setAttribute('style','background-color: yellow; border: 2px solid red;')", element);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        js.executeScript("arguments[0]" + ".setAttribute('style','border: 2px solid white;')", element);
    }

    public void innerHtmlByJs(String xpath, String value){
        js.executeScript("document.evaluate('"+ xpath +"',document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)" +
                ".singleNodeValue.innerHTML = '"+ value +"';");
    }

    public void highlightElement(WebDriver driver, WebElement element) {

        JavascriptExecutor js = ((JavascriptExecutor) driver);

        js.executeScript("arguments[0]" + ".setAttribute('style','background-color: yellow; border: 2px solid red;')",
                element);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        js.executeScript("arguments[0]" + ".setAttribute('style','border: 2px solid white;')", element);

    }

    /* My Tools */

    public static String getImageFilePath(){
        return imageFilePath;
    }

    public static String getImageUrl2(){
        return imageUrl2;
    }

    public static String getImageUrl1(){
        return imageUrl1;
    }

    public static String takeScreenshot(String method) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String screenshotName = dtf.format(now) + "-" + method + "-" + FormatUtils.GenerateNumber(10000) + ".jpg";
            imageFilePath = MyConstants.SCREENSHOT_PATH + screenshotName;
            MyLogger.info("--Start to take screen shot with image: " + imageFilePath);
            FileUtils.copyFile(scrFile, new File(imageFilePath));
            imageUrl2 = MyConstants.SCREENSHOT_URL + screenshotName;
            imageUrl1 = "https://drive.google.com/open?id=" + uploadPhotoToDrive(imageFilePath);
            MyLogger.info("--Image url on google drive is: " + imageUrl1);
        } catch (Exception e) {
            MyLogger.error("--Have an error occurring while taking failed testcase photo!");
            return null;
        }
        return imageFilePath;
    }

    public static String uploadPhotoToDrive(String filePath) {
        String screenshotUrl = "";
        try {
            if(filePath==null) {
                MyLogger.error("--Failed to capture a photo because have an error occurs while capturing!");
                return null;
            }

            MyLogger.info("--Start to upload screenshot to google drive with file: " + filePath);
            screenshotUrl = GoogleDrive.uploadScreenshot(filePath);
            if(screenshotUrl == null) {
                MyLogger.error("--Have an error occurring while uploading photo to google drive");
                return null;
            }
            GoogleDrive.setPhotoPermission();
        }catch(Exception e) {
            MyLogger.error("--Undentified error!");
            return null;
        }
        return screenshotUrl;
    }

    public static void uploadByRobot(String image){
        MyLogger.info("--Start to upload with image: " + MyConstants.IMAGES_PATH + image);
        try {
            StringSelection ss = new StringSelection(MyConstants.IMAGES_PATH + image);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
            sleep(1000);
            Robot robot = new Robot();
            if(System.getProperty("os.name").startsWith("Windows") || System.getProperty("os.name").startsWith("Linux")){
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }else if(System.getProperty("os.name").startsWith("Mac")){
                // Cmd + Tab is needed since it launches a Java app and the browser looses focus
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_TAB);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_TAB);
                robot.delay(500);
                //Open Goto window
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_SHIFT);
                robot.keyPress(KeyEvent.VK_G);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_SHIFT);
                robot.keyRelease(KeyEvent.VK_G);
                //Paste the clipboard value
                robot.keyPress(KeyEvent.VK_META);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_META);
                robot.keyRelease(KeyEvent.VK_V);
                //Press Enter key to close the Goto window and Upload windo
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
                robot.delay(500);
                robot.keyPress(KeyEvent.VK_ENTER);
                robot.keyRelease(KeyEvent.VK_ENTER);
            }

        }catch (Exception e){
            MyLogger.error("Failed to upload photo: " + e.getMessage());
            throw new TestException(e);
        }
        sleep(1000);
    }

    public static String CaptureScreenShotByRobot(String testStepsName) {
        try{
            Robot robotClassObject = new Robot();
            Rectangle screenSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage tmp = robotClassObject.createScreenCapture(screenSize);
            String path=System.getProperty("user.dir")+"/ScreenCapturesPNG/"+testStepsName+System.currentTimeMillis()+".png";
            ImageIO.write(tmp, "png",new File(path));
            return path;
        }catch(Exception e) {
            System.out.println("Some exception occured." + e.getMessage());
            return "";
        }
    }

    public static void writeJSONFile(JSONObject obj, String fileName) {
        //File file = new File(System.getProperty("user.dir") + "//data//" + fileName);
        File file = new File(MyConstants.OUTPUT_PATH + fileName);
        PrintWriter out = null;
        String content = null;
        try {
            out = new PrintWriter(new FileWriter(file, true));
            String item = obj.toString().replace("[", "").replace("]", "");
            out.append(item);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.flush();
            out.close();
            try {
                content = IOUtils.toString(new FileInputStream(file), "UTF-8");
                if(content.contains("}{")) {
                    content = content.replace("}{", ",");
                }
                IOUtils.write(content, new FileOutputStream(file), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void logFail(String log) {
        exception = log;
        MyLogger.warn(log);
        assertTrue(false);
    }

    public static void assertTextEquals(String actualMessage, String expectedMessage, String logMessage){
        MyLogger.info("--Start to assert text equals: " + actualMessage + " - " + expectedMessage);
        if(!actualMessage.equals(expectedMessage))
            logFail(logMessage);
    }

    public static void assertTextNotEquals(String actualMessage, String expectedMessage, String logMessage){
        MyLogger.info("--Start to assert text not equals: " + actualMessage + " - " + expectedMessage);
        if(actualMessage.equals(expectedMessage))
            logFail(logMessage);
    }

    public static void assertBooleanEquals(Boolean actual, Boolean expect, String logMessage){
        MyLogger.info("--Start to assert boolean equals: " + actual + " - " + expect);
        if(!actual.equals(expect))
            logFail(logMessage);
    }

    public static void assertTextContains(String actualMessage, String expectedMessage, String logMessage){
        MyLogger.info("--Start to assert text contains: " + actualMessage + " - " + expectedMessage);
        if(!actualMessage.contains(expectedMessage))
            logFail(logMessage);
    }

    public static void assertElementIsDisplayed(WebElement element, String logMessage){
        MyLogger.info("--Start to assert element is displayed");
        if(element == null)
            logFail(logMessage);
    }

    public static void assertElementIsNotDisplayed(WebElement element, String logMessage){
        MyLogger.info("--Start to assert element is not displayed");
        if(element != null && element.isDisplayed())
            logFail(logMessage);
    }

    public static void assertElementIsSelected(WebElement element, String logMessage){
        if(!element.isSelected())
            logFail(logMessage);
    }

    public static void assertElementIsNotSelected(WebElement element, String logMessage){
        MyLogger.info("--Start to assert element is not selected");
        if(element.isSelected())
            logFail(logMessage);
    }

    public static String waitForEmptyValueChanged(WebElement element, int timeout){
        int time = 0;
        while(element.getText().equals("") && time <= timeout){
            ActionGen.sleep(1000);
            time++;
        }
        return element.getText();
    }

    public static void addCookie(String key, String value){
        try{
            Cookie cookie = new Cookie(key,value);
            driver.manage().addCookie(cookie);
        }catch (Exception e){
            MyLogger.error2(e.getMessage());
        }

    }

    public static void deleteCookie(){
        driver.manage().deleteAllCookies();
    }

    public static By getBy(String object){
        String[] spl = object.split("_");
        String sheetName = spl[0];
        String filePath = MyConstants.INPUT_PATH + "Properties.xlsx";
        XSSFWorkbook workbook = XLSWorker.getWorkbook(filePath);
        XSSFSheet sheet = XLSWorker.getSheet(workbook, sheetName);
        for(int i=1; i<=sheet.getLastRowNum(); i++){
            if(sheet.getRow(i).getCell(0).toString().equals(object)){
                String locator = sheet.getRow(i).getCell(1).toString();
                String using = sheet.getRow(i).getCell(2).toString();
                //find by xpath
                if (using.equalsIgnoreCase("XPATH")) {
                    return By.xpath(locator);
                }

                //find by class
                else if (using.equalsIgnoreCase("CLASSNAME")) {
                    return By.className(locator);
                }

                //find by id
                else if (using.equalsIgnoreCase("ID")) {
                    return By.id(locator);
                }

                //find by name
                else if (using.equalsIgnoreCase("NAME")) {
                    return By.name(locator);
                }

                //find by css
                else if (using.equalsIgnoreCase("CSS")) {
                    return By.cssSelector(locator);
                }

                //find by link
                else if (using.equalsIgnoreCase("LINKTEXT")) {
                    return By.linkText(locator);
                }

                //find by partial link
                else if (using.equalsIgnoreCase("PARTIALLINK")) {
                    return By.partialLinkText(locator);
                }

                MyLogger.error("Cannot find By in properties file for " + object + " in properties file!");
                assertTrue(false);
            }
        }
        MyLogger.error("Cannot find " + object + " in properties file!");
        assertTrue(false);
        return null;
    }

    public static String getUrl(String value) {
        String filePath = MyConstants.INPUT_PATH + "Properties.xlsx";
        XSSFWorkbook workbook = XLSWorker.getWorkbook(filePath);
        XSSFSheet sheet = XLSWorker.getSheet(workbook, "URL");
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            if (sheet.getRow(i).getCell(0).toString().equals(value))
                return sheet.getRow(i).getCell(1).toString();
        }
        return null;
    }
}
