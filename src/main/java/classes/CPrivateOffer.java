package classes;

import utils.FormatUtils;

public class CPrivateOffer {
    private String productName;
    private String originalMoney;
    private String discountPercent;
    private String discountMoney;
    private String expiredDate;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getOriginalMoney() {
        return originalMoney;
    }

    public void setOriginalMoney(String originalMoney) {
        this.originalMoney = originalMoney;
    }

    public String getDiscountPercent() {
        if(!this.discountPercent.equals("") && !this.discountPercent.equals("NO")){
            return discountPercent;
        }else{
            float dis_percent = Long.valueOf(this.discountMoney)*100
                    / Long.valueOf(this.originalMoney.replace(",",""));
            return String.valueOf(dis_percent).replace(".0", "");
        }

    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getDiscountMoney() {
        return this.discountMoney;
    }

    public void setDiscountMoney(String discountMoney) {
        this.discountMoney = discountMoney;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getDiscountPrice(){
        float discountPrice;
        if(!this.discountPercent.equals("") && !this.discountPercent.equals("NO")) {
        	long originalMoney = Long.valueOf(this.originalMoney.replace(",", ""));
        	float percent = (1 - (Long.valueOf(this.discountPercent) / 100.0f));
            discountPrice = originalMoney*percent;
            System.out.println("originalMoney: " + Long.valueOf(this.originalMoney.replace(",", "")));
        	System.out.println("Discount percent: " + (1 - (Long.valueOf(this.discountPercent) / 100.0f)));
        	System.out.println("Discountprice: " + discountPrice);
        }else{
            discountPrice = Long.valueOf(this.originalMoney.replace(",","")) - Long.valueOf(this.discountMoney);
        }
        return FormatUtils.formatNumber(String.valueOf(discountPrice).replace(".0", ""), "#,000");
    }

    public String getOriginalMoneyFormat(){
        return FormatUtils.formatNumber(String.valueOf(getOriginalMoney()).replace(".0", ""), "#,000");
    }
}
