package classes;

public class CCarrier {
    private Boolean cptc;
    private Boolean cpbk;
    private Boolean cptn;
    private Boolean cpht;

    public Boolean getCptc() {
        return cptc;
    }

    public void setCptc(Boolean cptc) {
        this.cptc = cptc;
    }

    public Boolean getCpbk() {
        return cpbk;
    }

    public void setCpbk(Boolean cpbk) {
        this.cpbk = cpbk;
    }

    public Boolean getCptn() {
        return cptn;
    }

    public void setCptn(Boolean cptn) {
        this.cptn = cptn;
    }

    public Boolean getCpht() {
        return cpht;
    }

    public void setCpht(Boolean cpht) {
        this.cpht = cpht;
    }
}
