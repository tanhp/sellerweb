package classes;

import constant.MyConstants;

public class CShopInfo {
    private String slogan;
    private String shortDescription;
    private String sellerName;
    private String taxCode;
    private String shopAddress;
    private String shopCity;
    private String shopDistrict;
    private String shopWard;

    private String shopName;
    private String shopUrl;
    private String phone;
    private String email;

    public String getSlogan(){
        return this.slogan;
    }

    public String getShortDescription(){
        return this.shortDescription;
    }

    public String getSellerName(){
        return this.sellerName;
    }

    public String getTaxCode(){
        return this.taxCode;
    }

    public String getShopAddress(){
        return this.shopAddress;
    }

    public String getShopCity(){
        return this.shopCity;
    }

    public String getShopDistrict(){
        return this.shopDistrict;
    }

    public String getShopWard(){
        return this.shopWard;
    }

    public String getShopName(){
        return this.shopName;
    }

    public String getShopUrl(){
        return this.shopUrl;
    }

    public String getPhone(){
        return this.phone;
    }

    public String getEmail(){
        return this.email;
    }


    public void setSlogan(String slogan){
        this.slogan = slogan;
    }

    public void setShortDescription(String description){
        this.shortDescription = description;
    }

    public void setSellerName(String sellerName){
        this.sellerName = sellerName;
    }

    public void setTaxCode(String taxCode){
        this.taxCode =  taxCode;
    }

    public void setShopAddress(String shopAddress){
        this.shopAddress = shopAddress;
    }

    public void setShopCity(String shopCity){
        this.shopCity = shopCity;
    }

    public void setShopDistrict(String shopDistrict){
        this.shopDistrict = shopDistrict;
    }

    public void setShopWard(String shopWard){
        this.shopWard = shopWard;
    }

    public void setShopName(String shopName){
        this.shopName = shopName;
    }

    public void setShopUrl(String shopUrl){
        System.out.println("ABC: " + shopUrl);
        this.shopUrl = shopUrl.replace("http://sendo.vn",MyConstants.getBaseBuyerUrl().replace("https://sendo.vn","https://www.sendo.vn"));
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public void setEmail(String email){
        this.email = email;
    }
}
