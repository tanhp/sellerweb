package classes;

import utils.FormatUtils;
import utils.XLSWorker;

public class MyClasses {

    private static CProduct product;
    private static CShopInfo shopInfo;
    private static CPromotion promotion;
    private static CChat chat;
    private static CShippingSupport shipping;
    private static CPrivateOffer privateOffer;
    private static CSalesOrder salesOrder;
    private static CCarrier carrier;
    private static CBlog blog;
    private static CBanner banner;
    private static CVoucher voucher;
    private static CSearchProduct searchProduct;
    private static CRegister register;
    private static CSmartLabel smartLabel;

    public MyClasses(){
        product = new CProduct();
        shopInfo = new CShopInfo();
        promotion = new CPromotion();
        chat = new CChat();
        shipping = new CShippingSupport();
        privateOffer = new CPrivateOffer();
        salesOrder = new CSalesOrder();
        carrier = new CCarrier();
        blog = new CBlog();
        banner = new CBanner();
        voucher = new CVoucher();
        searchProduct = new CSearchProduct();
        register = new CRegister();
        smartLabel = new CSmartLabel();
    }

    public static void setSmartLabelObject(String object, String value){
        switch (object){
            case "SalesOrder_AddLabelName":
            case "SalesOrder_EditLabelName":
                smartLabel.setLabelName(value);
                break;
            case "SalesOrder_AddLabelColor":
            case "SalesOrder_EditLabelColor":
                smartLabel.setLabelColor(value);
                break;
        }
    }

    public static CSmartLabel getSmartLabelObject(){
        return smartLabel;
    }

    public static void setCarrierObject(String object, String value){
        switch(object){
            case "ecom_shipping_dispatch_cptc_checkbox":
            case "1_checkbox":
                if(value.equals("YES"))
                    carrier.setCptc(true);
                else
                    carrier.setCptc(false);
                break;
            case "ecom_shipping_dispatch_cptk_checkbox":
            case "2_checkbox":
                if(value.equals("YES"))
                    carrier.setCpbk(true);
                else
                    carrier.setCpbk(false);
                break;
            case "ecom_shipping_dispatch_cpn_checkbox":
            case "3_checkbox":
                if(value.equals("YES"))
                    carrier.setCptn(true);
                else
                    carrier.setCptn(false);
                break;
            case "ecom_shipping_dispatch_instant_checkbox":
            case "4_checkbox":
                if(value.equals("YES"))
                    carrier.setCpht(true);
                else
                    carrier.setCpht(false);
                break;
        }
    }

    public static CCarrier getCarrierObject(){
        return carrier;
    }

    public static CRegister getRegisterObject(){ return register; }

    public static void setRegisterObject(String object, String value){
        switch (object){
            case "Register_phoneNumber":
                register.setPhoneNumber(value);
                break;
            case "Register_password":
                register.setPassword(value);
                break;
            case "Register_email":
                register.setEmail(value);
                break;
            case "Register_storeName":
                register.setShopName(value);
                break;
            case "Register_confirmPassword":
                register.setConfirmPassword(value);
                break;
        }
    }

    public static void setProductObject(String object, String value){
        switch (object) {
            case "Product_ProductDetail_Category":
                product.setCatePath(value);
                break;
            case "Product_ProductDetail_CategoryPath":
                product.setCatePath2(value);
                break;
            case "Product_ProductList_ProductName":
            case "Product_ProductDetail_ProductName":
            case "Product_QuickEdit_ProductName":
                product.setProductName(value);
                privateOffer.setProductName(value);
                break;
            case "Product_ProductList_ProductSKU":
            case "Product_ProductDetail_ProductSKU":
            case "Product_QuickEdit_ProductSku":
                product.setProductSku(value);
                break;
            case "Product_ProductDetail_ProductPrice":
            case "Product_QuickEdit_ProductPrice":
            case "Product_ProductVariant_Price":
            case "Product_QuickEditVariant_Price":
                product.setProductPrice(value);
                privateOffer.setOriginalMoney(value);
                break;
            case "Product_ProductDetail_InStock":
            case "Product_ProductList_chkInStock":
            case "Product_QuickEdit_ProductInstock":
                product.setInStock(value);
                break;
            case "Product_ProductDetail_Size":
                product.setProductSize(value);
                break;
            case "Product_ProductDetail_Color":
                product.setProductColor(value);
                break;
            case "Product_ProductList_InStock":
                product.setInStock(value);
                break;
            case "Product_ProductDetail_PromotionPrice":
            case "Product_ProductVariant_SpecialPrice":
            case "Product_QuickEdit_PromotionPrice":
            case "Product_QuickEditVariant_SpecialPrice":
                product.setDiscountMoney(value);
                break;
            case "Product_ProductDetail_StockQuantity":
            case "Product_QuickEdit_StockQuantity":
            case "Product_ProductVariant_StockQuantity":
            case "Product_QuickEditVariant_StockQuantity":
                product.setStockQuantity(value);
                break;
            case "Product_ProductDetail_SaveDraft":
                if(value.equals("YES"))
                    product.setSaveDraft(true);
                /*else
                    product.setSaveDraft(false);*/
                System.out.println(product.getSaveDraft());
                break;
        }
    }

    public static void setProductFromId(String value, String id){
        switch (value) {
            case "PRODUCT":
                product.setProductId(id);
                product.setBuyerUrl(id);
                break;
            case "PROMOTION":
                promotion.setPromotionId(id);
                break;
            case "BLOG":
                blog.setBlogId(id);
        }
    }

    public static CProduct getProductObject(){
        return product;
    }

    public static void setShopInfoObject(String object, String value){
        switch (object){
            case "ShopInfo_ShopName":
                shopInfo.setShopName(value);
                break;
            case "ShopInfo_ShopUrl":
                shopInfo.setShopUrl(value);
                break;
            case "ShopInfo_Slogan":
                shopInfo.setSlogan(value);
                break;
            case "ShopInfo_ShortDescription":
                shopInfo.setShortDescription(value);
                break;
            case "ShopInfo_SellerName":
                shopInfo.setSellerName(value);
                break;
            case "ShopInfo_Phone":
                shopInfo.setPhone(value);
                break;
            case "ShopInfo_Email":
                shopInfo.setEmail(value);
                break;
            case "ShopInfo_TaxCode":
                shopInfo.setTaxCode(value);
                break;
            case "ShopInfo_ShopAddress":
                shopInfo.setShopAddress(value);
                break;
            case "ShopInfo_ShopCity":
                shopInfo.setShopCity(value);
                break;
            case "ShopInfo_ShopDistrict":
                shopInfo.setShopDistrict(value);
                break;
            case "ShopInfo_ShopWard":
                shopInfo.setShopWard(value);
                break;
        }
    }

    public static CShopInfo getShopInfoObject(){
        return shopInfo;
    }

    public static void setPromotionObject(String object, String value){
        switch (object){
            case "pg_Promotion_PromotionName":
                promotion.setPromotionName(value);
                break;
            case "pg_Promotion_DiscountAmount":
                if(Integer.valueOf(value) < 1000){
                    promotion.setDiscountPercent(value);
                    product.setDiscountPercent(value);
                }else{
                    promotion.setDiscountMoney(value);
                    product.setDiscountMoney(value);
                }


                break;
            case "pg_Promotion_PromotionDate":
                promotion.setPromotionDateTmp(value);
                break;
            case "pg_Promotion_PromotionDate1":
                promotion.setPromotionDate(value);
                break;
        }
    }

    public static CPromotion getPromotionObject(){
        return promotion;
    }

    public static void setChatObject(String object, String value){
        switch (object){
            case "Chat_ChatIFrame":
                chat.setUserName(value);
                break;
            case "ChatDetail_ChatTextBox":
                chat.setContentMessage(value);
                chat.setShortMessage(value);
                break;
        }
    }

    public static CChat getChatObject(){
        return chat;
    }

    public static void setShippingSupportObject(String object, String value){
        switch (object){
            case "ShippingSupport_OrderAmount01":
                shipping.setOrderAmount01(value);
                break;
            case "ShippingSupport_SellerSupportFee01":
                shipping.setSupportFee01(value);
                break;
            case "ShippingSupport_Effect01":
                shipping.setEffect01(value);
                break;
            case "ShippingSupport_OrderAmount02":
                shipping.setOrderAmount02(value);
                break;
            case "ShippingSupport_SellerSupportFee02":
                shipping.setSupportFee02(value);
                break;
            case "ShippingSupport_Effect02":
                shipping.setEffect02(value);
                break;
        }
    }
    public static CShippingSupport getShippingSupport(){
        return shipping;
    }

    public static void setPrivateOfferObject(String object, String value){
        //privateOffer.setProductName(product.getProductName());
        //privateOffer.setOriginalMoney(product.getProductPrice());
        switch (object){
            case "PrivateOffer_DiscountAmountPercent":
                privateOffer.setDiscountPercent(value);
                break;
            case "PrivateOffer_DiscountAmountMoney":
                privateOffer.setDiscountMoney(value);
                break;
            case "PrivateOffer_ExpireDate":
                privateOffer.setExpiredDate(value);
                break;
        }
    }

    public static CPrivateOffer getPrivateOfferObject(){
        return privateOffer;
    }

    public static void setSalesOrderObject(String object, String value){
        switch(object){
            case "Buyer_OrderNumber":
            case "SalesOrder_SearchOrder_OrderNumber":
                salesOrder.setOrderNumber(value);
                break;
        }
    }

    public static CSalesOrder getSalesOrderObject(){
        return salesOrder;
    }


    public static CBlog getBlogObject(){
        return blog;
    }

    public static void setBlogObject(String object, String value){
        switch(object){
            case "Blog_Name":
                blog.setName(value);
                break;
            case "Blog_ShortDescription":
                blog.setDescription(value);
                break;
            case "Blog_Tag":
                blog.setTag(value);
                break;
            case "Blog_Content":
                blog.setContent(value);
                break;
        }
    }

    public static CBanner getBannerObject(){
        return banner;
    }

    public static void setBannerObject(String object, String value){
        switch (object){
            case "Banner_SystemURL":
                banner.setSystemURL(value);
                break;
        }
    }

    public static CVoucher getVoucherObject(){
        return voucher;
    }

    public static void setVoucherObject(String object, String value){
        switch (object){
            case "Voucher_VoucherCode":
            case "Voucher_VoucherCodeDisplay":
            case "Voucher_List_Code":
                voucher.setVoucherCode(value);
                break;
            case "Voucher_IsDiscountPercent":
                if(value.equals("YES"))
                    voucher.setDiscountPercent(true);
                else
                    voucher.setDiscountPercent(false);
                break;
            case "Voucher_IsDiscountMoney":
                if(value.equals("YES"))
                    voucher.setDiscountMoney(true);
                else
                    voucher.setDiscountMoney(false);
                break;
            case "Voucher_PercentAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setPercentAmount(Integer.valueOf(value));
                break;
            case "Voucher_MaxVoucherAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setMaxVoucherAmount(Long.valueOf(value));
                break;
            case "Voucher_DiscountAmount":
                if(value.equals("") || value.equals("NO"))
                    value = "0";
                voucher.setDiscountAmount(Long.valueOf(value));
                break;
            case "Voucher_MinOrderAmount":
                if(value.equals(""))
                    value = "0";
                voucher.setMinOrderAmount(Long.valueOf(value));
                break;
            case "Voucher_NumOfUse":
                if(value.equals(""))
                    value = "0";
                voucher.setNumOfUse(Integer.valueOf(value));
                break;
            case "Voucher_StartDate":
                if(value.equals("TOMORROW"))
                    value = FormatUtils.getCurrentDate();
                voucher.setApprovedDate(value);
                break;
            case "Voucher_EndDate":
                if(value.equals("TOMORROW"))
                    value = FormatUtils.getCurrentDate();
                voucher.setExpireDate(value);
                break;
            case "Voucher_Notes":
                voucher.setNotes(value);
                break;
            case "IsAllowShowOnBuyerPage":
                if(value.equals("YES"))
                    voucher.setAllowShowOnBuyerPage(true);
                else
                    voucher.setAllowShowOnBuyerPage(false);
                break;
        }
    }

    public static void setProductVariantObject(String object, String value){
        switch (object){
            case "Product_ProductVariant_AttributeName":
                product.setVariant_AttributeName(value);
                break;
            case "Product_ProductVariant_AttributeValue":
                product.setVariant_AttributeValue(value);
                break;
            case "Product_ProductVariant_Price":
            case "Product_QuickEditVariant_Price":
                product.setVariant_Price(value);
                break;
            case "Product_ProductVariant_SpecialPrice":
            case "Product_QuickEditVariant_SpecialPrice":
                product.setVariant_SpecialPrice(value);
                break;
            case "Product_ProductVariant_PromotionDate":
            case "Product_QuickEditVariant_PromotionDate":
                product.setVariant_PromotionDate(value);
                break;
            case "Product_ProductVariant_StockQuantity":
            case "Product_QuickEditVariant_StockQuantity":
                product.setStockQuantity(value);
            	product.setVariant_StockQuantity(value);
            	break;
            case "Product_ProductVariant_SKU":
                product.setVariant_SKU(value);
                break;
        }
    }

    public static CSearchProduct getSearchProductObject(){
        return searchProduct;
    }

    public static void setSearchProductObject(String object, String value){
        switch (object){
            case "Product_ProductSearch_ProductCategory":
                searchProduct.setProductCategory(value);
                break;
            case "Product_ProductSearch_ProductName":
                searchProduct.setProductName(value);
                break;
            case "Product_ProductSearch_ProductSKU":
                searchProduct.setProductSku(value);
                break;
            case "Product_ProductSearch_ProductStatus":
                searchProduct.setProductStatus(value);
                break;
            case "Product_ProductSearch_ProductStock":
                searchProduct.setProductStock(value);
                break;
            case "Product_ProductSearch_Package":
                searchProduct.setProductPackage(value);
                break;
            case "Product_ProductSearch_ProductDate":
                searchProduct.setProductDate(value);
                break;
        }
    }
}
