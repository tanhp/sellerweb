import constant.MyConstants;
import operation.BaseTest;

import org.testng.annotations.Test;

public class TC_SalesOrder extends BaseTest {

    @Test
    public void C001_CreateNewOrderFromBuyer(){
        performTestCaseUsingExcel("SalesOrder","TC_001");
    }

    @Test
    public void C002_DeclarePrice(){
        performTestCaseUsingExcel("SalesOrder","TC_002");
    }

    @Test
    public void C003_InstantOrder(){
        performTestCaseUsingExcel("SalesOrder", "TC_003");
    }

    @Test
    public void C007_TvcOrder(){
        performTestCaseUsingExcel("SalesOrder", "TC_007");
    }

    @Test
    public void C008_DelayOrder(){
        performTestCaseUsingExcel("SalesOrder", "TC_008");
    }

    //@Test
    public void C010_MergeOrder(){
        if(MyConstants.environment.equals("staging") ){
            performTestCaseUsingExcel("SalesOrder", "TC_010");
        }
    }

    //@Test
    public void C011_SplitOrder(){
        if(MyConstants.environment.equals("staging")) {
            performTestCaseUsingExcel("SalesOrder", "TC_011");
        }
    }

}
