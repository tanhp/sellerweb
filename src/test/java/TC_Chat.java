import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_Chat extends BaseTest{
    @Test
    public void C001_QuickReplyToBuyer(){
        performTestCaseUsingExcel("Chat","TC_001");
    }

    @Test
    public void C002_SendOrderToBuyer(){
        performTestCaseUsingExcel("Chat","TC_002");
    }

    @Test
    public void C003_SendProductToBuyer(){
        performTestCaseUsingExcel("Chat","TC_003");
    }

    //@Test
    public void C004_SendPhotoToBuyer(){
        performTestCaseUsingExcel("Chat","TC_004");
    }

    @Test
    public void C005_SendVoucherToBuyer(){
        performTestCaseUsingExcel("Chat","TC_005");
    }

    @Test
    public void C006_SendMessageToBuyer(){
        performTestCaseUsingExcel("Chat","TC_006");
    }

    //@Test
    public void C007_SendEmoticonToBuyer(){
        performTestCaseUsingExcel("Chat","TC_007");
    }
}
