package others;

import api.APIController;
import org.testng.annotations.Test;
import utils.EmailSender;

public class MobileLogin {
    @Test
    public void login(){
        String source = "https://seller-api-stg.sendo.vn/api/token/mobile";
        String payload = "{\"username\":\"0901239999\",\"password\":\"123456\"}";
        String response = APIController.sendApiRequest("POST", source, payload, "");
        String subject = "Test Mobile Login Staging";
        String to1 = "tanhp@sendo.vn";
        //String to2 = "bichntn4@sendo.vn";
        EmailSender.sendNotificationMail(subject, to1, response, null);
    }
}
