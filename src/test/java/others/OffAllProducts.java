package others;

import operation.ActionGen;
import operation.BaseTest;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import api.APIController;

public class OffAllProducts extends BaseTest{
    WebDriver driver;
    Workbook wb = null;

    @Test
    public void C001_SetOffStockAllProducts() throws IOException{
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("ProductID");
        driver = getDriver();
        flag = false;
        ActionGen.navigateToURL("https://ban.sendo.vn");
        ActionGen.click(By.xpath("html/body/div[1]/header/div/div/p/a"));
        ActionGen.sendKeys(By.name("username"),"sendotest47@gmail.com");
        ActionGen.sendKeys(By.name("password"),"Tan9638527411");
        ActionGen.click(By.xpath("//*[@id=\"loginModal\"]/div/div/div[2]/form/div[3]/button"));
        ActionGen.sleep(5000);
        ActionGen.navigateToURL("https://ban.sendo.vn/shop#product");
        ActionGen.clickByJS(ActionGen.getElement(By.id("closeopup")));
        ActionGen.sleep(5000);
        ActionGen.selectIfOptionTextEquals(By.xpath("//select[contains(@data-bind,'ProductStock')]"),"Hết hàng");
        ActionGen.click(By.xpath("//button[contains(@data-bind,'SearchClick')]"));
        int page = 1;
        int j = 0;
        while(page <= 257) {
        	ActionGen.sleep(5000);
        	System.out.println("Page: " + page);
        	List<WebElement> products = ActionGen.getElements(By.xpath("//a[contains(@data-bind,'ProductName')]"));
        	for(int i=0; i<products.size(); i++) {
        		String id = products.get(i).getAttribute("href").replaceAll("https://ban.sendo.vn/shop#product/detail/", "");
        		Row row = sheet.createRow(j);
                Cell cell = row.createCell(0);
                cell.setCellValue(id);
            	System.out.println(id);
            	String source = "https://seller-api.sendo.vn/api/web/product/";
            	String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6InNlbmRvdGVzdDQ3QGdtYWlsLmNvbSIsIklzQ2VydGlmaWVkIjoiRmFsc2UiLCJJc0xvZ2luRnJvbU1vYmlsZUFwcCI6IkZhbHNlIiwiU2VucGF5SWQiOiIiLCJTZW50U01TIjoiVHJ1ZSIsIlN0b3JlSWQiOiI3Mjk0OCIsIlN0b3JlTGV2ZWwiOiIwIiwiU3RvcmVOYW1lIjoic2hvcGJhYnk0NyIsIlNob3BUeXBlIjoiMCIsIlN0b3JlU3RhdHVzIjoiMiIsIlRva2VuIjoiT2I4K09xUkp1OEJ0MnNsdUdJa0xVSGl3U1FTM3dSUWpmREtzY3h0aUV6Umxwb3dudGdqU3c4em5TVlNwaXg5alN2TXIyemY0Q1pwYjkxYnFzZlJFeHZLTVhYMjJXS0hHVnNxb3Z6c3p2Yk1IZjZZUjJLekNsNFVmSHRJY2tWNUtRcHVKZjlwRmdnY2ZESDdRb3hueUNoVEhyclZjS1hxSXlORVBqdUlKY1lrPSIsIlVzZXJOYW1lIjoiMjAxMzg4MjU4NCIsIlN0b3JlQWNjZXNzIjoie1wiU3RvcmVJZFwiOjcyOTQ4LFwiTmFtZVwiOlwic2hvcGJhYnk0N1wiLFwiU2hvcExvZ29cIjpcImh0dHA6Ly9tZWRpYTMuc2Nkbi52bi9pbWczLzIwMTgvMTJfMTAvVjB1SUgxLmpwZ1wiLFwiSXNPd25lclwiOnRydWV9IiwiZXhwIjoxNTUzMDA3NTU5LCJpc3MiOiJodHRwOi8vc2VsbGVyLWFwaS5zZW5kby52bi8iLCJhdWQiOiJodHRwOi8vc2VsbGVyLWFwaS5zZW5kby52bi8ifQ.e6OlzcAETWSW01JNs-QvMZ-_-kT-BW2CQoRHmW924aM";
            	String payload = "[" + id + "]";
            	//APIController.postWithAuth(source, payload, token);
            	ActionGen.sleep(2000);
            	j++;
        	}
        	
            page++;
        	ActionGen.click(By.xpath("//a[contains(@data-bind,'UserChangePage') and text()='" + page +"']"));
        }
        try {
            FileOutputStream out = new FileOutputStream("ProductID.xlsx");
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
