package others;

import constant.MyConstants;
import operation.BaseTest;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Movie{
    WebDriver driver;
    Workbook wb = null;
    @Test
    public void C001_SetOffStockAllProducts() throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", MyConstants.getChromeDriverPath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        for(int i = 55; i<=69; i++){
            Workbook wb = new XSSFWorkbook();
            driver.navigate().to("http://www.studyphim.vn/movies/search?category=single&page=" + i);
            Thread.sleep(1000);
            List<WebElement> movies1 = driver.findElements(By.className("movie-title"));
            System.out.println("Movie size: " + movies1.size());
            for(int j = 0; j < movies1.size(); j++){
                List<WebElement> movies = driver.findElements(By.className("movie-title"));
                String movie_name = movies.get(j).findElement(By.tagName("strong")).getText().replace(":","").replace("?","").replace("]","").replace("[","").replace("!","").replace("/","");
                String movie_url = movies.get(j).findElement(By.tagName("a")).getAttribute("href") + "/play?episode=1";
                System.out.println("Movie name: " + movie_name);
                System.out.println("Movie url: " + movie_url);

                driver.navigate().to(movies.get(j).findElement(By.tagName("a")).getAttribute("href") + "/play?episode=1");
                Sheet sheet;
                try {
                    sheet = wb.createSheet(movie_name);
                }catch(Exception e){
                    continue;
                }
                Thread.sleep(3000);

                List<WebElement> sentences = driver.findElements(By.xpath("//span[contains(@id,'cue')]"));
                System.out.println(sentences.size());
                for(int k=1; k<=sentences.size(); k++){
                    String locator = "cue" + k;
                    String all_text = driver.findElement(By.id(locator)).getText();
                    String child_text = driver.findElement(By.id(locator)).findElement(By.tagName("small")).getText();
                    Row row = sheet.createRow(k);
                    Cell cell1 = row.createCell(0);
                    Cell cell2 = row.createCell(1);
                    cell1.setCellValue(all_text.replace(child_text,""));
                    System.out.println(all_text.replace(child_text,""));
                    cell2.setCellValue(child_text);
                    System.out.println(child_text);
                }
                try {
                    FileOutputStream out = new FileOutputStream("SubViet_" + i + ".xlsx");
                    wb.write(out);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                driver.navigate().to("http://www.studyphim.vn/movies/search?category=single&page=" + i);
                driver.navigate().refresh();
                Thread.sleep(3000);
            }
            wb = null;
            Thread.sleep(1000);
        }


        /*driver.navigate().to("http://www.studyphim.vn/movies/the-lion-king-3-hakuna-matata-2004/play?episode=1");
        Thread.sleep(3000);
        List<WebElement> sentences = driver.findElements(By.xpath("//span[contains(@id,'cue')]"));
        System.out.println(sentences.size());
        for(int i=1; i<=sentences.size(); i++){
            String locator = "cue" + i;
            String all_text = driver.findElement(By.id(locator)).getText();
            String child_text = driver.findElement(By.id(locator)).findElement(By.tagName("small")).getText();
            Row row = sheet.createRow(i);
            Cell cell1 = row.createCell(0);
            Cell cell2 = row.createCell(1);
            cell1.setCellValue(all_text.replace(child_text,""));
            System.out.println(all_text.replace(child_text,""));
            cell2.setCellValue(child_text);
            System.out.println(child_text);
        }
        try {
            FileOutputStream out = new FileOutputStream("SubViet.xlsx");
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }



}
