package others;

import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;
import constant.MyConstants;
import constant.SendoAccount;
import logger.MyLogger;

import operation.ActionWeb;
import operation.BaseTest;
import operation.ParallelTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import utils.EmailSender;
import utils.FormatUtils;
import utils.GoogleHangouts;
import utils.SkypeSender;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Created by TanHP on 7/19/2018.
 */
public class Example{
    //@Test
    public void testManual(){
        System.out.println("ID is: " + System.getProperty("manualExecution"));
        // get system properties
        Properties properties = System.getProperties();
        Set set = properties.entrySet();
        // show system properties
        Iterator itr = set.iterator();
        while (itr.hasNext()) {
            Map.Entry entry = (Map.Entry) itr.next();
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }

    }

	//@Test
	public void testLog() {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
		driver = new FirefoxDriver();
		driver.get("https://google.com");
	}
	//@Test
    public void testEmail(){
        List<String> files = new ArrayList<>();
        files.add("D:\\HoPhamTan_SMC_New.xlsx");
        
        String[] emails = {"tanhopham1990@gmail.com"};
        EmailSender.sendFromGMail("tester01gm@gmail.com", "12345678Ac", emails, "Test Subject", "Test body");
        //EmailSender.sendNotificationMail("Test Email Subject","tanhopham1990@gmail.com,tanhp@sendo.vn","Test Email content",files);
    }
	
    //@Test
    public void Calculate(){
        float dis_percent = Long.valueOf("120000")*100
                / Long.valueOf("500000");
        System.out.println(String.valueOf(dis_percent).replace(".0", ""));
    }

    //@Test
    public void RunCommandLine() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("platformName","Android");
        caps.setCapability("platformVersion","4.4.4");
        caps.setCapability("deviceName","192.168.248.101:5555");
        caps.setCapability("browserName","Chrome");
        RemoteWebDriver driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);


    }

    //@Test
    public void testSendSkypeMessage(){
        Message skypeMessage = Message.create()
                .with(Text.rich("Seller automation report " + FormatUtils.getToday() + ": http://172.30.119.110:81/smc/output/FinalResults.xlsx"));
        SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
    }

    //@Test
    public void testSendSkypeFile(){
        File file = new File("D:\\Test.txt");
    }

    //@Test
    public void testEnum(){
        System.out.println(SendoAccount.BUYER_USERNAME);
        System.out.println(SendoAccount.BUYER_PASSWORD);
    }

    @Test
    public void testEmail2(){
	    String subject = "Test subject";
	    String to = "tanhp@sendo.vn";
	    String message = "Test message";
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append("*Tan dep trai!");

        com.google.api.services.chat.v1.model.Message hangoutsMessage = new com.google.api.services.chat.v1.model.Message();
        hangoutsMessage.setText(messageBuilder.toString());
        GoogleHangouts.sendHangoutsMessage("AAAAOye9IqE",hangoutsMessage);
    }

    public FirefoxOptions getFireFoxOptions(){
        FirefoxOptions ffOptions = new FirefoxOptions();
        ffOptions.addPreference("browser.download.manager.focusWhenStarting", true);
        ffOptions.addPreference("browser.download.useDownloadDir", false);
        ffOptions.addPreference("browser.helperApps.alwaysAsk.force", true);
        ffOptions.addPreference("browser.download.manager.alertOnEXEOpen", false);
        ffOptions.addPreference("browser.download.manager.closeWhenDone", true);
        ffOptions.addPreference("browser.download.manager.showAlertOnComplete", false);
        ffOptions.addPreference("browser.download.manager.useWindow", false);
        ffOptions.addPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
        return ffOptions;
    }

    public ChromeOptions getChromeOptions(){
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-extensions");
        if(System.getProperty("os.name").startsWith("Linux")){
            options.addArguments("--headless");
            options.addArguments("window-size=1400,900");
        }
        options.addArguments("--start-maximized");
        options.addArguments("disable-infobars"); // disabling infobars
        options.addArguments("--disable-extensions"); // disabling extensions
        options.addArguments("--disable-gpu"); // applicable to windows os only
        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        options.addArguments("--no-sandbox"); // Bypass OS security model
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("useAutomationExtension", false);
        return options;
    }

    public void setPermissionFileOnLinux(){
        try{
            String command = "chmod +x " + MyConstants.getChromeDriverPath();
            Runtime.getRuntime().exec(command);
        }catch (Exception e){

        }
    }

    public WebDriver initDriver(String browser){
        WebDriver driver = null;
        MyLogger.info("Start to launch browser: " + browser);
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", MyConstants.FIREFOX_DRIVER_PATH);
                driver = new FirefoxDriver(getFireFoxOptions());
                driver.manage().window().maximize();
                break;
            case "chrome":
                if(System.getProperty("os.name").startsWith("Linux"))
                    setPermissionFileOnLinux();
                System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
                driver = new ChromeDriver(getChromeOptions());
                break;
            case "iexplorer":
                System.setProperty("webdriver.ie.driver",MyConstants.IE_DRIVER_PATH);
                driver = new InternetExplorerDriver();
                break;
        }
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    @Test
    public void testEmail3() throws IOException, InterruptedException {
        ActionWeb.verifyOnGmail();
    }
}
