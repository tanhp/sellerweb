import constant.MyConstants;
import operation.BaseTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import utils.FormatUtils;
import utils.XLSWorker;

public class Example{

    @Test
    public void test(){
        String a = "Hello shop!";
        String b = "Hello shop!" +
                " 1 phút trước";
        System.out.println(b.contains(a));
    }

    @Test
    public void ABC(){
        XSSFWorkbook workbook = XLSWorker.getWorkbook("D:\\Sendo Auto Source\\Seller\\seller api 2\\input\\data.xlsx");
        XSSFSheet sheet = XLSWorker.getSheet(workbook,"CreateShop");
        for(int i = 0; i<sheet.getLastRowNum(); i++){
            String data = sheet.getRow(i).getCell(0).toString();
            data = FormatUtils.matchAndReplaceNonEnglishChar(data).replace(" ","");
            System.out.println(data);
        }
    }
    @Test
    public void C001_AddBannerWithValidURL(){
        WebDriver driver;
    	try {
            System.setProperty("webdriver.chrome.driver",MyConstants.getChromeDriverPath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-extensions");
            options.addArguments("--start-maximized");
            driver = new ChromeDriver(options);
            Actions builder = new Actions(driver);

            driver.get("https://lazada.vn");

            List<WebElement> menus = driver.findElements(By.xpath("//li[@class='lzd-site-menu-root-item']/a/span"));
            for(int i=0; i<menus.size(); i++){
                System.out.println(menus.get(i).getText());
                builder.moveToElement(menus.get(i)).build().perform();
                List<WebElement> sub1_1 = driver.findElements(By.xpath("//li[@class='sub-item-remove-arrow' and contains(@data-cate,'cate_1')]/a/span"));
                for(int j=0; j<sub1_1.size(); j++){
                    System.out.println(sub1_1.get(i).getText());
                    builder.moveToElement(menus.get(i)).build().perform();
                }
                List<WebElement> sub1_2 = driver.findElements(By.xpath("//li[@class='lzd-site-menu-sub-item']/a/span"));
                for(int j=0; j<sub1_2.size(); j++){
                    System.out.println(sub1_2.get(i).getText());
                    builder.moveToElement(menus.get(i)).build().perform();
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
