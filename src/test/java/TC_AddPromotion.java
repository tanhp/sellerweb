import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_AddPromotion extends BaseTest {
    @Test
    public void C001_AddPromotionWithoutVariantConfig(){
        performTestCaseUsingExcel("AddPromotion","TC_001");
    }

    @Test
    public void C002_AddPromotionWithVariantConfig(){
        performTestCaseUsingExcel("GiaTonKho_Promotion","TC_001");
    }
}
