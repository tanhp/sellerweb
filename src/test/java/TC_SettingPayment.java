import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SettingPayment extends BaseTest {
    @Test
    public void C001_TestSettingPayment(){
        performTestCaseUsingExcel("SettingPayment","TC_001");
    }

    @Test
    public void C002_TestSettingPayment(){
        performTestCaseUsingExcel("SettingPayment","TC_002");
    }
}
