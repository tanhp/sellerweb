import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SearchOrder extends BaseTest {
    @Test
    public void C001_SearchByOrderNumber(){
        performTestCaseUsingExcel("SearchOrder","TC_001");
    }

    @Test
    public void C002_SearchByOrderStatus(){
        performTestCaseUsingExcel("SearchOrder","TC_002");
    }
}
