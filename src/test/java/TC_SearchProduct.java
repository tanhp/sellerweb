import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SearchProduct extends BaseTest {
    @Test
    public void C001_SearchByName(){
        performTestCaseUsingExcel("SearchProduct","TC_001");
    }
    @Test
    public void C002_SearchBySku(){
        performTestCaseUsingExcel("SearchProduct","TC_002");
    }

    @Test
    public void C003_SearchByStatus(){
        performTestCaseUsingExcel("SearchProduct","TC_003");
    }

    @Test
    public void C004_SearchByStock(){
        performTestCaseUsingExcel("SearchProduct","TC_004");
    }

    @Test
    public void C005_SearchByPackage(){
        performTestCaseUsingExcel("SearchProduct","TC_005");
    }

    @Test
    public void C006_SearchByCategory(){
        performTestCaseUsingExcel("SearchProduct","TC_006");
    }

    @Test
    public void C007_SearchByDateRange(){
        performTestCaseUsingExcel("SearchProduct","TC_007");
    }

}
