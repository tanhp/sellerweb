import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_DeleteProduct extends BaseTest {
    //@Test
    public void C001_DeleteProduct(){
        performTestCaseUsingExcel("DeleteProduct","TC_001");
    }

    //@Test
    public void C002_DeleteMultiProducts(){
        performTestCaseUsingExcel("DeleteProduct","TC_002");
    }
}
