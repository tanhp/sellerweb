import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_PrivateOffer extends BaseTest{

    @Test(groups = { "uat" })
    public void C001_SendPrivateOfferByPercent(){
        performTestCaseUsingExcel("PrivateOffer","TC_001");
    }

    //@Test(groups = { "uat" })
    public void C002_SendPrivateOfferByPrice(){
        performTestCaseUsingExcel("PrivateOffer","TC_002");
    }
}
