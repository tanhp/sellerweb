import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_EditPromotion extends BaseTest {
    @Test
    public void C001_CreatePromotionFromEditProduct(){
        performTestCaseUsingExcel("EditPromotion","TC_001");
    }

    @Test
    public void C002_ClearPromotionFromEditProduct(){
        performTestCaseUsingExcel("EditPromotion","TC_002");
    }

    @Test
    public void C003_CreatePromotionWithVariant(){
        performTestCaseUsingExcel("GiaTonKho_EditPromotion","TC_001");
    }

    @Test
    public void C004_ClearPromotionWithVariant(){
        performTestCaseUsingExcel("GiaTonKho_EditPromotion","TC_002");
    }
}
