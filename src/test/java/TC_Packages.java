import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_Packages extends BaseTest {
    //@Test
    public void C001_UsingInDay(){
        performTestCaseUsingExcel("Packages","TC_001");
    }

    @Test
    public void C002_UsingInstant(){
        performTestCaseUsingExcel("Packages","TC_002");
    }

    @Test
    public void C003_UsingSelfDelivery(){
        performTestCaseUsingExcel("Packages","TC_003");
    }
}
