import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_InStock extends BaseTest{

    @Test
    public void C001_SwitchToInStock_OldProduct(){
        performTestCaseUsingExcel("InStock","TC_001");
    }

    @Test
    public void C002_SwitchToOutOfStock_OldProduct(){
        performTestCaseUsingExcel("InStock","TC_002");
    }

    @Test
    public void C003_SwitchOffNoVariant_OldProduct(){
        performTestCaseUsingExcel("InStock","TC_003");
    }

    @Test
    public void C004_SwitchOffVariant_OldProduct(){
        performTestCaseUsingExcel("InStock","TC_004");
    }
}
