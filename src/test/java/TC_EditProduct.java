import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_EditProduct extends BaseTest {
    @Test
    public void C001_EditProductWithoutVariantConfig(){
        performTestCaseUsingExcel("EditProduct","TC_001");
    }

    @Test
    public void C002_EditProductWithoutVariantConfig_OldProduct(){
        performTestCaseUsingExcel("EditProduct","TC_002");
    }

    @Test
    public void C003_EditProductWithVariantConfig(){
        performTestCaseUsingExcel("GiaTonKho_EditProduct","TC_001");
    }

    @Test
    public void C004_EditProductWithVariantConfig_OldProduct(){
        performTestCaseUsingExcel("GiaTonKho_EditProduct","TC_002");
    }
}
