package autotestng;

import com.samczsun.skype4j.formatting.Message;
import com.samczsun.skype4j.formatting.Text;
import constant.MyConstants;
import logger.MyLogger;

import operation.ParallelTest;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import utils.*;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AutoTestNG {
    TestNG testng;
    List<String>sheetRefs = new ArrayList<String>();
	List<Integer>rows = new ArrayList<Integer>();
	public static List<String>FSheets = new ArrayList<String>();
	public static List<Integer>FRows = new ArrayList<Integer>();
	public static List<Integer>FCols = new ArrayList<Integer>();
	public static List<String>FUrl = new ArrayList<String>();
	public static XSSFWorkbook wbHeader = null;
    String currentDate = FormatUtils.getCurrentTimeByTimezoneOffset(MyConstants.timezoneOffset, "yyyy-MM-dd");

	
	private void runTestSuite(String xmlFile, int row, String sheetRef){
		try{
			testng = new TestNG();
			String testngSuite = System.getProperty("user.dir")+"/testsuites/" + xmlFile;
			Thread.sleep(5000);
			testng.setTestSuites(Arrays.asList(testngSuite));
			testng.run();
			rows.add(row);
			sheetRefs.add(sheetRef);
		}catch (Exception e){
			e.printStackTrace();
		}

	}
	
	public void sendReportToSkype(String fileUrl) {
		 Message skypeMessage = Message.create()
                 .with(Text.rich("Seller automation report " + FormatUtils.getToday() + ": " + fileUrl));
         SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_TAN, skypeMessage, 1);
         SkypeSender.sendSkypeMessage(MyConstants.SKYPE_USERNAME, MyConstants.SKYPE_PASSWORD, MyConstants.SKYPE_IDENTITY_BICH, skypeMessage, 1);
	}

	public void sendReportToSlack(String fileUrl){
		String slackMessage = "Seller automation report " + FormatUtils.getToday() + ": " + fileUrl;
		SlackSender.pushNotifyToSlack("#qc-team","Auto Bot", slackMessage);
	}

	//@Test
	public void updateSmokeTest(){
		String filePath = MyConstants.OUTPUT_PATH + "FinalResults.xlsx";
		XSSFWorkbook wbSmokeTest = XLSWorker.getWorkbook(MyConstants.SMOKETEST_FILE);
		XSSFSheet sheetSmokeTest = XLSWorker.getSheet(wbSmokeTest, "Seller web");
		XSSFWorkbook wbFileResult = XLSWorker.getWorkbook(filePath);
		for(int i=3; i<wbFileResult.getNumberOfSheets(); i++){
			XSSFSheet sheetFileResult = wbFileResult.getSheetAt(i);
			MyLogger.info("SheetName: " + sheetFileResult.getSheetName());
			for(int j=2 ; j <= sheetFileResult.getLastRowNum(); j++){
				String errorMessage = "";
				try{
					String lstTmid = XLSWorker.getValueFromExcel(wbFileResult, j, "TMID", sheetFileResult.getSheetName());
					MyLogger.info("LSTTMID: " + lstTmid);
					String[] tmids = lstTmid.split(", ");
					for(int m=0; m<tmids.length; m++){
						MyLogger.info("TMID: " + tmids[m]);
						String result = XLSWorker.getValueFromExcel(wbFileResult, j, "Result", sheetFileResult.getSheetName());
						MyLogger.info("Result: " + result);

						if(result.equals("PASSED")){
							result = "Passed";
						}else if(result.contains("FAILED")){
							errorMessage = result;
							result = "Failed";
						}

						if(result!=null && !result.equals("")){
							for(int k=8 ; k<=sheetSmokeTest.getLastRowNum(); k++){
								String tmidST = sheetSmokeTest.getRow(k).getCell(5).toString().replace(".0","");
								MyLogger.info("TMIDST: " + tmidST);
								if(tmids[m] != null && !tmidST.equals("") && tmids[m].equals(tmidST)){
									MyLogger.info("Set result: " + result + " to TMID: " + tmids[m] + "->" + tmidST + " of sheetName: " + sheetFileResult.getSheetName());
									sheetSmokeTest.getRow(k).getCell(13).setCellValue(result);
									if(result.equals("Failed")){
										String screenShot = XLSWorker.getValueFromExcel(wbFileResult, j, "ScreenShot", sheetFileResult.getSheetName());
										sheetSmokeTest.getRow(k).getCell(17).setCellValue(errorMessage);
										sheetSmokeTest.getRow(k).getCell(18).setCellValue(screenShot);
									}
								}

							}
						}
					}
				}catch (Exception e){
					MyLogger.error2("Have error with TMID or Result in sheet: " + sheetFileResult.getSheetName());
				}
			}
		}
		XLSWorker.writeExcel(wbSmokeTest, MyConstants.OUTPUT_PATH + "SmokeTestResult.xlsx");
	}

	public void updatePercentIntoDB(float percent){
		Connection connect;
		Statement statement;
		try{
			connect = DriverManager.getConnection("jdbc:mysql://127.0.0.1", "root", "");
			statement = connect.createStatement();
			statement.execute("USE test");
			String sql = "UPDATE " + "seller"
					+ " SET " + "percent" + "='" + percent + "'"
					+ " WHERE 1;";
			statement.executeUpdate(sql);
		}catch (Exception e){
			e.printStackTrace();
		}

	}

    @Test
    public void runAutoTestNG(){
	    long startTime = System.currentTimeMillis();
		XSSFWorkbook workbook = XLSWorker.getWorkbook(MyConstants.HEADER_FILE);
		XSSFSheet sheet = XLSWorker.getSheet(workbook,"Header");
		for(int i = 1; i <= sheet.getLastRowNum(); i++){
			wbHeader = ParallelTest.wbHeader;
			String xmlFile = sheet.getRow(i).getCell(3).getStringCellValue();
			String sheetRef = sheet.getRow(i).getCell(4).getStringCellValue();
			String isRun = sheet.getRow(i).getCell(MyConstants.getRunIndex()).getStringCellValue();
			System.out.println(isRun);
			if(!xmlFile.equals("") && isRun.equals("Run")){
				System.out.println("XML File: " + xmlFile);
				System.out.println("Sheet Ref: " + sheetRef);
				runTestSuite(xmlFile,i,sheetRef);
				if(System.getProperty("os.name").startsWith("Windows")){
					float percent = (float) i*100/sheet.getLastRowNum();
					System.out.println("Update percent: " + percent);
					//updatePercentIntoDB(percent);
				}
			}
		}
		if(System.getProperty("os.name").startsWith("Windows")){
			//updatePercentIntoDB((float) 100);
		}
		String filePath = MyConstants.OUTPUT_PATH + "FinalResults.xlsx";
		String smokeTest = MyConstants.OUTPUT_PATH + "SmokeTestResult.xlsx";
		String logPath = MyConstants.LOG_PATH + "SellerTestLog.log";
		try {
        	File file = new File(filePath);
        	ExcelMerger.mergeExcelFiles(file);
            HyperlinkWorker.setUp(filePath, "Header");
        	HyperlinkWorker.cellStyleForHyperlink();
        	for(int i = 0; i<sheetRefs.size(); i++) {
        		HyperlinkWorker.setLinkToSheet(rows.get(i), 5, sheetRefs.get(i));
        	}
        	HyperlinkWorker.tearDown(filePath);
        	/*FSheets = XLSWorker.FSheets;
        	FRows = XLSWorker.FRows;
        	FCols = XLSWorker.FCols;
        	FUrl = XLSWorker.FUrl;
        	MyLogger.info("Setting failed screenshots to testcase...");
        	for(int i = 0; i < FSheets.size(); i++) {
        		if(FUrl.get(i) != null) {
					System.out.println(FUrl.get(i));
					System.out.println(FRows.get(i));
					System.out.println(FCols.get(i));
					HyperlinkWorker.setUp(filePath, FSheets.get(i));
                	HyperlinkWorker.cellStyleForHyperlink();
                	HyperlinkWorker.setUrlLink(FRows.get(i), FCols.get(i), FUrl.get(i));
                    HyperlinkWorker.tearDown(filePath);
        		}
        	}*/

        	updateSmokeTest();

        } catch (Exception e) {
            // TODO Auto-generated catch block
			MyLogger.error2(e.getMessage());
            e.printStackTrace();
        }finally {

			// Upload excel result file to google drive
			System.out.println("Smoketest file: " + smokeTest);
			String fileUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadExcelFile(filePath);
			String smokeUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadExcelFile(smokeTest);

			GoogleDrive.setExcelPermission();
			if(true){
				MyLogger.info("Send report result via Email with file: " + fileUrl);
				String subject = "Daily Automation Seller - " + FormatUtils.getToday();
				String to1 = "tanhp@sendo.vn";
				String to2 = "bichntn4@sendo.vn";
				String emailMessage;
				if(MyConstants.environment.equals("pro"))
					emailMessage = "[PRODUCTION] Seller automation report " + FormatUtils.getToday() + ": " + smokeUrl;
				else
					emailMessage = "[STAGING] Seller automation report " + FormatUtils.getToday() + ": " + smokeUrl;
				EmailSender.sendNotificationMail(subject,to1,emailMessage,null);
				EmailSender.sendNotificationMail(subject,to2,emailMessage,null);

				//sendReportToSlack(fileUrl);
			}

			String logUrl = "https://drive.google.com/open?id=" + GoogleDrive.uploadLog(logPath);
			String subject = "Daily Automation Seller - Log -  " + FormatUtils.getToday();
			String to1 = "tanhp@sendo.vn";
			String emailMessage = "Seller automation log " + FormatUtils.getToday() + ": " + logUrl + " " + fileUrl;
			EmailSender.sendNotificationMail(subject,to1,emailMessage,null);
            long endTime = System.currentTimeMillis();
            long timeElapsed = endTime - startTime;
            MyLogger.info("Time Elapsed: " + timeElapsed);
		}
    }

    //@Test
    public void runAutoTestNG2(){
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        List<XmlClass> classes1 = new ArrayList<XmlClass>();
        List<XmlClass> classes2 = new ArrayList<XmlClass>();
        List<Class> listenerClasses = new ArrayList<Class>();

        XmlSuite suite1 = new XmlSuite();
        suite1.setName("LoginSuite");
        XmlTest test1 = new XmlTest(suite1);
        test1.setName("ProgramTest");
        XmlClass class1 = new XmlClass("login.LoginFromSeller");
        classes1.add(class1);
        /*XmlClass class2 = new XmlClass("SampleProgram2");
        classes.add(class2);*/
        test1.setXmlClasses(classes1);
        suites.add(suite1);



       /* XmlSuite suite2 = new XmlSuite();
        suite2.setName("ProgramSuite2");
        XmlTest test2 = new XmlTest(suite2);
        test2.setName("ProgramTest2");
        XmlClass class2 = new XmlClass("chat.SellerChat");
        classes2.add(class2);
        test2.setXmlClasses(classes2);
        suites.add(suite2);*/

        // Create TestNg and execute suite
        TestNG tng = new TestNG();
        tng.setXmlSuites(suites);
        tng.setListenerClasses(listenerClasses);
        tng.run();

        try {
            XLSWorker.mergeExcelFiles(new File(MyConstants.OUTPUT_PATH + "FinalResults.xlsx"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
