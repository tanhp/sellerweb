import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_AddVoucher extends BaseTest {
    @Test
    public void C001_AddVoucherByPercent(){
        performTestCaseUsingExcel("Voucher","TC_001");
    }

    @Test
    public void C002_AddVoucherByMoney(){
        performTestCaseUsingExcel("Voucher","TC_002");
    }

    @Test
    public void EditVoucher01(){
        performTestCaseUsingExcel("EditVoucher", "TC_001");
    }

    @Test
    public void CloseVoucher(){
        performTestCaseUsingExcel("CloseVoucher", "TC_001");
    }

    //@Test
    public void DeactiveVoucher(){
        performTestCaseUsingExcel("DeactiveVoucher", "TC_001");
    }
}
