import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_PrintBill extends BaseTest {
    @Test
    public void C001_PrintOneBillNormalShop(){
        performTestCaseUsingExcel("PrintBill","TC_001");
    }

    @Test
    public void C002_PrintOneBillShopMall(){
        performTestCaseUsingExcel("PrintBill","TC_002");
    }

    //@Test
    public void C003_PrintMultiBill(){
        performTestCaseUsingExcel("PrintBill","TC_002");
    }
}
