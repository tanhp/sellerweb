import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SenpaySupport extends BaseTest {
    @Test
    public void C001_TestSenpaySupport(){
        performTestCaseUsingExcel("SenpaySupport","TC_001");
    }

    @Test
    public void C002_TestSenpaySupport(){
        performTestCaseUsingExcel("SenpaySupport","TC_002");
    }
}
