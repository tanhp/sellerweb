import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SortingCenter extends BaseTest {
    @Test
    public void C001_CheckShopSorting(){
        performTestCaseUsingExcel("Sorting Center","TC_001");
    }

    @Test
    public void C002_CheckShopSendo(){
        performTestCaseUsingExcel("Sorting Center","TC_002");
    }
}
