import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_Login extends BaseTest {

    @Test
    public void C006_LoginWithEmail_NormalShop(){
        performTestCaseUsingExcel("Login","TC_006");
    }
    
    @Test
    public void C007_LoginWithPhoneNumber_NormalShop(){
        performTestCaseUsingExcel("Login","TC_007");
    } 
    
    @Test
    public void C008_LoginWithEmail_SenmallShop(){
        performTestCaseUsingExcel("Login","TC_009");
    } 
    
    @Test
    public void C009_LoginWithPhoneNumber_SenmallShop(){
        performTestCaseUsingExcel("Login","TC_008");
    } 
    
    //@Test
    public void C010_LoginWithAccountNoExists(){
        performTestCaseUsingExcel("LoginNew","TC_001");
    }

}
