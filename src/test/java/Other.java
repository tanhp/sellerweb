import operation.ActionGen;
import operation.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Other extends BaseTest {

    @Test
    public void checkAppOnCHPlay(){
        ActionGen.navigateToURL("https://play.google.com/store/apps/details?id=com.sendoseller");
        WebElement element = ActionGen.getElement(By.xpath("//h1[@itemprop]/span"));
        if(!element.getText().equals("Sendo Bán"))
            ActionGen.logFail("App does not exist!");
    }
}
