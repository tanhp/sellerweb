import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_Carrier extends BaseTest {
    //@Test
    public void C001_UncheckAllCarriers(){
        performTestCaseUsingExcel("Carrier","TC_001");
    }

    //@Test
    public void C002_CheckLessThan3Carriers(){
        performTestCaseUsingExcel("Carrier","TC_002");
    }

    @Test
    public void C003_CheckAllCarriers(){
        performTestCaseUsingExcel("Carrier","TC_003");
    }

    @Test
    public void C004_CheckAllCarriers_SC(){
        performTestCaseUsingExcel("Carrier","TC_004");
    }
}
