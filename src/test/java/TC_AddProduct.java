import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_AddProduct extends BaseTest{
    @Test
    public void C001_AddNewProductWithoutCheckoutConfig(){
        performTestCaseUsingExcel("AddProduct","TC_001");
    }

    @Test
    public void C002_AddNewProductWithSubmitAndCreate(){
        performTestCaseUsingExcel("AddProduct","TC_002");
    }

    @Test
    public void C003_CopyProductWithoutCheckoutConfig(){
        performTestCaseUsingExcel("AddProduct","TC_003");
    }

    //@Test
    public void C004_AddSenmallProduct(){
        performTestCaseUsingExcel("AddProduct", "TC_004");
    }

    //@Test
    public void C005_AddRiskProduct(){
        performTestCaseUsingExcel("AddProduct", "TC_005");
    }

    @Test
    public void C006_CloneProduct(){
        performTestCaseUsingExcel("AddProduct", "TC_008");
    }

    @Test
    public void C007_SaveDraft(){
        performTestCaseUsingExcel("AddProduct", "TC_009");
    }

    @Test
    public void C008_AddTravelVoucher(){
        performTestCaseUsingExcel("AddProduct", "TC_006");
    }

    @Test
    public void C009_AddTravelEVoucher(){
        performTestCaseUsingExcel("AddProduct", "TC_007");
    }

    @Test
    public void C010_AddNewProductWithVariantConfig(){
        performTestCaseUsingExcel("GiaTonKho_AddProduct","TC_001");
    }

    @Test
    public void C011_AddNewProductWithCheckoutCate(){
        performTestCaseUsingExcel("GiaTonKho_AddProduct","TC_002");
    }

    @Test
    public void C012_CopyProductWithVarriantConfig(){
        performTestCaseUsingExcel("GiaTonKho_AddProduct","TC_003");
    }



}
