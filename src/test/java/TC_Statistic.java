import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_Statistic extends BaseTest {
    //@Test
    public void C001_VerifyPercentComplete(){
        performTestCaseUsingExcel("Statistic","TC_001");
    }

    //@Test
    public void C002_VerifyPercentOrderClaim(){
        performTestCaseUsingExcel("Statistic","TC_002");
    }
}
