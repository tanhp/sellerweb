import operation.BaseTest;
import org.testng.annotations.Test;

public class  TC_QuickEdit extends BaseTest {
    @Test
    public void C001_QuickEditWithoutVariantConfig(){
        performTestCaseUsingExcel("QuickEdit","TC_001");
    }

    @Test
    public void C002_QuickEditWithoutVariantConfig_OldProduct(){
        performTestCaseUsingExcel("QuickEdit","TC_002");
    }

    @Test
    public void C003_QuickEditWithVariantConfig(){
        performTestCaseUsingExcel("GiaTonKho_QuickEdit","TC_001");
    }

    @Test
    public void C004_QuickEditWithVariantConfig_OldProduct(){
        performTestCaseUsingExcel("GiaTonKho_QuickEdit","TC_002");

    }
}
