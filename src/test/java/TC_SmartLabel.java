import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_SmartLabel extends BaseTest {
    @Test
    public void C001_AddLabel(){
        performTestCaseUsingExcel("AddLabel","TC_001");
    }

    @Test
    public void C002_EditLabel(){
        performTestCaseUsingExcel("EditLabel","TC_001");
    }
}
