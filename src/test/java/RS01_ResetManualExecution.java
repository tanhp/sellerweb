import database.Database2;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RS01_ResetManualExecution {

    @Test
    public void RS01_01_ResetManualExecution(){
        String manualTestExecutionIDList = System.getProperty("manualExecution");
        String[] manualTestExecutionID = manualTestExecutionIDList.split(",");
        for(String item : manualTestExecutionID) {
            List<String> conditionValues = new ArrayList<String>();
            conditionValues.add("manualExecution , " + item);
            String firstStatus = Database2.getValueFromSelectedColumnMultiCondition("automationexecutions", "firstRun",conditionValues);
            String secondStatus = Database2.getValueFromSelectedColumnMultiCondition("automationexecutions", "secondRun",conditionValues);
            if(firstStatus.equals("QUEUED") || firstStatus.equals("RUNNING")) {
                conditionValues = new ArrayList<String>();
                List<String> values = new ArrayList<String>();
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                values.add("firstRun , READY");
                values.add("secondRun , READY");
                values.add("finalResult , READY");
                Database2.updateDatabase("automationexecutions", values, conditionValues);

            }else if(firstStatus.equals("FAILED") && secondStatus.equals("QUEUED") || firstStatus.equals("FAILED") && secondStatus.equals("RUNNING")){
                conditionValues = new ArrayList<String>();
                List<String> values = new ArrayList<String>();
                conditionValues = new ArrayList<String>();
                conditionValues.add("manualExecution , " + item);
                values.add("firstRun , READY");
                values.add("secondRun , READY");
                values.add("finalResult , READY");
                Database2.updateDatabase("automationexecutions", values, conditionValues);
            }
        }
    }
}
