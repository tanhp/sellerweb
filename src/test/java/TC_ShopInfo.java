import operation.BaseTest;
import org.testng.annotations.Test;

public class TC_ShopInfo extends BaseTest{
    @Test(groups = { "uat" })
    public void C001_ChangeShopInfoWithValidData(){
        performTestCaseUsingExcel("ShopInfo","TC_001");
    }
}
